@extends('layouts.app2')
@section('title', 'Profile') 
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/cropper/cropper.min.css') }}"/>
@endsection
@section('menu', 'profile')
{!! $errors->first('avatar', '<p class="help-block">:message</p>') !!}
@section('content')                        
    <div class="col-md-6 col-sm-4 col-xs-5">
        <form action="#" class="form-horizontal">
            <div class="panel panel-default">                                
                <div class="panel-body">
                    <h3><span class="fa fa-user"></span> {{ $user->name }}</h3>
                    @php
                        $level =  $user->level_id;
                        if ($level == 1) $level = "Admin";
                        elseif ($level = 2) $level = "Redaktur";
                        elseif ($level == 3) $level = "Reporter";
                        else $level = "Unknown";
                    @endphp
                    <p>{{ $level }}</p>
                    <div class="text-center" id="user_image">
                        <img src="{{ $user -> photo_path }}" width="250" height="250" class="img-thumbnail"/>
                    </div>                                    
                </div>
                <div class="panel-body form-group-separated">
                    
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Avatar</label>                                        
                        <div class="col-md-9 col-xs-7">
                            <a href="#" class="btn btn-primary btn-block btn-rounded" data-toggle="modal" data-target="#modal_change_photo">Change</a>
                        </div>
                    </div>
                    
                    <div class="form-group">  
                        <label class="col-md-3 col-xs-5 control-label">Password</label>                                      
                        <div class="col-md-9 col-xs-7">
                            <a href="#" class="btn btn-danger btn-block btn-rounded" data-toggle="modal" data-target="#modal_change_password">Change</a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-6 col-sm-8 col-xs-7">
       {!! Form::model($user, ['route'=> ['profile.update', $user->id], 'method'=>'patch']) !!}
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3><span class="fa fa-pencil"></span> Profile</h3>
                </div>
                <div class="panel-body form-group-separated">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Name</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" name="name" value="{{ $user->name }}" class="form-control"/>
                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Username</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" name="username" value="{{ $user->username }}" class="form-control"/>
                            {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">E-mail</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" name="email" value="{{$user->email}}" class="form-control"/>
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">About me</label>
                        <div class="col-md-9 col-xs-7">
                            <textarea class="form-control" name ="bio" rows="5">{{ $user->bio }}</textarea>
                            {!! $errors->first('bio', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>                                          
                    <div class="form-group">
                        <div class="col-md-12 col-xs-5">
                            <button class="btn btn-primary btn-rounded pull-right">Save</button>
                        </div>
                    </div>
                </div>
            </div>
       {!! Form::close() !!}

    </div>
    <!-- MODALS -->
    <div class="modal animated fadeIn" id="modal_change_photo" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="smallModalHead">Change Avatar</h4>
                </div>                    
                <form id="cp_crop" method="post" action="assets/crop_image.php">
                <div class="modal-body">
                    <div class="text-center">Photo only JPG, JPEG, and PNG.</div>                     
                </div>                    
                </form>
                {!! Form::model($user, ['route'=> ['profile.update', $user->id], 'method'=>'patch', 'files'=>true]) !!}
                    <div class="modal-body form-horizontal form-group-separated">
                        <div class="form-group">
                            <label class="col-md-3 control-label">New Avatar</label>
                            <div class="col-md-7">
                                <input type="file" multiple id="file-simple" name="avatar"/>
                                {!! $errors->first('avatar', '<p class="help-block">:message</p>') !!}
                            </div>                            
                        </div>                        
                    </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Accept</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    
    <div class="modal animated fadeIn" id="modal_change_password" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="smallModalHead">Change password</h4>
                </div>
                {!! Form::model($user, ['route'=> ['profile.update', $user->id], 'method'=>'patch', 'files'=>true]) !!}
                    <div class="modal-body form-horizontal form-group-separated">                        
                        <div class="form-group">
                            <label class="col-md-3 control-label">Old Password</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="old_password"/>
                                {!! $errors->first('old_password', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">New Password</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="password"/>
                                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Repeat New</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="password_confirmation"/>
                                {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Proccess</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>        
    <!-- EOF MODALS -->
@endsection
@section('script')
    <script type='text/javascript' src='{{ asset('js/plugins/icheck/icheck.min.js') }}'></script>
    <script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('js/plugins/bootstrap/bootstrap-file-input.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/form/jquery.form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/fileinput/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/actions.js') }}"></script>
    <script>
        $("#file-simple").fileinput({
                showUpload: false,
                showCaption: false,
                browseClass: "btn btn-danger",
                fileType: "any"
        });  
    </script>
@endsection