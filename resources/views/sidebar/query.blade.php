<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Pencarian</h3>
    </div>
    <div class="panel-body">
        <div class="form-group {!! $errors->has('query') ? 'has-error' : '' !!}">
            {!! Form::label('query', 'Apa yang kamu cari?') !!}
            {!! Form::text('query', null, ['class'=>'form-control', 'placeholder'=>'Masukan kueri pencarian']) !!}
            {!! $errors->first('query', '<p class="help-block">:message</p>') !!}
        </div>
        {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
    </div>
</div>