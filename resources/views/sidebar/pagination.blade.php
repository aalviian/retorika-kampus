<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Atur Halaman</h3>
    </div>
    <div class="panel-body">
        <div class="form-group {!! $errors->has('jumlah-data') ? 'has-error' : '' !!}">
            {!! Form::label('jumlah-data', 'Berapa data per halaman?') !!}
            {!! Form::number('jumlah-data', null, ['class'=>'form-control', 'placeholder'=>'Masukan jumlah data yang di inginkan']) !!}
            {!! $errors->first('jumlah-data', '<p class="help-block">:message</p>') !!}
        </div>
        {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
    </div>
</div>