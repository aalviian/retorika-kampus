   <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Export Data</h3>
        </div>
        <div class="panel-body">
            <div class="col-md-12">
                <div class="col-md-6">  
                    <a href="#" class="btn btn-info btn-sm" onClick="$('#customers').tableExport({type:'txt',escape:'false'});"><img src="{{ asset('img/icons/txt.png') }}" width="20" height="20">TXT</a>
                </div>
                <div class="col-md-6"> 
                    <a href="#" class="btn btn-success btn-sm" onClick ="$('#customers').tableExport({type:'excel',escape:'false'});"><img src="{{ asset('img/icons/xls.png') }}" width="20" height="20">EXCEL</a>
                </div>
            </div>
            <br>
            <br>
            <div class="col-md-12">
                <div class="col-md-6"> 
                    <a href="#" class="btn btn-warning btn-sm" onClick="$('#customers').tableExport({type:'pdf',escape:'false'});"><img src="{{ asset('img/icons/pdf.png') }}" width="20" height="20">PDF</a>
                </div>
                <div class="col-md-6"> 
                    <a href="#" class="btn btn-danger btn-sm" onClick ="$('#customers').tableExport({type:'png',escape:'false'});"><img src="{{ asset('img/icons/png.png') }}" width="20" height="20">PNG</a>
                </div>
            </div>
        </div>
    </div>