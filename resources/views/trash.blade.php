@extends('layouts.app2')
@section('title', 'Trash') 
@section('menu', 'trash')
@section('content')   
	<div class="col-md-12"> 
		<br>
		<br>
		<div class="row">
			<div class="panel panel-default tab"> 
				<ul class="nav nav-tabs" role="tablist">
					@if(Auth::user()->level_id == 1)
				    <li role="presentation" class="active"><a href="#rubric" aria-controls="rubric" role="tab" data-toggle="tab">Rubric ({{ App\Rubric::withTrashed()->whereNotNull('deleted_at')->count() > 0 ? App\Rubric::withTrashed()->whereNotNull('deleted_at')->count() : 0 }})</a></li>
				    @endif
				    @if(Auth::user()->level_id == 1)
				    <li role="presentation"><a href="#type" aria-controls="type" role="tab" data-toggle="tab">Type({{ App\Type::withTrashed()->whereNotNull('deleted_at')->count() > 0 ? App\Type::withTrashed()->whereNotNull('deleted_at')->count() : 0 }})</a></li>
				    @endif
				    @if(Auth::user()->level_id == 1 or Auth::user()->level_id == 2)
				    <li role="presentation"><a href="#highlight" aria-controls="highlight" role="tab" data-toggle="tab">Highlight({{ App\Highlight::withTrashed()->whereNotNull('deleted_at')->count() > 0 ? App\Highlight::withTrashed()->whereNotNull('deleted_at')->count() : 0 }})</a></li>
				    @endif
				    @if(Auth::user()->level_id == 1 or Auth::user()->level_id == 2 or Auth::user()->level_id == 3)
				    <li role="presentation"><a href="#news" aria-controls="news" role="tab" data-toggle="tab">News({{ App\News::withTrashed()->whereNotNull('deleted_at')->count() > 0 ? App\News::withTrashed()->whereNotNull('deleted_at')->count() : 0 }})</a></li>
				    @endif
			 	</ul> 

			  	<div class="tab-content">
				    <div role="tabpanel" class="tab-pane panel-body {{ Auth::user()->level_id == 1 ? "active" : "" }}" id="rubric">
			            <h3> Daftar Trash Rubrik</h3>
			            <div class="table-responsive">
				            <table class= "table table-hover">
				                <thead>
				                    <tr>
				                        <td>Slug</td>
				                        <td>Name</td>
				                        <td>Created at</td>
				                        <td>Updated at</td>
				                        <td>Action</td>
				                    </tr> 
				                </thead>
				                <tbody>
				                    @forelse( $rubrics as $rubric)
				                    <tr>
				                        <td>{{ $rubric -> slug }} </td>
				                        <td>{{ $rubric -> name }}</td>
				                        <td>{{ $rubric -> created_at }}</td>
				                        <td>{{ $rubric -> updated_at }}</td>
				                        <td>
				                            <a href = "{{ url('restore-rubric', $rubric->id)}}" class = "btn btn-xs btn-success">Restore</a>
				                            <a href = "{{ url('delete-rubric', $rubric->id)}}" class = "btn btn-xs btn-danger">Delete Permanently</a>
				                        </td>
				                        @empty
				                        <td colspan="7">
				                            <center>
				                                <h2><i class="fa fa-trash"></i></h2>
				                                <p><h4>Tidak ada data di tong sampah</h4></p>
				                            </center>
				                        </td>
				                        @endforelse
				                    </tr>
				                </tbody>
				            </table>
			            {{ $rubrics->links() }}
			            </div>
				    </div>

				    <div role="tabpanel" class="tab-pane panel-body" id="type">
			            <h3> Daftar Trash Tipe</h3>
			            <div class="table-responsive">
				            <table class= "table table-hover">
				                <thead>
				                    <tr>
				                        <td>Slug</td>
				                        <td>Name</td>
				                        <td>Created at</td>
				                        <td>Updated at</td>
				                        <td>Action</td>
				                    </tr> 
				                </thead>
				                <tbody>
				                    @forelse( $types as $type)
				                    <tr>
				                        <td>{{ $type -> slug }} </td>
				                        <td>{{ $type -> name }}</td>
				                        <td>{{ $type -> created_at }}</td>
				                        <td>{{ $type -> updated_at }}</td>
				                        <td>
				                            <a href = "{{ url('restore-type', $type->id)}}" class = "btn btn-xs btn-success ">Restore</a>
				                            <a href = "{{ url('delete-type', $type->id)}}" class = "btn btn-xs btn-danger">Delete Permanently</a>
				                        </td>
				                        @empty
				                        <td colspan="7">
				                            <center>
				                                <h2><i class="fa fa-trash"></i></h2>
				                                <p><h4>Tidak ada data di tong sampah</h4></p>
				                            </center>
				                        </td>
				                        @endforelse
				                    </tr>
				                </tbody>
				            </table>
				            {{ $types->links() }}
				        </div>
				    </div>

				    <div role="tabpanel" class="tab-pane panel-body {{ Auth::user()->level_id == 2 ? "active" : "" }}" id="highlight">
			            <h3> Daftar Trash Highlight</h3>
			            <div class="table-responsive">
				            <table class= "table table-hover">
				                <thead>
				                    <tr>
				                    	<td>Image</td>
				                       	<td>Slug</td>
				                        <td>Name</td>
				                        <td style="width:30%">Description</td>
				                        <td>Created by</td>
				                        <td>Created at</td>
				                        <td>Updated at</td>
				                        <td colspan="2"><center>Action</center></td>
				                    </tr> 
				                </thead>
				                <tbody>
				                    @forelse( $highlights as $highlight)
				                    <tr>
				                    	<td><img src="img/highlight/{{ $highlight -> image }}" alt="..." width="50" height="50" class="img-responsive"></td>
				                        <td>{{ $highlight -> slug }}</td>
				                        <td>{{ $highlight -> name }}</td>
				                        <td>{{ $highlight -> description }}</td>
				                        <td>{{ $highlight -> user -> name }}</td>
				                        <td>{{ $highlight -> created_at }}</td>
				                        <td>{{ $highlight -> updated_at }}</td>
				                        <td>
				                            <a href = "{{ url('restore-highlight', $highlight->id)}}" class = "btn btn-xs btn-success ">Restore</a>
				                            <a href = "{{ url('delete-highlight', $highlight->id)}}" class = "btn btn-xs btn-danger">Delete Permanently</a>
				                        </td>
				                        @empty
				                        <td colspan="7">
				                            <center>
				                                <h2><i class="fa fa-trash"></i></h2>
				                                <p><h4>Tidak ada data di tong sampah</h4></p>
				                            </center>
				                        </td>
				                        @endforelse
				                    </tr>
				                </tbody>
				            </table>
				            {{ $highlights->links() }}
				        </div>		    	
				    </div>

				    <div role="tabpanel" class="tab-pane panel-body {{ Auth::user()->level_id == 3 ? "active" : "" }}" id="news">
			            <h3> Daftar Trash News</h3>
			            <div class="table-responsive">
				            <table class= "table table-hover">
				                <thead>
				                    <tr>
				                    	<td>Image</td>
				                        <td style="width:15%">Slug</td>
				                        <td style="width:15%">Name</td>
				                        <td>Created by</td>
				                        <td>Updated by</td>
				                        <td>Approved by</td>
				                        <td>Action</td>
				                    </tr> 
				                </thead>
				                <tbody>
				                    @forelse( $news as $f_news)
				                    <tr>
				                    	<td><img src="img/news/{{ $f_news -> image }}" alt="..." width="50" height="50" class="img-responsive"></td>
				                        <td>{{ $f_news -> slug }} </td>
				                        <td>{{ $f_news -> title }}</td>
				                        <td>{{ $f_news -> created_by }}</td> 
				                        <td>{{ $f_news -> edited_by }}</td>
				                        <td>{{ $f_news -> approved_by }}</td>
				                        <td>
				                            <a href = "{{ url('restore-news', $f_news->id)}}" class = "btn btn-xs btn-success ">Restore</a>
				                            <a href = "{{ url('delete-news', $f_news->id)}}" class = "btn btn-xs btn-danger">Delete Permanently</a>
				                        </td>
				                        @empty
				                        <td colspan="7">
				                            <center>
				                                <h2><i class="fa fa-trash"></i></h2>
				                                <p><h4>Tidak ada data di tong sampah</h4></p>
				                            </center>
				                        </td>
				                        @endforelse
				                    </tr>
				                </tbody>
				            </table>
				            {{ $types->links() }}
				        </div>
				    </div>
			  	</div>
			</div>
		</div>
	</div>
@endsection
@section('script')
    <script type='text/javascript' src='{{ asset('js/plugins/icheck/icheck.min.js') }}'></script>
    <script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('js/actions.js') }}"></script>
@endsection