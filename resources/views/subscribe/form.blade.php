<div class= " form-group { !! $errors->has('Email') ? 'has-error' : '' !!}">
	{!! Form::label('email', 'Email') !!}
	{!! Form::email('email', null, ['class' =>'form-control', 'placeholder' => 'Ketik email valid...']) !!}
	{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<button type="submit" class="btn"><span class="fa fa-paper-plane"></span></button>