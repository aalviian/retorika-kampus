@extends('layouts.app2')
@section('title', 'News')
@section('menu', 'news')
@section('content') 
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3> List Subscribers 
                    <div id ="add-member" class= "btn btn-info btn-sm" >+</div>
                    {!! Form::open(['url' => '/subscribe', 'method'=>'post', 'id'=>'form-add']) !!}
                        <div class="col-md-5">
                            @include('subscribe.form')
                        </div> 
                    {!! Form::close() !!}
                </h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class= "table table-hover">
                        <thead>
                            <tr> 
                                <th style="width:5%">Email</th>
                                <th>Created at</th>
                                <th>Updated at</th>
                                <th colspan="2" style="width:40%"><center>Action</center></th>
                            </tr> 
                        </thead>
                        <tbody>
                            @forelse($users as $key=>$user) 
                            <tr>
                                @php $key = $key+ 1 @endphp
                                <td>{{ $user -> email }}</td>
                                <td>{{ $user -> created_at }}</td>
                                <td>{{ $user -> updated_at }}</td>
                                <td><center>
                                   <div id='<?php echo $key; ?>' onClick="edit_click(this.id)" class = "btn btn-sm btn-success">Edit</div>
                                        {!! Form::model($user, ['route'=> ['subscribe.update', $user], 'method'=>'patch','id'=>'form-edit'.$key]) !!}
                                            <br>
                                            @include('subscribe.form', ['model'=>$user])
                                       {!! Form::close() !!} 
                                    </center></td> 
                                <td><center>
                                    @unless($user->level_id == 1)
                                    {!! Form::model($user, ['route' => ['subscribe.destroy', $user], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    {!! Form::submit('delete', ['class'=>'btn btn-sm btn-danger js-submit-confirm']) !!}
                                    {!! Form::close() !!}
                                    @endunless
                                </center></td> 
                            </tr>
                            @empty
                                <td colspan="4"><center><h2>:(</h2><p>Daftar Subscribers masih kosong</p></center></td>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $users->links() }}  
                </div> 
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3> List News</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class= "table table-hover">
                        <thead>
                            <tr> 
                                <th style="width:50%">Judul</th>
                                <th>Rubrik</th>
                                <th>Created_at</th>
                                <th colspan="2" style="width:40%"><center>Action</center></th>
                            </tr> 
                        </thead>
                        <tbody>
                            @forelse($news as $key=>$f_news) 
                            <tr>
                                @php $key = $key+ 1 @endphp
                                <td>{{ $f_news -> title }}</td>
                                <td>{{ $f_news -> rubric -> name }}</td>
                                <td>{{ $f_news -> updated_at }}</td>
                                <td>
                                    @if($f_news -> is_spreaded == 0)
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#spread-news{{$f_news->id}}">Spread</button>
                                    @else
                                    <span class="label label-success">Done</span>
                                    @endif
                                </td> 
                            </tr>
                            @empty
                            <tr>
                            <td colspan="4">
                            <center><h2>:(</h2><br>Daftar berita masih kosong</center>
                            </td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $news->links() }} 
                </div> 
            </div>
        </div>
    </div>

    <!-- Modal -->
    @foreach($news as $f_news)
        <div id="spread-news{{$f_news->id}}" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Spreadnews {{$f_news->title}}</h4>
              </div>
              <div class="modal-body">
                {!! Form::open(['url' => 'spread-news/'.$f_news->slug, 'method' => 'post']) !!}
                {!! Form::select('spreads[]', App\Subscribe::getSubscriber($f_news->id), null,['class'=>'form-control js-selectize', 'multiple']) !!}
              </div>
              <div class="modal-footer">
                {!! Form::submit('Spreads', ['class'=> 'btn btn-primary']) !!}
                {!! Form::close() !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>
    @endforeach
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/actions.js') }}"></script> 
<script type="text/javascript">
    $(document).ready( function() {
        document.getElementById("form-add").style.display = "none";

        $( "#add-member" ).click( function() {
            $( "#form-add" ).toggle( 'slow' );
        });

    });
</script>
<script type="text/javascript">
    for (var i=1; i<=' <?php echo($count); ?>'; i++ ){
        document.getElementById("form-edit"+i).style.display = "none";
    }

    function edit_click(id)
    {
        $("#form-edit"+id).toggle( 'slow' );
    }
</script>
@endsection 