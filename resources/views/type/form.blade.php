{{-- <div class="form-group {!! $errors->has('is_news') ? 'has-error' : '' !!}">
	{!! isset( $model) ? '' : Form::label('tipe' , 'Tipe') !!}
	<p><label>{{ Form::radio('is_news', 'Bukan Headline', true) }} Bukan Headline</label></p>
	<p><label>{{ Form::radio('is_news', 'Headline') }} Headline</label></p>
	{!! $errors->first('is_news','<p class="help-block">:message</p>') !!}
</div> 

<div class="form-group {!! $errors->has('is_news') ? 'has-error' : '' !!}">
	{!! isset( $model) ? '' : Form::label('tipe' , 'Tipe') !!}
	<p><label>{{ Form::radio('headline', 'Headline Depan', true) }} Headline Depan</label></p>
	<p><label>{{ Form::radio('headline', 'Headline') }} Headline Rubric</label></p>
	{!! $errors->first('is_news','<p class="help-block">:message</p>') !!}
</div> 

<div id="rubric"> --}}
	<div class= " form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
	{!! Form::label('name', 'Category') !!}
	{!! Form::select('name', ['Not Headline' => 'Not Headline','Front Headline' => 'Front Headline','' => '']+App\Rubric::isChild()->pluck('name', 'name')->all(), null, ['class' =>'form-control js-selectize']) !!}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
	</div>
{{-- </div>  --}}
 
{!! Form::submit( isset( $model) ? 'Update' : 'Save', [ 'class'=> 'btn btn-primary']) !!}