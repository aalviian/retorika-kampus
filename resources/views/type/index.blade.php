@extends('layouts.app2')
@section('title', 'Types')
@section('menu', 'types')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Types
            </h3>
            <div class="btn-group pull-right">
                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                <ul class="dropdown-menu">
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'false'});"><img src='img/icons/json.png' width="24"/> JSON</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'false',ignoreColumn:'[2,3]'});"><img src='img/icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'true'});"><img src='img/icons/json.png' width="24"/> JSON (with Escape)</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'xml',escape:'false'});"><img src='img/icons/xml.png' width="24"/> XML</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'sql'});"><img src='img/icons/sql.png' width="24"/> SQL</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'csv',escape:'false'});"><img src='img/icons/csv.png' width="24"/> CSV</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'txt',escape:'false'});"><img src='img/icons/txt.png' width="24"/> TXT</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'excel',escape:'false'});"><img src='img/icons/xls.png' width="24"/> XLS</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'doc',escape:'false'});"><img src='img/icons/word.png' width="24"/> Word</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'powerpoint',escape:'false'});"><img src='img/icons/ppt.png' width="24"/> PowerPoint</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'png',escape:'false'});"><img src='img/icons/png.png' width="24"/> PNG</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'pdf',escape:'false'});"><img src='img/icons/pdf.png' width="24"/> PDF</a></li>
                </ul>
            </div>                                    
        </div>
        <div class="panel-body">
            <span>Tambah: 
                <div id ="add-type" class= "btn btn-info btn-sm" >+</div>
                {!! Form::open(['route' => 'type.store', 'id'=>'form-add']) !!}
                    <div class="col-md-5">
                        @include('type.form')
                    </div>
                {!! Form::close() !!}
            <span>
            <div class="table-responsive">
                <table id="customers2" class="table datatable">
                    <thead>
                        <tr>
                            <th>Slug</th>
                            <th>Name</th>
                            <th>Created at</th>
                            <th>Updated at</td>
                            <th colspan="2"><center>Action</center></th>
                        </tr>
                    </thead>
                    <tbody> 
                        @forelse($types as $key=>$type)
                        <tr>
                            @php $key = $key+ 1 @endphp
                            <td>{{ $type -> slug }}</td>
                            <td>{{ $type -> name }}</td>
                            <td>{{ $type -> created_at }}</td>
                            <td>{{ $type -> updated_at }}</td>
                            <td>
                            @unless($type->name == 'Not Headline' OR $type->name == 'Front Headline')
                                <div id='@php echo $key; @endphp' onClick="edit_click(this.id)" class = "btn btn-sm btn-success">Edit</div>
                                {!! Form::model($type, ['route'=> ['type.update', $type], 'method'=>'patch','id'=>'form-edit'.$key]) !!}
                                    @include('type.form', ['model'=>$type])
                                {!! Form::close() !!} 
                            @endunless
                            </td>
                            <td>
                            @unless($type->name == 'Not Headline' OR $type->name == 'Front Headline')
                                {!! Form::model($type, ['route' => ['type.destroy', $type], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                {!! Form::submit('delete', ['class'=>'btn btn-sm btn-danger js-submit-confirm']) !!}
                                {!! Form::close() !!}
                            @endunless
                            </td>
                        </tr>
                        @empty
                            <td colspan="5"><center><h2>:(</h2><p>Tipe berita kosong</p></center></td>
                        @endforelse
                    </tbody>
                </table> 
                {{ $types->links() }}                                   
            </div>
        </div>
    </div>
@endsection

@section('script')
<script type='text/javascript' src="{{ asset('js/plugins/icheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/tableexport/tableExport.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/tableexport/jquery.base64.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/tableexport/html2canvas.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/tableexport/jspdf/libs/sprintf.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/tableexport/jspdf/jspdf.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/tableexport/jspdf/libs/base64.js') }}"></script>  
<script type="text/javascript" src="{{ asset('js/actions.js') }}"></script> 
<script type="text/javascript">
    $(document).ready( function() { 

        document.getElementById("form-add").style.display = "none";

        $( "#add-type" ).click( function() {
            $( "#form-add" ).toggle('slow');
        });

        for (var i=1; i<=' <?php echo($count); ?>'; i++ ){
            document.getElementById("form-edit"+i).style.display = "none";
        }

    });
    $(document).ready(function() {
        $('#customers2').DataTable();
    });
</script>
<script type="text/javascript">
    function edit_click(id)
    {
        $("#form-edit"+id).toggle( 'slow' );
    }
</script>
@endsection 