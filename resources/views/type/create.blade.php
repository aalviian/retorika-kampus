@extends('layouts.app')
	@section('content')
		<div class= "container">
			<div class= "row">
				<div class= "col-md-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3>New Rubric</h3>
						</div>
						<div class="panel-body">
							{!! Form::open(['route' => 'rubric.store', 'files'=>true]) !!}
								@include('rubric.form')
							{!! Form::close() !!}	
						</div>
					</div>
				</div>
			</div> 
		</div>
	@endsection