<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Retorika Kampus</title>
  
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert.css') }}" rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/selectize.bootstrap3.css') }}" rel='stylesheet' type='text/css'> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous"> 
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <style type="text/css">
        #form-add {
         display : none;
        }
        button {
         margin-bottom: 10px;
        }
    </style>
    @yield('style')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        Retorika Kampus
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                @if(Auth::check())
                    @if(Auth::user()->level_id == 1)
                        <ul class="nav navbar-nav">
                            <li><a href="{{ url('rubric') }}">Rubric</a></li>
                            <li><a href="{{ url('type') }}">Type</a></li>
                            <li><a href="{{ url('highlight') }}">Highlight</a></li>
                            <li><a href="{{ url('news') }}">News</a></li>
                            <li><a href="{{ url('member') }}">Members</a></li>
                            <li><a href="{{ url('sponsor') }}">Sponsor</a></li>
                        </ul>
                    @elseif (Auth::user()->level_id == 2)
                        <ul class="nav navbar-nav">
                            <li><a href="{{ url('highlight') }}">Highlight</a></li>
                            <li><a href="{{ url('news') }}">News</a></li>
                            <li><a href="{{ url('statistic') }}">Statistik</a></li>
                        </ul>
                    @endif
                @endif 

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="/">Home</a></li>
                        @else
                        @php 
                            $rubric = App\Rubric::withTrashed()->whereNotNull('deleted_at')->count() > 0 ? App\Rubric::withTrashed()->whereNotNull('deleted_at')->count() : 0; 
                            $type = App\Type::withTrashed()->whereNotNull('deleted_at')->count() > 0 ? App\Type::withTrashed()->whereNotNull('deleted_at')->count() : 0;
                            $highlight = App\Highlight::withTrashed()->whereNotNull('deleted_at')->count() > 0 ? App\Highlight::withTrashed()->whereNotNull('deleted_at')->count() : 0;
                            $news = App\News::withTrashed()->whereNotNull('deleted_at')->count() > 0 ? App\News::withTrashed()->whereNotNull('deleted_at')->count() : 0;
                            $count = $rubric + $type + $highlight + $news; 
                        @endphp
                            @if(Auth::user()->level_id == 1)
                                <li><a href="{{ url('authenticate') }}"><i class="fa fa-key-o" aria-hidden="true"></i> Get Token</a></li>
                                <li><a href="{{ url('trash') }}"><i class="fa fa-trash-o" aria-hidden="true"></i> Trash ({{$count}})</a></li>
                            @endif
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('profile') }}">Profile</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav> 
        @include('layouts.flash')
        <div class="animate-bottom">
            @yield('content')
        </div> 
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/app2.js') }}"></script>
    <script src="{{ asset('js/selectize.min.js') }}"></script>
    @include('sweet::alert')
    @yield('script')
    <script>
        $("#alert-time").fadeTo(4000, 500).slideUp(500, function(){
            $("#alert-time").slideUp(500);
        });
    </script>
</body>
</html>
