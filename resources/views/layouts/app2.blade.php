<!DOCTYPE html>

<html lang="en">

    <head>
        @php
          $config = App\Configuration::find('1');
        @endphp        

        <title>{{$config->web_name}} | @yield('title')</title>            

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <meta name="viewport" content="width=device-width, initial-scale=1" />

	<link rel="shortcut icon" href="{{ asset('images/general/icon.png') }}" type="image/x-icon">

        <link rel="icon" href="{{ asset('images/general/icon.png') }}" type="image/x-icon" />

        <link rel="stylesheet" type="text/css" id="theme" href="{{ asset('css/theme-default.css') }}"/> 

        <link href="{{ asset('css/sweetalert.css') }}" rel='stylesheet' type='text/css'>

        <link href="{{ asset('css/selectize.bootstrap3.css') }}" rel='stylesheet' type='text/css'>

        @yield('style')              

    </head>

    <body> 

        <style type="text/css">
            .xn-mylogo {
                position: relative;
                border-bottom: 0px;
                background-color: #ffffff;
                max-width: 100;
                min-width: 20;
                padding-top: 50px;
            }
            .mylogo {
                position: absolute;
                margin-top: -50px;
                width: 100%;
                height: 100%;
            }
        </style>

        <div class="page-container">

            <div class="page-sidebar">

                <ul class="x-navigation">

                    <li class="xn-logo">
                        <a href="{{ url('/') }}">Retorika Kampus</a>
                        <a href="#" class="x-navigation-control"></a> 
                    </li>

                    <li class="xn-profile">

                        <a href="#" class="profile-mini">

                            <img src="{{ Auth::user()->photo_path }}" alt="John Doe"/>

                        </a>

                        <div class="profile">

                            <div class="profile-image">

                                <img src="{{ Auth::user()->photo_path }}" alt="John Doe"/>

                            </div>

                            <div class="profile-data">

                                <div class="profile-data-name">{{ Auth::user()->name }}</div>

                                @php

                                    $level =  Auth::user()->level_id;

                                    if ($level == 1) $level = "Admin";

                                    elseif ($level == 2) $level = "Redaktur";

                                    elseif ($level == 3) $level = "Reporter";

                                    else $level = "User";

                                @endphp

                                <div class="profile-data-title">{{ $level }}</div>

                                <br>

                                <!--<div class="profile-data-name">{{ Auth::user()->level_id }}</div>-->

                            </div>

                            <div class="profile-controls"></div>

                        </div>                                                                        

                    </li>

                    <li class="xn-title">Navigation</li>

                    @if(Auth::check())

                        @if(Auth::user()->level_id == 1)

                            <ul class="nav navbar-nav">

                                <li>

                                    <a href="{{ url('/home') }}"><span class="fa fa-home"></span><span class="xn-text">Home</a>

                                </li>

                                <li class="xn-openable">

                                    <a href="#"><span class="fa fa-life-ring"></span> <span class="xn-text">Rubric</span></a>

                                    <ul>

                                        <li><a href="{{ url('rubric') }}"><span class="xn-text">Create</span></a></li>

                                        <li><a href="{{ url('rubric-order') }}"><span class="xn-text">Settings</span></a></li>

                                    </ul>

                                </li> 



                                <li>

                                    <a href="{{ url('type') }}"><span class="fa fa-map-signs"></span><span class="xn-text">Type</a>

                                </li>

                                <li  class="xn-openable">

                                    <a href="#"><span class="fa fa-space-shuttle"></span> <span class="xn-text">Highlight</span></a>

                                    <ul>

                                        <li><a href="{{ url('highlight') }}"><span class="xn-text">Create</span></a></li>

                                        <li><a href="{{ url('highlight-order') }}"><span class="xn-text">Settings</span></a></li>

                                    </ul>

                                </li>

                                <li>

                                    <a href="{{ url('news') }}"><span class="fa fa-newspaper-o"></span><span class="xn-text">News</a>

                                </li>

                                <li>

                                    <a href="{{ url('member') }}"><span class="fa fa-users"></span><span class="xn-text">Members</a>

                                </li>

                                <li>

                                    <a href="{{ url('advertisement') }}"><span class="fa fa-google-wallet"></span><span class="xn-text">Advertisement</a>

                                </li>

                                <li>

                                    <a href="{{ url('subscribe') }}"><span class="fa fa-chain-broken"></span><span class="xn-text">Subscribe</a>

                                </li>

                                <li>

                                    <a href="{{ url('comment') }}"><span class="fa fa-comments-o"></span><span class="xn-text">Comments</a>

                                </li>


                                <li>

                                    <a href="{{ url('accesslog') }}"><span class="fa fa-rocket"></span><span class="xn-text">Access Log</a>

                                </li>

                                <li>

                                    <a href="{{ url('config') }}"><span class="fa fa-cog"></span><span class="xn-text">Configuration</a>

                                </li>

                            </ul>

                        @elseif (Auth::user()->level_id == 2)

                            <ul class="nav navbar-nav">

                                <li>

                                    <a href="{{ url('/home') }}"><span class="fa fa-home"></span><span class="xn-text">Home</a>

                                </li>

                                <li><a href="{{ url('highlight') }}"><span class="fa fa-space-shuttle"></span> <span class="xn-text">Highlight</a></li>

                                <li><a href="{{ url('news') }}"><span class="fa fa-newspaper-o"></span> <span class="xn-text">News</a></li>

                                <li>

                                    <a href="{{ url('member') }}"><span class="fa fa-users"></span><span class="xn-text">Members</a>

                                </li>

                            </ul>

                        @elseif (Auth::user()->level_id == 3 or Auth::user()->level_id == 4)

                            <ul class="nav navbar-nav">

                                <li>

                                    <a href="{{ url('/home') }}"><span class="fa fa-home"></span><span class="xn-text">Home</a>

                                </li>

                                <li><a href="{{ url('news') }}"><span class="fa fa-newspaper-o"></span> <span class="xn-text">News</a></li>

                            </ul>

                        @endif

                    @endif                   

                </ul>

            </div>



            <div class="page-content">

                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">

                    <li class="xn-icon-button">

                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>

                    </li>

                    <li class="xn-search">

                        {!! Form::open(['url' => 'pencarian/berita', 'method' => 'get']) !!}

                            <input type="text" name="query"  autocomplete="off" placeholder="Searching news..."/>

                        {!! Form::close() !!}

                    </li>   



                    <li class="xn-icon-button pull-right last">

                        <a href="#"><span class="fa fa-power-off"></span></a>

                        <ul class="xn-drop-left animated zoomIn">

                            <li><a href="{{ route('profile.index') }}"><span class="fa fa-user"></span> Profile</a></li>

                            <li><a href="pages-lock-screen.html"><span class="fa fa-lock"></span> Lock Screen</a></li>

                            <li><a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Sign Out</a></li>

                        </ul>                        

                    </li>

                    @php 

                        $rubric = App\Rubric::withTrashed()->whereNotNull('deleted_at')->count() > 0 ? App\Rubric::withTrashed()->whereNotNull('deleted_at')->count() : 0; 

                        $type = App\Type::withTrashed()->whereNotNull('deleted_at')->count() > 0 ? App\Type::withTrashed()->whereNotNull('deleted_at')->count() : 0;

                        $highlight = App\Highlight::withTrashed()->whereNotNull('deleted_at')->count() > 0 ? App\Highlight::withTrashed()->whereNotNull('deleted_at')->count() : 0;

                        $news = App\News::withTrashed()->whereNotNull('deleted_at')->count() > 0 ? App\News::withTrashed()->whereNotNull('deleted_at')->count() : 0;

                        if(Auth::user()->level_id == 1)

                        $count = $rubric + $type + $highlight + $news; 

                        if(Auth::user()->level_id == 2)

                        $count = $highlight + $news;

                        if(Auth::user()->level_id == 3)

                        $count = $news;

                    @endphp

                    @if(Auth::user()->level_id == 4)



                    @else

                    <li class="pull-right"> 

                        <a href="{{ url('trash') }}"><i class="fa fa-trash-o" aria-hidden="true"></i> Trash ({{$count}})</a>

                    </li>

                    @endif
                    @php
                        $count_online = \App\User::where('is_login', 1)->orderBy('login_date', 'desc')->count();
                    @endphp
                    <li class="pull-right">
                        <a href="{{ url('log') }}"><img src="{{ url('img/data/online.png') }}" height="15" width="15"></img><span class="xn-text">&nbsp; &nbsp; &nbsp;Online ({{ $count_online }})</a>                        
                    </li>



                </ul>



                <ul class="breadcrumb">

                    <li><a href="#">Home</a></li>                    

                    <li class="active">@yield('menu')</li>

                </ul>

                <div class="page-content-wrap">

                    <div class="row">

                        <div class="col-md-12">

                            @include('layouts.flash')

                            @yield('content')

                        </div>

                    </div>

                </div>

            </div>            

        </div>



        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">

            <div class="mb-container">

                <div class="mb-middle">

                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>

                    <div class="mb-content">

                        <p>Are you sure you want to log out?</p>                    

                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>

                    </div>

                    <div class="mb-footer">

                        <div class="pull-right">

                            <a href="{{ route('logout') }}" onclick="event.preventDefault();

                                document.getElementById('logout-form').submit();" class="btn btn-success btn-lg">

                                            Yes

                            </a>



                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

                                {{ csrf_field() }}

                            </form>

                            <button class="btn btn-default btn-lg mb-control-close">No</button>

                        </div>

                    </div>

                </div>

            </div>

        </div>



        <audio id="audio-alert" src="{{ asset('audio/alert.mp3') }}" preload="auto"></audio>

        <audio id="audio-fail" src="{{ asset('audio/fail.mp3') }}" preload="auto"></audio>

        <script type="text/javascript" src="{{ asset('js/plugins/jquery/jquery.min.js') }}"></script>

        <script type="text/javascript" src="{{ asset('js/plugins/jquery/jquery-ui.min.js') }}"></script>

        <script type="text/javascript" src="{{ asset('js/plugins/bootstrap/bootstrap.min.js') }}"></script>

        <script src="{{ asset('js/sweetalert.min.js') }}"></script>

        @include('sweet::alert')

        <script src="{{ asset('js/app2.js') }}"></script>

        <script src="{{ asset('js/selectize.min.js') }}"></script>

        <script>

            $("#alert-time").fadeTo(4000, 500).slideUp(500, function(){

                $("#alert-time").slideUp(500);

            }); 

        </script>

        <script src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>

        @yield('script')              

    </body>

</html>













