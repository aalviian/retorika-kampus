@extends('layouts.app')
@section('content')
    <div class= "container">
        <div class= "row">
            <div class= "col-md-12">
                    <h3> Statistics  
                    </h3>
                
                <table class= "table table-hover">
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Member</td>
                            <td>Role</td>
                            <td>News</td>
                        </tr> 
                    </thead>
                    <tbody>
                        <?php $counter=1; ?>
                        @foreach($users as $user)
                            @if($user->level_id != 1 )
                            <tr>
                                <td>{{ $counter++ }}</td>
                                <td>{{ $user -> name }}</td>
                                <td>
                                    @if($user->level_id == 2)
                                        {{'Redaktur'}}
                                    @elseif($user->level_id == 3)
                                        {{'Reporter'}}
                                    @else
                                        {{'Member'}}
                                    @endif                                    
                                </td>
                                <td>{{ $statistic[$user->username] }}</td>
                                
                            </tr>
                            @endif
                        @endforeach
                    </body>
                </table>
            </div>
        </div>
    </div>
@endsection