@extends('layouts.app')
	@section('content')
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3>Edit Highlight '{{ $highlight->name }}'</h3>
						</div>
						<div class="panel-body">
							{!! Form::model($highlight, ['route'=> ['highlight.update', $highlight], 'method'=>'patch']) !!}
		                        @include('highlight.form', ['model'=>$highlight])
		                    {!! Form::close() !!} 
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection