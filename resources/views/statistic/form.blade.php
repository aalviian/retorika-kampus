<div class= " form-group { !! $errors->has('name') ? 'has-error' : '' !!}">
	{!! Form::label('name', 'Name') !!}
	{!! Form::text('name', null, ['class' =>'form-control', 'placeholder' => 'Ketik nama rubric...']) !!}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class= " form-group { !! $errors->has('description') ? 'has-error' : '' !!}">
	{!! Form::label('description' , 'description') !!}
	{!! Form::text('description', null, ['class' =>'form-control', 'placeholder' => 'Ketik nama rubric...']) !!}
	{!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('image') ? 'has-error' : '' !!}">
	{!! Form::label('image', 'Highlight Image ( jpg,jpeg,png )') !!}
	{!! Form::file('image') !!}
	{!! $errors->first('image', '<p class="help-block">:message</p>')  !!}
	@if (isset($model) && $model->image !== '' )
	<div class= "row">
		<div class= "col-md-6">
			<br>
			<p>Current iamge:</p>
			<div class= "thumbnail">
				<img src = "{{ url('/img/highlight/' . $model->image) }}" class="img-rounded" width="200" height="200">
			</div>
		</div>
	</div>
	@endif
	@if (isset($model) && $model->image == '')
	<div class= "row">
		<div class= "col-md-6">
			<br>
			<p>Current image:</p>
			<div class= "thumbnail">
				<img src = "{{ url('/img/highlight/contoh.jpg') }}" class="img-rounded" width="200" height="200">
			</div>
		</div>
	</div>
	@endif
</div>

<center>
<a href="{{ route('highlight.index') }}" class="btn btn-warning">Cancel</a>
{!! Form::submit( isset( $model) ? 'Update' : 'Save', [ 'class'=> 'btn btn-primary']) !!}
</center>