@extends('layouts.app2')
@section('title', 'Profile') 
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/cropper/cropper.min.css') }}"/>
@endsection
@section('menu', 'profile')
{!! $errors->first('avatar', '<p class="help-block">:message</p>') !!}
@section('content')                        
    <div class="col-md-6 col-sm-4 col-xs-5">
        <form action="#" class="form-horizontal">
            <div class="panel panel-default">                                 
                <div class="panel-body">
                    <h3><span class="fa fa-user"></span>  Logo</h3>
                    <div class="text-center" id="user_image">
                        <img src="{{ $config->photo_path }}" width="400" height="250" class="img-thumbnail"/>
                    </div>                                    
                </div>
                <div class="panel-body form-group-separated">
                    
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Logo</label>                                        
                        <div class="col-md-9 col-xs-7">
                            <a href="#" class="btn btn-primary btn-block btn-rounded" data-toggle="modal" data-target="#modal_change_photo">Change</a>
                        </div>
                    </div>

                </div>
            </div>
        </form>
        <br>
        <form action="config/update/{{$config->id}}" class="form-horizontal" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="panel panel-default">                                
                <div class="panel-body">
                    <h3><span class="fa fa-user"></span> Social Network</h3>                                   
                </div>
                <div class="panel-body form-group-separated">
                    
                    <div class="form-group">                                       
                        <label class="col-md-3 col-xs-5 control-label"><span class="fa fa-facebook-square" aria-hidden="true">  Facebook</span></label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" name="facebook" value="{{ $config->facebook }}" class="form-control"/>
                            {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group">                                       
                        <label class="col-md-3 col-xs-5 control-label"><span class="fa fa-twitter-square" aria-hidden="true">  Twitter</span></label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" name="twitter" value="{{ $config->twitter }}" class="form-control"/>
                            {!! $errors->first('twitter', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group">                                       
                        <label class="col-md-3 col-xs-5 control-label"><span class="fa fa-cc" aria-hidden="true">  Email</span></label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" name="email" value="{{ $config->email }}" class="form-control"/>
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12 col-xs-5">
                            <button class="btn btn-primary btn-rounded pull-right">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-6 col-sm-8 col-xs-7">
        <form action="config/update/{{$config->id}}" class="form-horizontal" method="post" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3><span class="fa fa-pencil"></span> General</h3>
                </div>
                <div class="panel-body form-group-separated">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Web Name</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" name="web_name" value="{{ $config->web_name }}" class="form-control"/>
                            {!! $errors->first('web_name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">URL</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" name="url" value="{{ $config->url}}" class="form-control"/>
                            {!! $errors->first('url', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Tagline</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" name="tagline" value="{{ $config->tagline }}" class="form-control"/>
                            {!! $errors->first('tagline', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Keywords</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="text" name="keywords" value="{{ $config->keywords }}" class="form-control"/>
                            {!! $errors->first('keywords', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Maps Latitude</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="number" name="lat" value="{{ $config->lat }}" class="form-control"/>
                            {!! $errors->first('lat', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Maps Longitude</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="number" name="long" value="{{ $config->long }}" class="form-control"/>
                            {!! $errors->first('long', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Address</label>
                        <div class="col-md-9 col-xs-7">
                            <textarea class="form-control" name ="address" rows="5">{{ $config->address }}</textarea>
                            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>                                          
                    <div class="form-group">
                        <div class="col-md-12 col-xs-5">
                            <button class="btn btn-primary btn-rounded pull-right">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
       <br>
        <form action="config/update/{{$config->id}}" class="form-horizontal" method="post" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3><span class="fa fa-pencil"></span> Contact</h3>
                </div>
                <div class="panel-body form-group-separated">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Phone Number</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="number" name="phone_number" value="{{ $config->phone_number }}" class="form-control" onkeypress="return event.charCode >= 48", placeholder="ex: 21837448" />
                            {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Mobile Number</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="number" name="mobile_number" value="{{ $config->mobile_number }}" class="form-control" onkeypress="return event.charCode >= 48", placeholder="ex: 08747346374" />
                            {!! $errors->first('mobile_number', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-5 control-label">Fax Number</label>
                        <div class="col-md-9 col-xs-7">
                            <input type="number" name="fax_number" value="{{ $config->fax_number }}" class="form-control" onkeypress="return event.charCode >= 48", placeholder="ex: 52133789893" />
                            {!! $errors->first('fax_number', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-xs-5">
                            <button class="btn btn-primary btn-rounded pull-right">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>       
    </div>
    <!-- MODALS -->
    <div class="modal animated fadeIn" id="modal_change_photo" tabindex="-1" role="dialog" aria-labelledby="smallModalHead" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="smallModalHead">Change Logo</h4>
                </div>                    
                <form id="cp_crop" method="post" action="assets/crop_image.php">
                <div class="modal-body">
                    <div class="text-center">Photo only JPG, JPEG, and PNG.</div>                     
                </div>                    
                </form>
                <form action="config/update/{{$config->id}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-body form-horizontal form-group-separated">
                        <div class="form-group">
                            <label class="col-md-3 control-label">New Logo</label>
                            <div class="col-md-7">
                                <input type="file" multiple id="file-simple" name="logo"/>
                                {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
                            </div>                            
                        </div>                        
                    </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Accept</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    
    <!-- EOF MODALS -->
@endsection
@section('script')
    <script type='text/javascript' src='{{ asset('js/plugins/icheck/icheck.min.js') }}'></script>
    <script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('js/plugins/bootstrap/bootstrap-file-input.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/form/jquery.form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/fileinput/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/actions.js') }}"></script>
    <script>
        $("#file-simple").fileinput({
                showUpload: false,
                showCaption: false,
                browseClass: "btn btn-danger",
                fileType: "any"
        });  
    </script>
@endsection