@extends('layouts.app')
	@section('content')
		<div class= "container">
			<div class= "row">
				<div class= "col-md-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3>Get Token</h3>
						</div>
						<div class="panel-body">
							{!! Form::open(['url' => 'authenticate', 'method'=>'post']) !!}
								<div class= " form-group { !! $errors->has('email') ? 'has-error' : '' !!}">
									{!! Form::label('email', 'Email') !!}
									{!! Form::text('email', null, ['class' =>'form-control']) !!}
									{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
								</div>

								<div class= " form-group { !! $errors->has('password') ? 'has-error' : '' !!}">
									{!! Form::label('password' , 'Password') !!}
									{!! Form::password('password', null, ['class' =>'form-control']) !!}
									{!! $errors->first('password', '<p class="help-block">:message</p>') !!}
								</div>

								{!! Form::submit('Save', [ 'class'=> 'btn btn-primary']) !!}
							{!! Form::close() !!}	
						</div>
					</div>
				</div>
			</div> 
		</div>
	@endsection