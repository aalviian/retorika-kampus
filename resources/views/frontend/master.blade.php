<!DOCTYPE html>
<html lang="en">
<head>
@php
  $config = App\Configuration::find('1');
@endphp
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<title>{{$config->web_name}} - @yield('title')</title>
<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
<link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
<link href="{{ asset('css/frontend/bootstrap.min.css') }}" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
<link href="{{ asset('css/frontend/ionicons.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/frontend/font-awesome.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/frontend/animate.css') }}" />
<link href="{{ asset('css/frontend/custom-red.css') }}" rel="stylesheet" id="style">
<link rel="stylesheet" href="{{ asset('css/frontend/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('css/frontend/owl.transitions.css') }}">
<link rel="stylesheet" href="{{ asset('css/frontend/magnific-popup.css') }}">
<link href="{{ asset('css/sweetalert.css') }}" rel='stylesheet' type='text/css'>
</head>
<body> 

<div id="preloader">
  <div id="status"></div>
</div>

<div class="wrapper"> 
  <div class="header-toolbar">
    <div class="container">
      <div class="row">
        <ul id="inline-popups" class="list-inline pull-right">
                  @if(Auth::check())
                    <li><a href="{{ url('home') }}">Dashboard</a></li>
                    <li>
                      <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();" data-effect="mfp-zoom-in">Log Out</a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                      </form>
                    </li>
                  @else
                    <li><a href="/request-ads">advertisement</a></li>
                    <li><a class="open-popup-link" href="#log-in" data-effect="mfp-zoom-in">log in</a></li>
                    <li><a class="open-popup-link" href="#create-account" data-effect="mfp-zoom-in">create account</a></li>
                   {{--  <li><a  href="#">About</a></li> --}}
                  @endif
                </ul>
      </div>
    </div>
  </div>
  <div class="sticky-header"> 
    <div class="container header">
      <div class="row">
        <div class="col-sm-8 col-md-8 wow fadeInUpLeft animated">
          <a href="{{ url('/') }}">
            <img class="img-responsive" src= " {{url('images/general/'.$config->logo)}} " width="468">    
          </a>
        </div>
        @yield('ads1')
      </div>
    </div>
    <div class="nav-search-outer"> 
      <nav class="navbar navbar-inverse" role="navigation">
        <div class="container">
          <div class="row">
            <div class="col-sm-16"> <a href="javascript:;" class="toggle-search pull-right"><span class="fa fa-search"></span></a>
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              </div>
              <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav text-uppercase main-nav ">
                  @foreach($rubrics as $rubric)

                    @if($rubric->hasChild())
                    <li class="dropdown">
                      <a href="{{url('berita/rubrik/'.$rubric->slug)}}" class="dropdown-toggle" data-toggle="dropdown">{{ $rubric->name }} <span class="fa fa-caret-down"></span></a>
                      <ul class="dropdown-menu text-capitalize" role="menu">
                        @foreach ($rubric->childs as $child_rubric)
                        <li class="{{ ((Request::is('/berita/rubrik/$child_rubric->slug/*')) ? 'active' : '') }}"><a href="{{ url('berita/rubrik',$child_rubric->slug) }}"><span class="fa fa-arrow-circle-right nav-sub-icn"></span>{{ $child_rubric->name }}</a></li>
                        @endforeach
                      </ul>
                    </li>

                    @else
                    <li class="{{ ((Request::is('/berita/rubric/$rubric->slug/*')) ? 'active' : '') }}"><a href="{{ url('berita/rubrik',$rubric->slug) }}">{{ $rubric->name }}</a></li>

                    @endif
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="search-container">
          <div class="container">
            {!! Form::open(['url' => 'pencarian', 'method' => 'get']) !!}
              <input id="search-bar" placeholder="Type & Hit Enter.." autocomplete="off" name="query">
            {!! Form::close() !!}
          </div>
        </div>
      </nav>
    </div>
  </div>

  @yield('content')


  <footer>
    <div class="top-sec">
      <div class="container ">
        <div class="row match-height-container">
          <div class="col-sm-6 subscribe-info  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="40">
            <div class="row">
              <div class="col-sm-16">
                <div class="f-title">{{$config->web_name}}</div>
                <p>{{$config->address}}</p>
              </div>
              <div class="col-sm-16">
                <div class="f-title">subscribe to newsletter</div>
                {!! Form::open(['url' => '/subscribe', 'method'=>'post']) !!}
                  <div class= " form-group { !! $errors->has('Email') ? 'has-error' : '' !!}">
                    {!! Form::email('email', null, ['class' =>'form-control', 'placeholder' => 'Ketik email valid...']) !!}
                    {!! $errors->first('email', '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>:message</div>') !!}
                    <button type="submit" class="btn"><span class="fa fa-paper-plane"></span></button>
                  </div>
                {!! Form::close() !!} 
              </div>
            </div> 
          </div>
 
          <!-- Rubric -->
          <div class="col-sm-5 popular-tags  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="40">
            <div class="row">
              <div class="f-title">Rubrik</div>
              <ul class="tags list-unstyled pull-left">
                @foreach($rubrics as $rubric)
                  @if($rubric->hasChild())
                    @foreach ($rubric->childs as $child_rubric)
                      <li>
                        <a class="rss" href="{{ url('berita/rubrik',$child_rubric->slug) }}">
                          {{ $child_rubric->name }}
                        </a>
                      </li>
                    @endforeach
                  @else
                    <li class="{{ ((Request::is('/berita/rubric/$rubric->slug/*')) ? 'active' : '') }}"><a href="{{ url('berita/rubrik',$rubric->slug) }}">{{ $rubric->name }}</a></li>
                  @endif
                @endforeach
              </ul>
            </div>
          </div>

          <!-- Fokus -->
          <div class="col-sm-5 recent-posts  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="40">
            <div class="row">
              <div class="f-title">Fokus</div>
              <ul class="tags list-unstyled pull-left">
                @forelse($highlights as $highlight)
                  <li>
                    <a class="rss" href="{{ url('berita/fokus/'.$highlight->slug) }}">{{ $highlight->name }}</a> 
                  </li>
                @empty
                  <p><b><center><h3>:(</h3></center></b></p>
                  <p>Fokus tidak tersedia</p>
                @endforelse
              </ul>  
              </div>
              
          </div>
        </div>
      </div>
    </div>
    <div class="btm-sec">
      <div class="container">
        <div class="row">
          <div class="col-sm-16 f-social  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="10">
            <ul class="list-inline">
              <li> <a href="{{ $config->twitter }}"><i class="fa fa-twitter"></i></a> </li>
              <li> <a href="{{ $config->facebook }}"><i class="fa fa-facebook"></i></a> </li>
              <li> <a href = "mailto: {{ $config->email }}"><i class="fa fa-newspaper-o"></i></a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <div id="create-account" class="white-popup mfp-with-anim mfp-hide">
    {!! Form::open(['url' => 'user-store', 'method'=>'post', 'class'=>'form-horizontal','role'=>'form']) !!}
        <h3>Create an account</h3>
        <hr>
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Name</label>

            <div class="col-md-8">
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            <label for="username" class="col-md-4 control-label">Username</label>

            <div class="col-md-8">
                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

            <div class="col-md-8">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">Password</label>

            <div class="col-md-8">
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

            <div class="col-md-8">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
        </div>

        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
            <label for="phone" class="col-md-4 control-label">Phone</label>

            <div class="col-md-8">
                <input id="phone" type="number" class="form-control" name="phone" onkeypress="return event.charCode >= 48", min="1" placeholder="ex: 87654628320" required>

                @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Register
                </button>
            </div>
        </div>
    {!! Form::close() !!}
  </div>
  <div id="log-in" class="white-popup mfp-with-anim mfp-hide">
    {!! Form::open(['url' => 'login', 'class'=>'form-horizontal','role'=>'form']) !!}
      <h3>Log In Your Account</h3>
      <hr>
      <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
          <div class="col-md-12">
              <div class="input-group"> 
                  <div class="input-group-addon">
                      <span class="fa fa-user"></span>
                  </div>
                  <input id="username" type="username" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username" required autofocus/>
                  @if ($errors->has('username'))
                      <span class="help-block">
                          <strong>{{ $errors->first('username') }}</strong>
                      </span>
                  @endif
              </div>
          </div>
      </div>
      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <div class="col-md-12">
              <div class="input-group">
                  <div class="input-group-addon">
                      <span class="fa fa-lock"></span>
                  </div>                                
                  <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
              </div>
          </div>
      </div>
      <div class="form-group">
          <div class="col-md-12">
              <div class="checkbox">
                  <label>
                      <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember Me
                  </label>
              </div>
          </div>
      </div>
      <div class="form-group">
          <div class="col-md-12">
              <a href="{{ url('/password/reset') }}">Forgot your password?</a>
          </div>          
      </div>
      <div class="form-group">
          <div class="col-md-12">
              <button class="btn btn-primary btn-lg btn-block">Login</button>
          </div>
      </div>
    {!! Form::close() !!}
  </div>
</div>

<!-- jQuery --> 
<script src="{{ asset('js/frontend/jquery.min.js') }}"></script> 
<!--jQuery easing--> 
<script src="{{ asset('js/frontend/jquery.easing.1.3.js') }}"></script> 
<!-- bootstrab js --> 
<script src="{{ asset('js/frontend/bootstrap.js') }}"></script> 
<!--style switcher--> 
<script src="{{ asset('js/frontend/style-switcher.js') }}"></script> <!--wow animation--> 
<script src="{{ asset('js/frontend/wow.min.js') }}"></script> 
<!-- time and date --> 
<script src="{{ asset('js/frontend/moment.min.js') }}"></script> 
<!--news ticker--> 
<script src="{{ asset('js/frontend/jquery.ticker.js') }}"></script> 
<!-- owl carousel --> 
<script src="{{ asset('js/frontend/owl.carousel.js') }}"></script> 
<!-- magnific popup --> 
<script src="{{ asset('js/frontend/jquery.magnific-popup.js') }}"></script> 
<!-- weather --> 
<script src="{{ asset('js/frontend/jquery.simpleWeather.min.js') }}"></script> 
<!-- calendar--> 
<script src="{{ asset('js/frontend/jquery.pickmeup.js') }}"></script> 
<!-- go to top --> 
<script src="{{ asset('js/frontend/jquery.scrollUp.js') }}"></script> 
<!-- scroll bar --> 
<script src="{{ asset('js/frontend/jquery.nicescroll.js') }}"></script> 
<script src="{{ asset('js/frontend/jquery.nicescroll.plus.js') }}"></script> 
<!--masonry--> 
<script src="{{ asset('js/frontend/masonry.pkgd.js') }}"></script> 
<!--media queries to js--> 
<script src="{{ asset('js/frontend/enquire.js') }}"></script> 
<!--custom functions--> 
<script src="{{ asset('js/frontend/custom-fun.js') }}"></script>
<script src="{{ asset('js/sweetalert.min.js') }}"></script>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.10";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@include('sweet::alert')
</body>
</html>