 @extends('frontend.master')
@section('title', 'Home')

@section('content') 

<style type="text/css">
  .image-lanscape {
    width: 750px;
    height: 400px;
  }
  .image-lanscape2 {
    width: 175px;
    height: 175px;
  }
  .image-lanscape3 {
    width: 130px;
    height: 130px;
  }
  .image-fitt {
    width: 100%;
    height: 100%;
  }
</style>

<div class="container ">
  <div class="row "> 
    <!-- left sec start -->
    <div class="col-md-10 col-sm-10">
      <div class="row">
        <!-- News start -->
        <div class="col-sm-16">
          <div class="row">
            <div class="col-xs-16 wow fadeInLeft animated science" data-wow-delay="0.5s" data-wow-offset="130">
              <div class="main-title-outer pull-left">
                <div class="main-title">Berita</div>
              </div>
              <div class="row">
                <div class="col-sm-16">
                  <ul class="list-unstyled  top-bordered ex-top-padding">
                  <br>
                  <b>Pencarian ditemukan: {{ $count }} hasil</b>
                  <br>
                    @forelse($news as $f_news)
                    <li> <a href="{{url('berita/rubrik/'.$f_news->rubric->slug.'/'.$f_news->slug)}}">
                      <div class="row">
                        <div class="col-lg-3 col-md-4 hidden-sm ">
                          <img width="150" height="150" alt="" src="{{$f_news->photo_path}}" class="img-responsive img-thumbnail pull-left">
                        </div>
                        <div class="col-lg-13 col-md-12">
                          <h3>{{$f_news->title}}</h3>
                          <div class='content'>
                            {!! str_limit(strip_tags($f_news->content), $limit=150, $end='...' ) !!}
                          </div>
                          <div class="text-danger sub-info">
                            @php
                                $user = App\User::where('id', $f_news->created_by)->first();
                            @endphp
                            @if(!empty($user))
                              <span class="fa fa-pencil">&nbsp;{{ $user->name }}&nbsp;&nbsp;</span>
                            @else
                              <span class="fa fa-pencil">&nbsp;Unkown&nbsp;&nbsp;</span>
                            @endif
                            <span class="fa fa-calendar">&nbsp;{{ $f_news->created_at }}&nbsp;&nbsp;</span>  
                          </div>
                        </div>
                      </div>
                      </a> </li>
                      @empty
                      <center>
                        <h1>:(</h1>
                        <br>
                        <h3>Pencarian tidak ditemukan
                      </center>
                      @endforelse
                  </ul>
                  {!! $news->appends(['query'=>$query])->links() !!}
                </div>
              </div>
            </div>
          </div>
          <hr>
        </div>
        <!-- News end --> 

        <!--wide ad start-->
        <center>
          @isset($ads5)
            <div class="col-sm-16 wow fadeInDown animated"  width="728" height="90"  data-wow-delay="0.5s" data-wow-offset="25">
              <a href="{{$ads5->url}}">
                <img class= "img-responsive" src="{{url('/img/data/advertisement/'.$ads5->image)}}" alt=""/>
              </a>
              <hr>
            </div> 
          @endisset

          @empty($ads5)
            <div class="col-sm-16 wow fadeInDown animated " data-wow-delay="0.5s" data-wow-offset="25">
              <img class="img-responsive" src="{{url('images/ads/728-90-ad.gif')}}" width="728" height="90" alt=""/>
            <hr>
            </div>
          @endempty
        </center>
        <!--wide ad end--> 
        
      </div>
    </div>
    <!-- left sec end --> 

    <!-- right sec start -->
    <div class="col-sm-6 hidden-xs right-sec">
      <div class="bordered top-margin">
        <div class="row ">
          <center>
            @isset($ads2)
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50"> 
              <a href="{{$ads2->url}}">
                <img class="img-responsive" src="{{ url('/img/data/advertisement/'.$ads2->image ) }}" width="336" height="280" alt=""/>
              </a>
            </div>
            @endisset

            @empty($ads2)
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50"> <img class="img-responsive" src="{{url('images/ads/336-280-ad.gif')}}" width="436" height="380" alt=""/></div>
            @endempty

            @isset($ads3)
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50">
              <a href="{{$ads3->url}}">
                <img class="img-responsive" src="{{ url('/img/data/advertisement/'.$ads3->image ) }}" width="336" height="280" alt=""/>
              </a>
            </div>
            @endisset

            @empty($ads3)
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50"> <img class="img-responsive" src="{{url('images/ads/336-280-ad.gif')}}" width="436" height="380" alt=""/> </div>
            @endempty
          </center>
        </div>
      </div>
      <div class="col-sm-16 bt-spac wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="150">
        <div class="table-responsive">
          <a href="#" class="sponsored">Fokus</a> 
          <table class="table table-bordered social">
            <tbody>
              <tr>
                @php $key=1; @endphp
                @forelse($highlights as $highlight)
                  @if($key % 4 != 0)
                  <td>
                    <a class="rss" href="{{ url('berita/fokus/'.$highlight->slug) }}">
                      <p><img src="{{ $highlight->photo_path }}" width="20" height="20"><br>{{ $highlight->name }}</p>
                    </a> 
                  </td>
                  @php $key++; @endphp
                  @endif
                  @if($key % 4 == 0)
                    <tr></tr>
                    @php $key++; @endphp
                  @endif
                @empty
                  <p><b><center><h3>:(</h3></center></b></p>
                  <p>Fokus tidak tersedia</p>
                @endforelse
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="130"> 
        <ul class="nav nav-tabs nav-justified " role="tablist">
          <li class="active"><a href="#popular" role="tab" data-toggle="tab">popular</a></li>
          <li><a href="#recent" role="tab" data-toggle="tab">recent</a></li>
        </ul>

        <div class="tab-content">
          <!-- Start Popular -->
          <div class="tab-pane active" id="popular">
            <ul class="list-unstyled">
              @forelse($popular_news as $f_popular_news)
                <li>
                  <a href="{{ url('berita/rubrik/'.$f_popular_news->rubric->slug.'/'.$f_popular_news->slug) }}">
                    <div class="row">
                      <div class="col-sm-5  col-md-4 ">
                        <img class="image-fitt img-responsive img-thumbnail pull-left" src="{{ $f_popular_news->photo_path }}" width="164" height="152" alt=""/>
                      </div>
                      <div class="col-sm-11  col-md-12 ">
                        <h4>{{ $f_popular_news->title }}</h4>
                        <div class="text-danger sub-info">
                          <div class="time">
                            <span class="fa fa-calendar"></span>&nbsp;&nbsp;{{ $f_popular_news->created_at }}
                          </div>
                          <div class="comments">
                            <span class="fa fa-eye"></span>&nbsp;&nbsp;{{ $f_popular_news->views }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
                </li>
              @empty
              <p><b><center><h3>:(</h3></center></b></p>
              <p>Popular News tidak tersedia</p>
              @endforelse
            </ul>
          </div>
          <!-- End Popular -->

          <!-- Start Recent News -->
          <div class="tab-pane" id="recent">
            <ul class="list-unstyled">
              @forelse($recent_news as $f_recent_news)
                <li>
                  <a href="{{ url('berita/rubrik/'.$f_recent_news->rubric->slug.'/'.$f_recent_news->slug) }}">
                    <div class="row">
                      <div class="col-sm-5  col-md-4 ">
                        <img class="image-fitt img-responsive img-thumbnail pull-left" src="{{ $f_recent_news->photo_path }}" width="164" height="152" alt=""/>
                      </div>
                      <div class="col-sm-11  col-md-12 ">
                        <h4>{{ $f_recent_news->title }}</h4>
                        <div class="text-danger sub-info">
                          <div class="time">
                            <span class="fa fa-calendar"></span>&nbsp;&nbsp;{{ $f_recent_news->created_at }}
                          </div>
                          <div class="comments">
                            <span class="fa fa-paper-plane-o"></span>&nbsp;&nbsp;{{ $f_recent_news->rubric->name }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
                </li>
              @empty
              <p><b><center><h3>:(</h3></center></b></p>
              <p>Recent News tidak tersedia</p>
              @endforelse
            </ul>
          </div>
          <!-- End Recent News -->
        </div>
      </div>
      <div class="bordered top-margin">
        <div class="row ">
          <center>
            @isset($ads4)
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50"> 
              <a href="{{$ads4->url}}">
                <img class="img-responsive" src="{{ url('/img/data/advertisement/'.$ads4->image ) }}" width="336" height="280" alt=""/>
              </a>
            </div>
            @endisset

            @empty($ads4)
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50"> <img class="img-responsive" src="{{url('images/ads/336-280-ad.gif')}}" width="436" height="380" alt=""/></div>
            @endempty
          </center>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection
  