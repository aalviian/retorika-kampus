@extends('frontend.master')
@section('title', $news->title)
@section('og-url', 'http://www.retorikakampus.com/berita/fokus/'.$highlight_slug.'/'.$news->slug)
@section('og-type', 'http://www.retorikakampus.com')
@section('og-title', $news->title)
@section('og-caption', $news->title)
@section('og-description', $news->content)
@section('og-image', $news->photo_path)
@section('ads1')

@isset($ads1)
<div class="col-sm-8 col-md-8 wow fadeInUpLeft animated">
  <a href="{{$ads1->url}}">
    <img class="img-responsive pull-right" src="{{ url('/img/data/advertisement/'.$ads1->image ) }}" width="468"/>
  </a>   
</div> 
@endisset

@empty($ads1)
<div class="col-sm-8 col-md-8 pull-right">
  <img class=" pull-right" src="{{ url('images/ads/468-60-ad.gif') }}" width="468"/>
</div> 
@endempty

@endsection
@section('content') 

<div class="container ">
  <div class="row "> 
    <!-- left sec start -->
    <div class="col-md-10 col-sm-10">
      <div class="row">
        <!-- hot news start -->
        <div class="col-sm-16 hot-news hidden-xs">
          <div class="row">
            <div class="col-sm-15"> <i class="fa fa-clock-o icon-news pull-left"></i>
              <ul id="js-news" class="js-hidden">
                @forelse($recent_news as $f_recent_news)
                  <li class="news-item"><a href="{{ url('berita/rubrik/'.$f_recent_news->rubric->slug.'/'.$f_recent_news->slug) }}">{{ $f_recent_news->title }}</a></li>
                @empty
                <center>
                  <h3>Berita belum tersedia</h3>
                </center>
                @endforelse
              </ul> 
            </div>
          </div>
        </div>
        <!-- hot news end -->
        <style>
        html {
          margin: 40px auto;
        }
        .btn-twitter {
            background: #00ACEE;
            border-radius: 0;
            color: #fff;
            border-width: 1px;
            border-style: solid;
            border-color: #0075a2;
          }
          .btn-twitter:link, .btn-twitter:visited {
            color: #fff; 
          }
          .btn-twitter:active, .btn-twitter:hover {
            background: #0075a2;
            color: #fff; 
          }
          .imagees{
          position: relative;
          display: block;
          background: #000;
          height: auto;
          width: 100%;
            }
          .post-wrapper .imagees img{
          position: relative;
          top: 0;
          right: 0;
          left: 0;
          bottom: 0;
          margin: 0 auto;
          display: flex;
          text-align: center;
          vertical-align: middle;
          width: 100%;
          height: auto;
          }
         .post-content p{
             margin: 0;
             text-align: justify;
         }
        .chat
        {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .chat li
        {
            margin-bottom: 10px;
            padding-bottom: 5px;
            border-bottom: 1px dotted #B3A9A9;
        }

        .chat li.left .chat-body
        {
            margin-left: 60px;
        }

        .chat li.right .chat-body
        {
            margin-right: 60px;
        }


        .chat li .chat-body p
        {
            margin: 0;
            color: #777777;
        }

        .panel .slidedown .glyphicon, .chat .glyphicon
        {
            margin-right: 5px;
        }

        .panel-body
        {
            overflow-y: scroll;
            height: 250px;
        }

        ::-webkit-scrollbar-track
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        ::-webkit-scrollbar
        {
            width: 12px;
            background-color: #F5F5F5;
        }

        ::-webkit-scrollbar-thumb
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #555;
        }
      </style>
        <!-- News start -->
        <div  class="col-sm-16 banner-outer wow fadeInLeft animated" data-wow-delay="1s" data-wow-offset="50">
          <div class="row">
              <h1>{{$news->title}}
                @if(Auth::check())
                  @if(Auth::user()->id == $news->created_by or Auth::user()->id == 1)
                    <a href="{{ route('news.edit', $news->id) }}" class = "btn btn-sm btn-success pull-right">Edit</a>
                  @endif
                @endif
              </h1>
              <p>
                @if(!empty($user))
                  <span class="fa fa-pencil">&nbsp;{{ $user->name }}&nbsp;&nbsp;</span>
                @else
                  <span class="fa fa-pencil">&nbsp;Unkown&nbsp;&nbsp;</span>
                @endif
                <span class="fa fa-calendar">&nbsp;{{$news->created_at}}&nbsp;&nbsp;</span>
                <span class="fa fa-eye">&nbsp;{{$news->views}}&nbsp;&nbsp;</span>
              </p>
              <br>
              <center>
                <figure class="imagees">
                      <img class="img-responsive" src="{{ $news->photo_path }}" width="800" height="600" alt=""/>
                  </center>
                </figure>
              </center>
              <br>Sumber gambar : {{ $news->image_source }}</br>
              <br>
              <div class="post-content clearfix">
                {!! $news->content !!}
              </div>
              <br>
              <div class="sub-info">
                <a href="{{url('berita/rubrik/'.$news->rubric->slug)}}">
                  <span class="fa fa-paper-plane-o">&nbsp;{{ $news->rubric->name }}&nbsp;&nbsp;</span>
                </a>
              </div>
              <div class="sub-info">
                @foreach($news->highlight as $news_highlight)
                  <span class="fa fa-paw">
                    <a href="{{url('berita/fokus/'.$news_highlight->slug)}}">
                      {{ $news_highlight->name }},&nbsp;&nbsp;
                    </a>
                  </span>
                @endforeach
              </div>
          </div>
          <hr>
        </div>
        <div id="fb-root"></div>
        <div class="col-md-12">
          <div class="col-md-3">
            <div class="fb-share-button" id="share_button" data-href="http://www.retorikakampus.com/berita/rubrik/{{$news->rubric->slug}}/{{$news->slug}}" data-layout="box_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Bagikan</a></div>
            <!-- banner outer end -->
              {{-- <div class="fb-like" data-share="true" data-width="450"></div> --}}
          </div>
          <div class="col-md-3">
            <a class="btn-twitter" width="30" height="30" href="https://twitter.com/intent/tweet?text={{$news->title}}. Link: http://www.retorikakampus.com/berita/rubrik/{{$news->rubric->slug}}/{{$news->slug}} . Via @RetorikaKampus #RetorikaKampus" data-size="large"><i class="fa fa-twitter fa-fw"></i>
            Tweet</a>
          </div>
          <div class="col-md-5">
              <div class="line-it-button" data-lang="en" data-type="like" data-url="http://www.retorikakampus.com/berita/rubrik/{{$news->rubric->slug}}/{{$news->slug}}" data-share="true" style="display: none;"></div>
              <script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>     
          </div>
          <div class="col-md-3">
              <div class="line-it-button" data-lang="en" data-type="share-a" data-url="http://www.retorikakampus.com/berita/rubrik/{{$news->rubric->slug}}/{{$news->slug}}" style="display: none;"></div>
              <script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>     
          </div>
        </div>
        <!-- banner outer end -->
        <br><br>
        <!--wide ad start-->
        <center>
          @isset($ads5)
            <div class="col-sm-16 wow fadeInDown animated"  width="728" height="90"  data-wow-delay="0.5s" data-wow-offset="25">
              <a href="{{$ads5->url}}">
                <img class= "img-responsive" src="{{url('/img/data/advertisement/'.$ads5->image)}}" alt=""/>
              </a>
              <hr>
            </div> 
          @endisset

          @empty($ads5)
            <div class="col-sm-16 wow fadeInDown animated " data-wow-delay="0.5s" data-wow-offset="25">
              <img class="img-responsive" src="{{url('images/ads/728-90-ad.gif')}}" width="728" height="90" alt=""/>
            <hr>
            </div>
          @endempty
        </center>
        <!--wide ad end--> 
        <!-- Comments start -->
        <div class="col-sm-16">
          <div class="row">
            <div class="col-xs-16 wow fadeInLeft animated science" data-wow-delay="0.5s" data-wow-offset="130">
              <div class="main-title-outer pull-left">
                <div class="main-title">Komentar</div>
              </div><br><br>
              <div class="row"><br><br>
                <div class="panel">
                  @forelse ($comments as $comment)
                  <ul class="chat">
                    <li class="left clearfix"><span class="chat-img pull-left">
                        <img src="{{ $comment->photo_path }}" alt="User Avatar" class="img-circle" />
                    </span>
                        <div class="chat-body clearfix">
                            <div class="header">
                                <strong class="primary-font">&nbsp;&nbsp;&nbsp;{{ $comment -> name }}</strong> <small class="pull-right text-muted">
                                    <span class="glyphicon glyphicon-time"></span>{{ $comment -> created_at}}</small>
                            </div>
                            <p>&nbsp;&nbsp;&nbsp;
                                {{ $comment -> message }}
                            </p>
                        </div>
                    </li>
                  </ul>
                  @empty
                    <center><h2>:(</h2><br><h5>Tidak ada komentar</h5></center>
                  @endforelse
                </div>
              </div>
              <div class="row">
                <div class="col-sm-16">
                  {!! Form::open(['route'=>'comment.store', 'method'=>'post']) !!}
                  {!! Form::text('slug', $news->rubric->slug, ['class' =>'hidden']) !!}
                  {!! Form::number('news_id', $news->id, ['class' =>'hidden']) !!}
                    <div class= " form-group { !! $errors->has('name') ? 'has-error' : '' !!}">
                      {!! Form::label('name', 'Nama') !!}
                      {!! Form::text('name', null, ['class' =>'form-control', 'placeholder' => 'Ketik nama anda...']) !!}
                      {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class= " form-group { !! $errors->has('email') ? 'has-error' : '' !!}">
                      {!! Form::label('email', 'Email') !!}
                      {!! Form::text('email', null, ['class' =>'form-control', 'placeholder' => 'Ketik email anda...']) !!}
                      {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class= " form-group { !! $errors->has('message') ? 'has-error' : '' !!}">
                      {!! Form::label('message', 'Komentar') !!}
                      {!! Form::textarea('message', null, ['class' =>'form-control', 'placeholder' => 'Ketik komentar anda...']) !!}
                      {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
                    </div>
                    {!! Form::submit( 'Kirim', [ 'class'=> 'btn btn-primary pull-right']) !!}
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
          <hr>
        </div>
        <!-- Comments end -->
        <!-- Berita terkait start -->
        <div class="col-sm-16">
          <div class="row">
            <div class="col-xs-16 wow fadeInLeft animated science" data-wow-delay="0.5s" data-wow-offset="130">
              <div class="main-title-outer pull-left">
                <div class="main-title">Berita Terkait</div>
              </div>
              <div class="row">
                <div class="col-sm-16">
                  <ul class="list-unstyled  top-bordered ex-top-padding">
                    @forelse($highlight_news as $f_highlight_news)
                    <li> <a href="{{url('berita/fokus/'.$highlight_slug.'/'.$f_highlight_news->slug)}}">
                      <div class="row">
                        <div class="col-lg-3 col-md-4 hidden-sm  ">
                          <img width="150" height="150" alt="" src="{{$f_highlight_news->photo_path}}" class="img-thumbnail pull-left">
                        </div>
                        <div class="col-lg-13 col-md-12">
                          <h3>{{$f_highlight_news->title}}</h3>
                          <div class='content'>
                            {!! str_limit(strip_tags($f_highlight_news->content), $limit=150, $end='...' ) !!}
                          </div>
                          <div class="text-danger sub-info">
                            <div class="time"><span class="fa fa-calendar"></span>&nbsp;&nbsp;{{$f_highlight_news->created_at}}</div>
                            <div class="comments">
                              <span class="fa fa-eye"></span>{{$f_highlight_news->views}}
                            </div>
                          </div>
                        </div>
                      </div>
                      </a> </li>
                      @empty
                      <center>
                        <h1>:(</h1>
                        <br>
                        <h3>Berita belum tersedia</h3>
                      </center>
                      @endforelse
                  </ul>
                  {{ $highlight_news->links() }}
                </div>
              </div>
            </div>
          </div>
          <hr>
        </div>
        <!-- Berita terkait end --> 


      </div>
    </div>
    <!-- left sec end --> 

    <!-- right sec start -->
    <div class="col-sm-6 right-sec">
      <div class="bordered top-margin">
        <div class="row ">
          <center>
            @isset($ads2)
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50"> 
              <a href="{{$ads2->url}}">
                <img class="img-responsive" src="{{ url('/img/data/advertisement/'.$ads2->image ) }}" width="336" height="280" alt=""/>
              </a>
            </div>
            @endisset

            @empty($ads2)
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50"> <img class="img-responsive" src="{{url('images/ads/336-280-ad.gif')}}" width="436" height="380" alt=""/></div>
            @endempty

            @isset($ads3)
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50">
              <a href="{{$ads3->url}}">
                <img class="img-responsive" src="{{ url('/img/data/advertisement/'.$ads3->image ) }}" width="336" height="280" alt=""/>
              </a>
            </div>
            @endisset

            @empty($ads3)
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50"> <img class="img-responsive" src="{{url('images/ads/336-280-ad.gif')}}" width="436" height="380" alt=""/> </div>
            @endempty
          </center>
        </div>
      </div>
      <div class="col-sm-16 bt-spac wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="150">
        <div class="table-responsive">
          <a href="#" class="sponsored">Fokus</a> 
          <table class="table table-bordered social">
            <tbody>
              <tr>
                @php $key=1; @endphp
                @forelse($highlights as $highlight)
                  @if($key % 4 != 0)
                  <td>
                    <a class="rss" href="{{ url('berita/fokus/'.$highlight->slug) }}">
                      <p><img src="{{ $highlight->photo_path }}" width="20" height="20"><br>{{ $highlight->name }}</p>
                    </a> 
                  </td>
                  @php $key++; @endphp
                  @endif
                  @if($key % 4 == 0)
                    <tr></tr>
                    @php $key++; @endphp
                  @endif
                @empty
                  <center><h2>:(</h2><br><h5>Tidak ada komentar</h5></center>
                  <p>Fokus tidak tersedia</p>
                @endforelse
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="130"> 
        <ul class="nav nav-tabs nav-justified " role="tablist">
          <li class="active"><a href="#popular" role="tab" data-toggle="tab">populer</a></li>
          <li><a href="#recent" role="tab" data-toggle="tab">terbaru</a></li>
        </ul>

        <div class="tab-content">
          <!-- Start Popular -->
          <div class="tab-pane active" id="popular">
            <ul class="list-unstyled">
              @forelse($popular_news as $f_popular_news)
                <li>
                  <a href="{{ url('berita/rubrik/'.$f_popular_news->rubric->slug.'/'.$f_popular_news->slug)}}">
                    <div class="row">
                      <div class="col-sm-5  col-md-4 ">
                        <img class="img-thumbnail pull-left" src="{{ $f_popular_news->photo_path }}" width="164" height="152" alt=""/>
                      </div>
                      <div class="col-sm-11  col-md-12 ">
                        <h4>{{ $f_popular_news->title }}</h4>
                        <div class="text-danger sub-info">
                          <div class="time">
                            <span class="fa fa-calendar"></span>&nbsp;&nbsp;{{ $f_popular_news->created_at }}
                          </div>
                          <div class="comments">
                            <span class="fa fa-eye"></span>&nbsp;&nbsp;{{ $f_popular_news->views }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
                </li>
              @empty
              <p><b><center><h3>:(</h3></center></b></p>
              <p>Popular News tidak tersedia</p>
              @endforelse
            </ul>
          </div>
          <!-- End Popular -->

          <!-- Start Recent News -->
          <div class="tab-pane" id="recent">
            <ul class="list-unstyled">
              @forelse($recent_news as $f_recent_news)
                <li>
                  <a href="{{ url('berita/rubrik/'.$f_recent_news->rubric->slug.'/'.$f_recent_news->slug)}}">
                    <div class="row">
                      <div class="col-sm-5  col-md-4 ">
                        <img class="img-thumbnail pull-left" src="{{ $f_recent_news->photo_path }}" width="164" height="152" alt=""/>
                      </div>
                      <div class="col-sm-11  col-md-12 ">
                        <h4>{{ $f_recent_news->title }}</h4>
                        <div class="text-danger sub-info">
                          <div class="time">
                            <span class="fa fa-calendar"></span>&nbsp;&nbsp;{{ $f_recent_news->created_at }}
                          </div>
                          <div class="comments">
                            <span class="fa fa-paper-plane-o"></span>&nbsp;&nbsp;{{ $f_recent_news->rubric->name }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
                </li>
              @empty
              <p><b><center><h3>:(</h3></center></b></p>
              <p>Recent News tidak tersedia</p>
              @endforelse
            </ul>
          </div>
          <!-- End Recent News -->
        </div>
      </div>
      <div class="bordered top-margin">
        <div class="row ">
          <center>
            @isset($ads4)
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50"> 
              <a href="{{$ads4->url}}">
                <img class="img-responsive" src="{{ url('/img/data/advertisement/'.$ads4->image ) }}" width="336" height="280" alt=""/>
              </a>
            </div>
            @endisset

            @empty($ads4)
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50"> <img class="img-responsive" src="{{url('images/ads/336-280-ad.gif')}}" width="436" height="380" alt=""/></div>
            @endempty
          </center>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
    FB.init({appId: '110638142937641', status: true, cookie: true,
    xfbml: true, version: 'v2.10'});
    };
    (function() {
    var e = document.createElement('script'); e.async = true;
    e.src = document.location.protocol +
    '//connect.facebook.net/en_US/all.js';
    document.getElementById('fb-root').appendChild(e);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#share_button').click(function(e){
        e.preventDefault();
        FB.ui(
        {
        method: 'feed',
        name: 'This is the content of the "name" field.',
        link: 'http://www.groupstudy.in/articlePost.php?id=A_111213073144',
        picture: 'http://www.groupstudy.in/img/logo3.jpeg',
        caption: 'Top 3 reasons why you should care about your finance',
        description: "What happens when you don't take care of your finances? Just look at our country -- you spend irresponsibly, get in debt up to your eyeballs, and stress about how you're going to make ends meet. The difference is that you don't have a glut of taxpayers…",
        message: ""
        });
    });
});
</script>
@endsection
  