@extends('layouts.app2')
@section('title', 'Profile') 
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/cropper/cropper.min.css') }}"/>
@endsection
@section('menu', 'profile')
{!! $errors->first('avatar', '<p class="help-block">:message</p>') !!}
@section('content')                        
    <div class="col-md-6 col-sm-4 col-xs-5">
        <div class="panel panel-default">   
            <div class="panel-heading">
                <h3>Online</h3>
            </div>                              
            <div class="panel-body">
                @foreach($onlines as $online)
                   <img src="{{ url('img/data/online.png') }}" height="10" width="10"></img> {{ $online->name }} </br>
                @endforeach
            </div>
        </div>
        <br>
    </div>
    <div class="col-md-6 col-sm-8 col-xs-7">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Log History</h3>
            </div>                              
            <div class="panel-body">
                @foreach($logs as $log)
                    @if($log->is_login == 1)
                        <img src="{{ $log->photo_path }}" width="15" height="15" /> {{ $log->name }} sedang online - {{ $log->login_date }}</br></br>
                    @else
                        <img src="{{ $log->photo_path }}" width="15" height="15" /> {{ $log->name }} telah keluar/logout - {{ $log->logout_date }}</br></br>
                    @endif
                @endforeach
            </div>
        </div>
        <br>     
    </div>
    
    <!-- EOF MODALS -->
@endsection
@section('script')
    <script type='text/javascript' src='{{ asset('js/plugins/icheck/icheck.min.js') }}'></script>
    <script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>  
    <script type="text/javascript" src="{{ asset('js/plugins/bootstrap/bootstrap-file-input.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/form/jquery.form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/fileinput/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/actions.js') }}"></script>
    <script>
        $("#file-simple").fileinput({
                showUpload: false,
                showCaption: false,
                browseClass: "btn btn-danger",
                fileType: "any"
        });  
    </script>
@endsection