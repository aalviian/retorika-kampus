@extends('layouts.app2')
@section('title', 'Home')
@section('content')
@unless(Auth::user()->level_id == 4 or Auth::user() -> level_id == 3)
	<div class="col-md-3">
	    <div class="widget widget-default widget-item-icon">
            <div>                
                <div class="widget-item-left">
	                <span class="fa fa-envelope"></span>
	            </div>                     
		        <div class="widget-data">
		         	<div class="widget-title">Total News</div>
		            <div class="widget-int">{{ $news }}</div>
		        </div> 
            </div>                              
	    </div>    
	</div>
	<div class="col-md-3">
	    <div class="widget widget-default widget-item-icon">
	        <div class="widget-item-left">
	            <span class="fa fa-user"></span>
	        </div>                             
	        <div class="widget-data">
                <div class="widget-title">Total Member</div>                          
                <div class="widget-int">{{ count($users) }}</div>
			</div>  
		</div>  
	</div>
	<div class="col-md-3">
		<div class="widget widget-default widget-item-icon">
	    	<div class="widget-item-left">
	        	<span class="fa fa-user"></span>
			</div>                             
	        <div class="widget-data">
            	<div class="widget-title">Total Pengunjung</div>
            	<div class="widget-int">{{ $visitors }}</div>
			</div>  
		</div>  
	</div>
	<div class="col-md-3">
		<div class="widget widget-danger widget-padding-sm">
	    	<div class="widget-big-int plugin-clock">00:00</div>                           
			<div class="widget-subtitle plugin-date">Loading...</div>
			<div class="widget-controls">                                
				<a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="left" title="Remove Widget"><span class="fa fa-times"></span></a>
			</div>                            
			<div class="widget-buttons widget-c3">
				<div class="col">
					<a href="#"><span class="fa fa-clock-o"></span></a>
			    </div>
				<div class="col">
					<a href="#"><span class="fa fa-bell"></span></a>
			    </div>
				<div class="col">
					<a href="#"><span class="fa fa-calendar"></span></a>
				</div>
			</div>                            
		</div>
	</div>
	<br>
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Grafik Pengunjung Portal</h3>
				{!! Form::open(['url' => 'home/', 'method' => 'post']) !!}
				{!! Form::text('logRubricsType', $logRubricsType,['class'=>'hidden']) !!}
                {!! Form::select('logVisitType', [ 'hourly' => 'Hourly', 'daily' => 'Daily', 'weekly' => 'Weekly','monthly' => 'Monthly' ], $logVisitType,['onchange' => 'this.form.submit()', 'class'=>'form-control js-selectize']) !!}
                {!! Form::close() !!}
			</div>
			<div class="panel-body">
				<div id="chartContainer" style="height: 300px;"><svg></svg></div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Grafik Pengunjung per Rubrik</h3>
				{!! Form::open(['url' => 'home/', 'method' => 'post']) !!}
                {!! Form::text('logVisitType', $logVisitType,['class'=>'hidden']) !!}
                {!! Form::select('logRubricsType', [ 'today' => 'Today', 'thisweek' => 'This Week', 'thismonth' => 'This Month' ], $logRubricsType,['onchange' => 'this.form.submit()', 'class'=>'form-control js-selectize']) !!}
                {!! Form::close() !!}
			</div>
			<div class="panel-body">
				<div id="chartContainer2" style="height: 300px;"><svg></svg></div>
			</div>
		</div>
	</div>
	<br>
	<div class="col-md-12"> 
		<br>
		<br>
		<div class="row">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Jumlah Berita</h3>
				</div>
				<div class="panel-body">
					<br><br>
					<div class="panel panel-default tab"> 
						<ul class="nav nav-tabs" role="tablist">
						    <li role="presentation"><a href="#news" aria-controls="news" role="tab" data-toggle="tab">News</a></li>
						    <li role="presentation"><a href="#redaktur" aria-controls="redaktur" role="tab" data-toggle="tab">Redaktur</a></li>
						    <li role="presentation"><a href="#reporter" aria-controls="reporter" role="tab" data-toggle="tab">Reporter</a></li>
					 	</ul> 

					  	<div class="tab-content">
						    <div role="tabpanel" class="tab-pane panel-body active" id="news">
					            <h3> Keseluruhan</h3>
					            <div class="table-responsive">
						            <table class= "table table-hover">
						                <thead>
						                    <tr>
						                    	<th>Name</th>
						                    	<th>Username</th>
						                    	<th>News</th>
						                    </tr> 
						                </thead>
						                <tbody>
						                    @foreach( $cek as $f_userNews )
						                    	<tr>
						                    		<td>{{ $f_userNews['name'] }}</td>
						                    		<td>{{ $f_userNews['username'] }}</td>
						                    		<td>{{ $f_userNews['count'] }}</td>
						                    	</tr>
						                    @endforeach
						                </tbody>
						            </table>
						        </div>
						    </div>
						    <div role="tabpanel" class="tab-pane panel-body" id="redaktur">
					            <h3> Redaktur</h3>
					            <div class="table-responsive">
						            <table class= "table table-hover">
						                <thead>
						                    <tr>
						                    	<th>Name</th>
						                    	<th>Username</th>
						                    	<th>News</th>
						                    </tr> 
						                </thead>
						                <tbody>
						                    @foreach( $cek as $f_userNews )
						                    	@if($f_userNews['level_id']==2)
							                    	<tr>
							                    		<td>{{ $f_userNews['name'] }}</td>
							                    		<td>{{ $f_userNews['username'] }}</td>
							                    		<td>{{ $f_userNews['count'] }}</td>
							                    	</tr>
						    					@endif
						                    @endforeach
						                </tbody>
						            </table>
						        </div>
						    </div>
						    <div role="tabpanel" class="tab-pane panel-body" id="reporter">
					            <h3> Reporter</h3>
					            <div class="table-responsive">
						            <table class= "table table-hover">
						                <thead>
						                    <tr>
						                    	<th>Name</th>
						                    	<th>Username</th>
						                    	<th>News</th>
						                    </tr> 
						                </thead>
						                <tbody>
						                    @foreach( $cek as $f_userNews )
						                    	@if($f_userNews['level_id']==3)
							                    	<tr>
							                    		<td>{{ $f_userNews['name'] }}</td>
							                    		<td>{{ $f_userNews['username'] }}</td>
							                    		<td>{{ $f_userNews['count'] }}</td>
							                    	</tr>
						    					@endif
						                    @endforeach
						                </tbody>
						            </table>
						        </div>
						    </div>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endunless
@if(Auth::user()->level_id == 4 or Auth::user()->level_id == 3)
	<div class="col-md-6">
	    <div class="widget widget-default widget-item-icon">
            <div>                
                <div class="widget-item-left">
	                <span class="fa fa-envelope"></span>
	            </div>                     
		        <div class="widget-data">
		         	<div class="widget-title">Total News Kamu</div>
		            <div class="widget-int">{{ $news_user }}</div>
		        </div> 
            </div>                              
	    </div>    
	</div>
	<div class="col-md-6">
		<div class="widget widget-danger widget-padding-sm">
	    	<div class="widget-big-int plugin-clock">00:00</div>                           
			<div class="widget-subtitle plugin-date">Loading...</div>
			<div class="widget-controls">                                
				<a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="left" title="Remove Widget"><span class="fa fa-times"></span></a>
			</div>                            
			<div class="widget-buttons widget-c3">
				<div class="col">
					<a href="#"><span class="fa fa-clock-o"></span></a>
			    </div>
				<div class="col">
					<a href="#"><span class="fa fa-bell"></span></a>
			    </div>
				<div class="col">
					<a href="#"><span class="fa fa-calendar"></span></a>
				</div>
			</div>                            
		</div>
	</div>
	<div class="col-md-12">
        <!-- START BAR CHART -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Grafik popularitas 20 berita terakhir</h3>                                
            </div>
            <div class="panel-body">
                <div id="chartContainer3" style="height: 300px;"></div>
            </div>
        </div>
        <!-- END BAR CHART -->
    </div>
@endif
@endsection

@section('script') 
    <script type='text/javascript' src="{{ asset('js/plugins/icheck/icheck.min.js') }}"></script>        
    <script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/scrolltotop/scrolltopcontrol.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/morris/raphael-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/morris/morris.min.js') }}"></script>       
    <script type="text/javascript" src="{{ asset('js/plugins/rickshaw/d3.v3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/rickshaw/rickshaw.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/nvd3/lib/d3.v3.js') }}"></script>        
    <script type="text/javascript" src="{{ asset('js/plugins/nvd3/nv.d3.min.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('js/plugins/owl/owl.carousel.min.js') }}"></script>                 
    <script type="text/javascript" src="{{ asset('js/plugins/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script>      
    <script type="text/javascript" src="{{ asset('js/actions.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/demochart/newsrubric.js') }}"></script>
	<script type="text/javascript">
		var morrisCharts = function() {
		    Morris.Bar({
		        element: 'chartContainer',
		        data: <?php echo json_encode($AccessLogs, JSON_NUMERIC_CHECK); ?>,
		        xkey: 'y',
		        ykeys: ['a'],
		        labels: ['Jumlah'],
		        barColors: ['#3498db']
		    });
		}();
	</script>
	<script type="text/javascript">
		var morrisCharts = function() {
		    Morris.Bar({
		        element: 'chartContainer2',
		        data: <?php echo json_encode($rubricsLogs, JSON_NUMERIC_CHECK); ?>,
		        xkey: 'y',
		        ykeys: ['a'],
		        labels: ['Jumlah'],
		        barColors: ['#3498db']
		    });
		}();
	</script>
	<script type="text/javascript">
		var morrisCharts = function() {
		    Morris.Bar({
		        element: 'chartContainer3',
		        data: <?php echo json_encode($userNewsLogs, JSON_NUMERIC_CHECK); ?>,
		        xkey: 'y',
		        ykeys: ['a'],
		        labels: ['Views'],
		        barColors: ['#3498db']
		    });
		}();
	</script>
@endsection
