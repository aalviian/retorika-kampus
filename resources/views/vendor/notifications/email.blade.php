@component('mail::layout')

@slot('header')
    @component('mail::header', ['url' => 'http://retorika.maisipi.com'])
        Retorika Kampus
    @endcomponent
@endslot

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }} <br>
@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        default:
            $color = 'blue';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

<!-- Salutation -->
@if (! empty($salutation))
{{ $salutation }}
@else
Regards,<br>Retorika Kampus
@endif

<!-- Subcopy -->
@isset($actionText)
@component('mail::subcopy')
If you’re having trouble clicking the "{{ $actionText }}" button, copy and paste the URL below
into your web browser: [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset


{{-- Footer --}}
@slot('footer')
    @component('mail::footer')
        Retorika Kampus - Membuka Cakrawala
    @endcomponent
@endslot

@endcomponent
