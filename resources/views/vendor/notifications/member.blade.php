<body>
Hi, {{ $name }}.
<br>
<b>Congratulation</b>, you have become part of us (Retorika Kampus) as <b>{{ $level }}</b>
<br>
<br>
Berikut data anda: <br>
Name : {{ $name }} <br>
Username : {{ $username }} <br>
Email : <b>{{ $email }}</b> <br>
Password : <b>{{ $password }}</b> <br>
Role : {{ $level }} <br> 
<br>
<br>
<a href="retorikakampus.com/login-page" class="btn btn-success">Click to Login</a>
<br>
We are glad to choose you! Please, don't hesitate to contact us.<br>
<br>
<br>
Sincerely
<br>
Muhammad Alvian Supriadi<br> 
</body>