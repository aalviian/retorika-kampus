@extends('layouts.app2')
@section('title', 'News')
@section('menu', 'news')
@section('content') 
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3> Member  
                <div id ="add-member" class= "btn btn-info btn-sm" >+</div>
                {!! Form::open(['route' => 'member.store', 'id'=>'form-add']) !!}
                    <div class="col-md-5">
                        @include('member.form')
                    </div> 
                {!! Form::close() !!}
            </h3>
        </div>

        <div class="panel-body">
            <div class="table-responsive">
                <table class= "table table-hover">
                    <thead>
                        <tr> 
                            <th >Name</th>
                            <th >Username</th>
                            <th >Email</th> 
                            <th >Role</th>
                            <th >Confirmed</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th colspan="2" style="width:60%"><center>Action</center></th>
                        </tr> 
                    </thead>
                    <tbody>
                        @forelse($users as $key=>$user) 
                        <tr>
                        @php
                            if($user-> level_id == 1) $role = "Admin";
                            elseif($user-> level_id == 2) $role = "Redaktur";
                            elseif($user-> level_id == 3) $role = "Reporter";
                            elseif($user-> level_id == 4) $role = "User";
                        @endphp
                            @php $key = $key+ 1 @endphp
                            <td>{{ $user -> name }}</td>
                            <td>{{ $user -> username }}</td>
                            <td>{{ $user -> email }}</td>
                            <td>{{ $role }}</td>
                            <td>{{ $user-> is_confirmed }}</td>
                            <td>{{ $user -> created_at }}</td>
                            <td>{{ $user -> updated_at }}</td>
                            <td>
                                @if(Auth::user()->level_id == 1)
                                    @if($user -> id != 1)
                                        <center>
                                            <div id='<?php echo $key; ?>' onClick="edit_click(this.id)" class = "btn btn-sm btn-success">Edit</div>
                                            {!! Form::model($user, ['route'=> ['member.update', $user], 'method'=>'patch','id'=>'form-edit'.$key]) !!}
                                                <br>
                                                @include('member.form', ['model'=>$user])
                                            {!! Form::close() !!} 
                                        </center>
                                    @endif
                                @endif
                            </td>
                            <td><center>
                                @if($user->level_id != 1 or Auth::user()->id == 1)
                                {!! Form::model($user, ['route' => ['member.destroy', $user], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                {!! Form::submit('delete', ['class'=>'btn btn-sm btn-danger js-submit-confirm']) !!}
                                {!! Form::close() !!}
                                @endunless
                            </center></td> 
                        </tr>
                        @empty
                            <td colspan="8"><center><h2>:(</h2><p>Member kosong</p></center></td>
                        @endforelse
                    </tbody>
                </table>
                {{ $users->links() }}  
            </div> 
        </div>
    </div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/actions.js') }}"></script> 
<script type="text/javascript">
    $(document).ready( function() {
        document.getElementById("form-add").style.display = "none";

        $( "#add-member" ).click( function() {
            $( "#form-add" ).toggle( 'slow' );
        });

    });
</script>
<script type="text/javascript">
    for (var i=1; i<=' <?php echo($count); ?>'; i++ ){
        document.getElementById("form-edit"+i).style.display = "none";
    }

    function edit_click(id)
    {
        $("#form-edit"+id).toggle( 'slow' );
    }
</script>
@endsection 