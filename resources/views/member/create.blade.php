@extends('layouts.app')
	@section('content')
		<div class= "container">
			<div class= "row">
				<div class= "col-md-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3>New Member</h3>
						</div>
						<div class="panel-body">
							{!! Form::open(['route' => 'member.store', 'files'=>true]) !!}
								@include('member.form')
							{!! Form::close() !!}	
						</div>
					</div>
				</div>
			</div> 
		</div>
	@endsection

	@section('script')
	<script type="text/javascript" src="{{ asset('js/tinymce.min.js') }}"></script> 
	<script type="text/javascript" src="{{ asset('js/actions.js') }}"></script> 
	<script type="text/javascript">
	tinymce.init({
	    selector : "textarea",
	    plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],

	    toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",

	    image_advtab: true ,
	});
	</script>
	@endsection 