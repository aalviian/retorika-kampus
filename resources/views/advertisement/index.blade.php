@extends('layouts.app2')
@section('title', 'advertisements')
@section('menu', 'advertisements')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Advertisements
            </h3>
            <div class="btn-group pull-right">
                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
                <ul class="dropdown-menu">
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'false'});"><img src='img/icons/json.png' width="24"/> JSON</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'false',ignoreColumn:'[2,3]'});"><img src='img/icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'true'});"><img src='img/icons/json.png' width="24"/> JSON (with Escape)</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'xml',escape:'false'});"><img src='img/icons/xml.png' width="24"/> XML</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'sql'});"><img src='img/icons/sql.png' width="24"/> SQL</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'csv',escape:'false'});"><img src='img/icons/csv.png' width="24"/> CSV</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'txt',escape:'false'});"><img src='img/icons/txt.png' width="24"/> TXT</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'excel',escape:'false'});"><img src='img/icons/xls.png' width="24"/> XLS</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'doc',escape:'false'});"><img src='img/icons/word.png' width="24"/> Word</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'powerpoint',escape:'false'});"><img src='img/icons/ppt.png' width="24"/> PowerPoint</a></li>
                    <li class="divider"></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'png',escape:'false'});"><img src='img/icons/png.png' width="24"/> PNG</a></li>
                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'pdf',escape:'false'});"><img src='img/icons/pdf.png' width="24"/> PDF</a></li>
                </ul>
            </div>                                     
        </div>
        <div class="panel-body">
            <span>Tambah: 
                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalCreate">
                  +
                </button>                
                <div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                      </div>
                      {!! Form::open(['route' => 'advertisement.store', 'files'=>true]) !!}
                          <div class="modal-body">
                            @include('advertisement.form')
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                          </div>
                      {!! Form::close() !!} 
                    </div>
                  </div>
                </div>
            <span>
            <div class="table-responsive"> 
                <table id="customers2" class="table datatable">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Image</th>
                            <th>Url</th>
                            <th>Type</th>
                            <th>Order</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th colspan="2"><center>Action</center></th>
                        </tr>
                    </thead>
                    <tbody> 
                        @forelse($advertisements as $key=>$advertisement)
                        <tr>
                            @php $key = $key+ 1 @endphp
                            <td>{{ $advertisement -> title }}</td>
                            <td><img src="{{ $advertisement -> photo_path }}" alt="..." width="50" height="50" class="img-responsive"></td>
                            <td>{{ $advertisement -> url }}</td>
                            <td>{{ $advertisement -> type }}</td>
                            <td>{{ $advertisement -> order }}</td>
                            <td>{{ $advertisement -> created_at }}</td>
                            <td>{{ $advertisement -> updated_at }}</td>
                            <td>
                                {{-- <a href="{{ route('advertisement.edit', $advertisement->id) }}" class = "btn btn-sm btn-success">Edit</a> --}}
                                <button type="button" class="btn btn-primary btn-sm" id='<?php echo $key; ?>' onClick="edit_click(this.id)"'>Edit</button>               
                                <div class="modal fade" id="modalEdit{{$key}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Edit advertisement</h4>
                                      </div>
                                      {!! Form::model($advertisement, ['route'=> ['advertisement.update', $advertisement], 'method'=>'patch', 'files'=>true]) !!}
                                          <div class="modal-body">
                                                @include('advertisement.form')
                                          </div>

                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                          </div>
                                      {!! Form::close() !!}
                                    </div>
                                  </div>
                                </div>
                            </td>
                            <td>
                                {!! Form::model($advertisement, ['route' => ['advertisement.destroy', $advertisement], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                {!! Form::submit('delete', ['class'=>'btn btn-sm btn-danger js-submit-confirm']) !!}
                                {!! Form::close() !!}
                            </td> 
                        </tr>
                        @empty
                        <tr>
                        <td colspan="8">
                        <center><h2>:(</h2><br>Daftar iklan masih kosong</center>
                        </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table> 
                {{ $advertisements->links() }}                                   
            </div>
        </div>
    </div>
@endsection

@section('script')
<script type='text/javascript' src="{{ asset('js/plugins/icheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/tableexport/tableExport.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/tableexport/jquery.base64.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/tableexport/html2canvas.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/tableexport/jspdf/libs/sprintf.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/tableexport/jspdf/jspdf.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/tableexport/jspdf/libs/base64.js') }}"></script>  
<script type="text/javascript" src="{{ asset('js/actions.js') }}"></script> 
<script type="text/javascript">
    function edit_click(id)
    {
        $("#modalEdit"+id).modal();
    }
</script>
@endsection 