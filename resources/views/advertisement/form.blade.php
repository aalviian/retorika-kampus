<div class= " form-group { !! $errors->has('title') ? 'has-error' : '' !!}">
	{!! Form::label('title', 'Title') !!}
	{!! Form::text('title',  null, ['class' =>'form-control', 'placeholder' => 'Ketik judul iklan...']) !!}
	{!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>

<div class= " form-group {!! $errors->has('type') ? 'has-error' : '' !!}">
	{!! Form::label('type', 'Type') !!}
	{!! Form::select('type', ['depan' => 'Depan','rubrik' => 'Rubrik','berita' => 'Berita', 'fokus' => 'Fokus'], null, ['class' =>'form-control js-selectize']) !!}
	{!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div> 

<div class= " form-group { !! $errors->has('order') ? 'has-error' : '' !!}">
	{!! Form::label('order', 'Order') !!}
	{!! Form::number('order',  null, ['class' =>'form-control','onkeypress'=>'return event.charCode >= 48', 'min'=>'1']) !!}
	{!! $errors->first('order', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('image') ? 'has-error' : '' !!}">
	{!! Form::label('image', 'Image ( jpg,jpeg,png )') !!}
	{!! Form::file('image') !!}
	{!! $errors->first('image', '<p class="help-block">:message</p>')  !!}
	@if (isset($model) && $model->image !== '' )
	<div class= "row">
		<div class= "col-md-6">
			<br>
			<p>Current iamge:</p>
			<div class= "thumbnail">
				<img src = "{{ url('/img/highlight/' . $model->image) }}" class="img-rounded" width="200" height="200">
			</div>
		</div>
	</div>
	@endif
	@if (isset($model) && $model->image == '')
	<div class= "row">
		<div class= "col-md-6">
			<br>
			<p>Current image:</p>
			<div class= "thumbnail">
				<img src = "{{ url('/img/data/advertisement/default.png') }}" class="img-rounded" width="200" height="200">
			</div>
		</div>
	</div>
	@endif
</div>

<div class= " form-group { !! $errors->has('url') ? 'has-error' : '' !!}">
	{!! Form::label('url', 'Url') !!}
	{!! Form::text('url',  null, ['class' =>'form-control', 'placeholder' => 'Contoh: http://www.iklanku.com']) !!}
	{!! $errors->first('url', '<p class="help-block">:message</p>') !!}
</div>

<div class= " form-group { !! $errors->has('description') ? 'has-error' : '' !!}">
	{!! Form::label('description', 'Description') !!}
	{!! Form::text('description',  null, ['class' =>'form-control', 'placeholder' => 'Tuliskan deskripsi iklan..']) !!}
	{!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>