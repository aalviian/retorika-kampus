<!DOCTYPE HTML>
<html>
<head>
<title>Poses-404 Website Template | Home :: W3layouts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="{{ asset('css/404.css') }}">
</head>
<body>
<div class="wrap">
    <h1>Retorika Kampus</h1>
    <div class="banner">
        <img src="{{ asset('images/404/banner.png') }}" alt="" />
    </div>
    <div class="page">
        <h2>Dude,,we can't find that page!</h2>
    </div>
    <div class="footer">
    </div>
</div>
</body>
</html>

