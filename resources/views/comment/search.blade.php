@extends('layouts.app2')
@section('title', 'News')
@section('menu', 'news')
@section('content') 
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3> Comments List
                <div class="xn-search pull-right">

                {!! Form::open(['url' => 'pencarian/komentar', 'method' => 'get']) !!}

                    <input type="text" name="query"  autocomplete="off" placeholder="Search..."/>

                {!! Form::close() !!}

                </div>
            </h3>
            
        </div>

        <div class="panel-body">
            <div class="table-responsive">
                <table class= "table table-hover">
                    <thead>
                        <tr> 
                            <th style="width:5%">Berita</th>
                            <th style="width:10%">Name</th>
                            <th style="width:5%">Email</th>
                            <th style="width:20%">Message</th> 
                            <th>Created at</th>
                            <th style="width:40%" colspan="2"><center>Action</center></th>
                        </tr> 
                    </thead>
                    <tbody>
                        @forelse($comments as $key=>$comment) 
                        <tr>
                            @php $key = $key+ 1 @endphp
                            <td>{{ $comment -> news -> title }}</td>
                            <td>{{ $comment -> name }}</td>
                            <td>{{ $comment -> email }}</td>
                            <td>{{ $comment -> message }}</td>
                            <td>{{ $comment -> created_at }}</td>
                            <td><center>
                               <a href="{{url('berita/rubrik/'.$comment->news->rubric->slug.'/'.$comment -> news -> slug)}}" class="btn btn-sm btn-info">View</a> 
                            </center></td>
                            <td><center>
                                {!! Form::model($comment, ['route' => ['comment.destroy', $comment], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                {!! Form::submit('delete', ['class'=>'btn btn-sm btn-danger js-submit-confirm']) !!}
                                {!! Form::close() !!}
                            </center></td>   
                        </tr>
                        @empty
                            <td colspan="6"><center><h2>:(</h2><p>Komentar kosong</p></center></td>
                        @endforelse
                    </tbody>
                </table>
                {{ $comments->appends(['query'=>$query])->links() }}
            </div> 
        </div>
    </div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/actions.js') }}"></script> 
<script type="text/javascript">
    $(document).ready( function() {
        document.getElementById("form-add").style.display = "none";

        $( "#add-member" ).click( function() {
            $( "#form-add" ).toggle( 'slow' );
        });

    });
</script>
<script type="text/javascript">
    for (var i=1; i<=' <?php echo($count); ?>'; i++ ){
        document.getElementById("form-edit"+i).style.display = "none";
    }

    function edit_click(id)
    {
        $("#form-edit"+id).toggle( 'slow' );
    }
</script>
@endsection 