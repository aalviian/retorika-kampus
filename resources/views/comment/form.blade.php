<div class= " form-group { !! $errors->has('name') ? 'has-error' : '' !!}">
	{!! Form::label('name', 'Full Name') !!}
	{!! Form::text('name', null, ['class' =>'form-control', 'placeholder' => 'Ketik nama lengkap...']) !!}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div> 

<div class= " form-group { !! $errors->has('Email') ? 'has-error' : '' !!}">
	{!! Form::label('email', 'Email') !!}
	{!! Form::email('email', null, ['class' =>'form-control', 'placeholder' => 'Ketik email valid...']) !!}
	{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>

<div class= " form-group { !! $errors->has('phone') ? 'has-error' : '' !!}">
	{!! Form::label('phone', 'Phone') !!}
	{!! Form::number('phone', null, ['class' =>'form-control', 'onkeypress'=>"return event.charCode >= 48", 'placeholder' => 'Contoh: 87808125863 atau 81265738934']) !!}
	{!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>

@isset($model)
<div class= " form-group { !! $errors->has('username') ? 'has-error' : '' !!}">
	{!! Form::label('username', 'Username') !!}
	{!! Form::text('username', null, ['class' =>'form-control', 'placeholder' => 'Ketik username...']) !!}
	{!! $errors->first('username', '<p class="help-block">:message</p>') !!}
</div>
@endisset

<div class= " form-group { !! $errors->has('level') ? 'has-error' : '' !!}">
	{!! Form::label('level_id', 'Role') !!}
	{!! Form::select('level_id', ['' => ''] +App\Level::pluck('name', 'id')->all(), isset($model) ? $model->level_id : null, ['class' =>'form-control js-selectize']) !!}
	{!! $errors->first('level', '<p class="help-block">:message</p>') !!}
</div>

{!! Form::submit( isset( $model) ? 'Update' : 'Save', [ 'class'=> 'btn btn-primary']) !!}