@extends('layouts.app')
	@section('content')
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3>Edit member '{{ $member->name }}'</h3>
						</div>
						<div class="panel-body">
							{!! Form::model($member, ['route'=> ['member.update', $member], 'method'=>'patch']) !!}
		                        @include('member.form', ['model'=>$member])
		                    {!! Form::close() !!} 
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection