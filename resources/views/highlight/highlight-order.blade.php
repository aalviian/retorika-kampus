@extends('layouts.app2')
@section('title', 'highlight Order')
@section('menu', 'highlight order')
@section('content') 
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3> Order highlight  
            </h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class= "table table-hover">
                    <thead>
                        <tr> 
                            <th style="width:30%">Name</th>
                            <th style="width:20%">Order</th>
                        </tr> 
                    </thead>
                    <tbody>
                    {!! Form::open(['url' => 'highlight-order-save', 'method'=>'post']) !!} 
                        @foreach ($highlights as $highlight) 
                        
                        <tr> 
                            <td>
                                <div class= " form-group { !! $errors->has('name') ? 'has-error' : '' !!}">
                                    {!! Form::label('name' , 'Name') !!}
                                    {!! Form::text('name[]', $highlight->name, ['class' =>'form-control']) !!}
                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </td>
                            <td>
                                <div class= " form-group { !! $errors->has('order') ? 'has-error' : '' !!}">
                                    {!! Form::label('order' , 'Order') !!}
                                    {!! Form::number('order[]', $highlight->order, ['onkeypress'=>'return event.charCode >= 48', 'min'=>'1','class' =>'form-control']) !!}
                                    {!! $errors->first('order', '<p class="help-block">:message</p>') !!}
                                </div> 
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="2"><center>{!! Form::submit('Save', [ 'class'=> 'btn btn-primary']) !!}</center></td>
                        </tr>
                    {!! Form::close() !!}
                    </tbody>
                </table> 
            </div> 
        </div>
    </div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/actions.js') }}"></script> 
@endsection 