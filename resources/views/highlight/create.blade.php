@extends('layouts.app2')
@section('title', 'Highlights')
@section('menu', 'new highlights')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Create Highlight
            </h3>                                 
        </div>
        <div class="panel-body">
           {!! Form::open(['route' => 'highlight.store', 'files'=>true]) !!}
                @include('highlight.form')
            {!! Form::close() !!} 
        </div>
    </div>
@endsection

@section('script')
<script type='text/javascript' src="{{ asset('js/plugins/icheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/actions.js') }}"></script> 
@endsection 