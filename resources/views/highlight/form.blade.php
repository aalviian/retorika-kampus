<div class= " form-group { !! $errors->has('name') ? 'has-error' : '' !!}">
	{!! Form::label('name', 'Name') !!}
	{!! Form::text('name',  null, ['class' =>'form-control', 'placeholder' => 'Ketik nama highlight...']) !!}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('image') ? 'has-error' : '' !!}">
	{!! Form::label('image', 'Highlight Image ( jpg,jpeg,png )') !!}
	{!! Form::file('image') !!}
	{!! $errors->first('image', '<p class="help-block">:message</p>')  !!}
	@if (isset($model) && $model->image !== '' )
	<div class= "row">
		<div class= "col-md-6">
			<br>
			<p>Current iamge:</p>
			<div class= "thumbnail">
				<img src = "{{ url('/img/highlight/' . $model->image) }}" class="img-rounded" width="200" height="200">
			</div>
		</div>
	</div>
	@endif
	@if (isset($model) && $model->image == '')
	<div class= "row">
		<div class= "col-md-6">
			<br>
			<p>Current image:</p>
			<div class= "thumbnail">
				<img src = "{{ url('/img/data/highlight/default.png') }}" class="img-rounded" width="200" height="200">
			</div>
		</div>
	</div>
	@endif
</div>
