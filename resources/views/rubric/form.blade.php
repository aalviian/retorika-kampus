<div class= " form-group { !! $errors->has('title') ? 'has-error' : '' !!}">
	{!! isset( $model) ? '' : Form::label('name' , 'Name') !!}
	{!! Form::text('name', null, ['class' =>'form-control', 'placeholder' => 'Ketik nama rubrik berita...']) !!}
	{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class= " form-group { !! $errors->has('parent_id') ? 'has-error' : '' !!}">
	{!! Form::label('parent_id', 'Parent') !!}
	{!! Form::select('parent_id', ['' => ''] +App\Rubric::where('parent_id', 0)->pluck('name', 'id')->all(), null, ['class' =>'form-control']) !!}
	{!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
</div > 

{!! Form::submit( isset( $model) ? 'Update' : 'Save', [ 'class'=> 'btn btn-primary']) !!}  