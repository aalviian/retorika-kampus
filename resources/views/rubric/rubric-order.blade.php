@extends('layouts.app2')
@section('title', 'Rubric Order')
@section('menu', 'rubric order')
@section('content') 
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3> Order Rubric  
            </h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class= "table table-hover">
                    <thead>
                        <tr> 
                            <th style="width:30%">Name</th>
                            <th style="width:20%">Order</th>
                        </tr> 
                    </thead>
                    <tbody>
                    {!! Form::open(['url' => 'rubric-order-save', 'method'=>'post']) !!} 
                        @foreach ($rubrics as $rubric) 
                        
                        <tr> 
                            <td>
                                <div class= " form-group { !! $errors->has('name') ? 'has-error' : '' !!}">
                                    {!! Form::label('name' , 'Name') !!}
                                    {!! Form::text('name[]', $rubric->name, ['class' =>'form-control']) !!}
                            </td>
                            <td>
                                <div class= " form-group { !! $errors->has('order') ? 'has-error' : '' !!}">
                                    {!! Form::label('order' , 'Order') !!}
                                    {!! Form::text('order[]', $rubric->order, ['class' =>'form-control']) !!}
                                </div> 
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="2"><center>{!! Form::submit('Save', [ 'class'=> 'btn btn-primary']) !!}</center></td>
                        </tr>
                    {!! Form::close() !!}
                    </tbody>
                </table> 
            </div> 
        </div>
    </div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/actions.js') }}"></script> 
@endsection 