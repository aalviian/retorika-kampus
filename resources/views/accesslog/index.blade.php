@extends('layouts.app2')
@section('title', 'News')
@section('menu', 'news')
@section('content') 
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3> Access Logs List
            <div class="btn-group pull-right">
                <button class="btn btn-danger" data-toggle="modal" data-target="#delete_data"><i class="fa fa-ban"></i> Delete</button>
            </div>
            <div class="btn-group pull-right">
                <button class="btn btn-success" data-toggle="modal" data-target="#export_data"><i class="fa fa-bars"></i> Export Data</button>
            </div>
        </div>

        <div class="panel-body">
            <div class="table-responsive">
                <table id="AccessLog" class= "table datatable">
                    <thead>
                        <tr> 
                            <th style="width:10%">Path</th>
                            <th style="width:5%">Ip</th>
                            <th style="width:5%">Creted_at</th>
                        </tr> 
                    </thead>
                    <tbody>
                        @forelse($AccessLog as $f_AccessLog)
                        <tr>
                            <td>{{ $f_AccessLog->path }}</td>
                            <td>{{ $f_AccessLog->ip }}</td>
                            <td>{{ $f_AccessLog->created_at }}</td>
                        @empty
                            <td colspoan="3">
                            <center>
                                <h2>:(</h2>
                                <p> Access log kosong </p>
                            </center>
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
                {{ $AccessLog->links() }}  
            </div> 
        </div>
    </div>
    <!-- Modals -->
    <div class="modal fade" id="export_data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5> Export Data </h5>
                    <p>Start Date and End Date must be different date</p>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => 'accesslog/export', 'method' => 'post']) !!}
                    {!! Form::label('file', 'Export to') !!}
                    {!! Form::select('file', ['csv' => 'CSV', 'xlx' => 'XLX', 'pdf' => 'PDF' ], null,['class'=>'form-control js-selectize']) !!}
                    {!! Form::label('start_date', 'Start Date') !!}
                    {!! Form::date('start_date', null, ['class'=>'form-control']) !!}
                    {!! Form::label('end_date', 'End Date') !!}
                    {!! Form::date('end_date', null, ['class'=>'form-control']) !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Export', [ 'class'=> 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete_data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5> Delete Data </h5>
                    <p>Start Date and End Date must be different date</p>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => 'accesslog/delete', 'method' => 'post']) !!}
                    {!! Form::label('start_date', 'Start Date') !!}
                    {!! Form::date('start_date', null, ['class'=>'form-control']) !!}
                    {!! Form::label('end_date', 'End Date') !!}
                    {!! Form::date('end_date', null, ['class'=>'form-control']) !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {!! Form::submit('Delete', [ 'class'=> 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/actions.js') }}"></script> 
<script type="text/javascript">
    $(document).ready( function() {
        document.getElementById("form-add").style.display = "none";

        $( "#add-member" ).click( function() {
            $( "#form-add" ).toggle( 'slow' );
        });

    });
</script>
<script type="text/javascript">
    for (var i=1; i<=' <?php echo($count); ?>'; i++ ){
        document.getElementById("form-edit"+i).style.display = "none";
    }

    function edit_click(id)
    {
        $("#form-edit"+id).toggle( 'slow' );
    }
</script>
@endsection 