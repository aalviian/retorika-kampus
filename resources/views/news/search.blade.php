@extends('layouts.app2')

@section('title', 'News')

@section('menu', 'news')

@section('content') 

    <div class="panel panel-default">

        <div class="panel-heading"> 

            <h3 class="panel-title">News

            </h3>

            <div class="btn-group pull-right">

                <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>

                <ul class="dropdown-menu">

                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'false'});"><img src='img/icons/json.png' width="24"/> JSON</a></li>

                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'false',ignoreColumn:'[2,3]'});"><img src='img/icons/json.png' width="24"/> JSON (ignoreColumn)</a></li>

                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'true'});"><img src='img/icons/json.png' width="24"/> JSON (with Escape)</a></li>

                    <li class="divider"></li>

                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'xml',escape:'false'});"><img src='img/icons/xml.png' width="24"/> XML</a></li>

                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'sql'});"><img src='img/icons/sql.png' width="24"/> SQL</a></li>

                    <li class="divider"></li>

                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'csv',escape:'false'});"><img src='img/icons/csv.png' width="24"/> CSV</a></li>

                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'txt',escape:'false'});"><img src='img/icons/txt.png' width="24"/> TXT</a></li>

                    <li class="divider"></li>

                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'excel',escape:'false'});"><img src='img/icons/xls.png' width="24"/> XLS</a></li>

                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'doc',escape:'false'});"><img src='img/icons/word.png' width="24"/> Word</a></li>

                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'powerpoint',escape:'false'});"><img src='img/icons/ppt.png' width="24"/> PowerPoint</a></li>

                    <li class="divider"></li>

                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'png',escape:'false'});"><img src='img/icons/png.png' width="24"/> PNG</a></li>

                    <li><a href="#" onClick ="$('#customers2').tableExport({type:'pdf',escape:'false'});"><img src='img/icons/pdf.png' width="24"/> PDF</a></li>

                </ul>

            </div>                                    

        </div>

        <div class="panel-body">

            <span>Tambah: 

                <a href="{{ route('news.create') }}" class= "btn btn-info btn-sm" >+</a>

            <span>

            <div class="table-responsive">

                <table id="customers2" class="table datatable">

                    <thead>

                        <tr>

                            <th>Image</th>

                            <th>Rubric</th>

                            <th>Type</th>

                            <th>Highlight</th>

                            <th>Judul</th>

                            <th>Created_by</th>

                            <th>Edited_by</th>

                            <th>Published_by</th>

                            <th style="width:15%">Status</th>

                            <th colspan="3"><center>Action</center></th>

                        </tr>

                    </thead>

                    <tbody>

                        @forelse($news as $f_news)

                            <tr>

                                <td><img src="{{ $f_news -> photo_path }}" width="30" height="30"></td>

                                <td>{{ $f_news -> rubric -> name }}</td>

                                <td> 

                                    @foreach ($f_news -> type as $type)

                                    <span class="label label-primary"><i class="fa fa-btn fa-tags"></i>{{ $type->name }}</span>

                                        @endforeach

                                    </td>

                                <td>

                                    @foreach ($f_news -> highlight as $highlight)

                                        <span class="label label-primary"><i class="fa fa-btn fa-tags"></i>{{ $highlight->name }}</span>

                                    @endforeach

                                </td>

                                <td>{{ $f_news -> title }}</td>

                                <td>{{ App\User::where('id', $f_news -> created_by)->first()->name }}</td>

                                @if($f_news->edited_by != NULL) 

                                    <td>{{ App\User::where('id', $f_news -> edited_by)->first()->name }}</td>

                                @else
                                        
                                    <td></td>

                                @endif
                                @if($f_news->approved_by != NULL)

                                    <td>{{ App\User::where('id', $f_news -> approved_by)->first()->value('name') }}</td>

                                @else

                                    <td></td>

                                @endif

                                <td>

                                    @if(Auth::user()->level_id == 3 or Auth::user()->level_id == 4)

                                        {{ $f_news -> status }}

                                    @else

                                        {!! Form::open(['url' => 'news/updateStatus/'.$f_news->id, 'method' => 'post']) !!}

                                        {!! Form::select('status_change', [ 'published' => 'published', 'pending' => 'pending' ], $f_news -> status,['onchange' => 'this.form.submit()', 'class'=>'form-control js-selectize']) !!}

                                        {!! Form::close() !!}

                                    @endif

                                </td>

                                <td>
                                    <a href="{{url('berita/rubrik/'.$f_news->rubric->slug.'/'.$f_news->slug)}}" class="btn btn-sm btn-info">View</a>
                                        
                                </td>

                                <td>
                                    
                                    <a href="{{ route('news.edit', $f_news->id) }}" class = "btn btn-sm btn-success">Edit</a>

                                </td>

                                <td>

                                    @if(Auth::user()->id == $f_news->created_by)

                                        {!! Form::model($f_news, ['route' => ['news.destroy', $f_news], 'method' => 'delete', 'class' => 'form-inline'] ) !!}

                                        {!! Form::submit('delete', ['class'=>'btn btn-sm btn-danger js-submit-confirm']) !!}

                                        {!! Form::close() !!}

                                    @endif

                                </td> 

                            </tr>

                        @empty

                            <td colspan="10"><center><h2>:(</h2><p>Berita yang anda cari tidak ada..</p></center></td>

                        @endforelse

                    </tbody>

                </table> 

                {{ $news->appends(['query'=>$query])->links() }}                                   

            </div>

        </div>

    </div>

@endsection



@section('script')

<script type='text/javascript' src="{{ asset('js/plugins/icheck/icheck.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/plugins/tableexport/tableExport.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/plugins/tableexport/jquery.base64.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/plugins/tableexport/html2canvas.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/plugins/tableexport/jspdf/libs/sprintf.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/plugins/tableexport/jspdf/jspdf.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/plugins/tableexport/jspdf/libs/base64.js') }}"></script> 

<script type="text/javascript" src="{{ asset('js/actions.js') }}"></script> 

@endsection 