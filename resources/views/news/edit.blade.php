@extends('layouts.app2')

@section('style')

<script type="text/javascript" src="{{ asset('nicEdit/nicEdit.js')}}"></script> 
<script type="text/javascript">
//<![CDATA[
        
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });

  //]]>
</script>

@endsection

@section('title', 'Edit News')

@section('menu', 'edit news')

@section('content') 

    <div class="panel panel-default">

		<div class="panel-heading">

			<h3>Edit News</h3>

		</div>

		<div class="panel-body">

			{!! Form::model($news, ['route'=> ['news.update', $news], 'method'=>'patch', 'files'=>true]) !!}

	            @include('news.form', ['model'=>$news])

	        {!! Form::close() !!} 

		</div>

	</div>

@endsection



@section('script')

<script type="text/javascript" src="{{ asset('js/actions.js') }}"></script> 

@endsection 