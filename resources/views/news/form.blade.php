<div class= " form-group { !! $errors->has('title') ? 'has-error' : '' !!}">

	{!! Form::label('title', 'Title') !!}

	{!! Form::text('title', null, ['class' =>'form-control', 'placeholder' => 'Ketik judul berita...']) !!}

	{!! $errors->first('title', '<p class="help-block">:message</p>') !!}

</div>



<div class= " form-group { !! $errors->has('content') ? 'has-error' : '' !!}">

	{!! Form::label('content' , 'Content') !!}

	{!! Form::textarea('content', null, ['class' =>'form-control', 'placeholder' => 'Ketik isi berita...']) !!}

	{!! $errors->first('content', '<p class="help-block">:message</p>') !!}

</div> 



<div class= " form-group { !! $errors->has('keywords') ? 'has-error' : '' !!}">

	{!! Form::label('keywords', 'Keywords') !!}

	{!! Form::text('keywords', null, ['class' =>'form-control', 'placeholder' => 'Ketik keywords berita...']) !!}

	{!! $errors->first('keywords', '<p class="help-block">:message</p>') !!}

</div>



<div class= " form-group { !! $errors->has('rubric') ? 'has-error' : '' !!}">

	@if(Auth::user()->level_id!=4)

		{!! Form::label('rubric' , 'Rubric') !!}

		@if(isset($news))

			{!! Form::select('rubric', ['' => ''] + $pluck, (!empty($news->rubric_id)? $news->rubric_id:'null'), ['class' =>'form-control js-selectize']) !!}

		@else

			{!! Form::select('rubric', ['' => ''] + $pluck, null, ['class' =>'form-control js-selectize']) !!}

		@endif

	@else

		{!! Form::text('rubric', 1, ['class' =>'hidden']) !!}

	@endif

	{!! $errors->first('rubric', '<p class="help-block">:message</p>') !!}

</div>


@if(Auth::user()->level_id == 1 or Auth::user()->level_id == 2)
	<div class= " form-group { !! $errors->has('highlight') ? 'has-error' : '' !!}">

		{!! Form::label('highlight' , 'Highlight') !!}

		{!! Form::select('highlight[]', [ '' => '' ]+App\Highlight::pluck('name','id')->all(), null, ['class' =>'form-control js-selectize', 'multiple']) !!}

		{!! $errors->first('highlight', '<p class="help-block">:message</p>') !!}

	</div>
@endif


	<div class= " form-group { !! $errors->has('type') ? 'has-error' : '' !!}">

		@if(Auth::user()->level_id!=4 and Auth::user()->level_id!=3)

			{!! Form::label('type' , 'Headline') !!}

			{!! Form::select('type[]', [ '' => '' ]+App\Type::pluck('name','id')->all(), null, ['class' =>'form-control js-selectize']) !!}

		@else

			{!! Form::text('type[]', 1, ['class' =>'hidden']) !!}

		@endif

		{!! $errors->first('type', '<p class="help-block">:message</p>') !!}

	</div>



<div class="form-group {!! $errors->has('image') ? 'has-error' : '' !!}">

	{!! Form::label('image', 'News Image ( jpg,jpeg,png )') !!}

	{!! Form::file('image') !!}

	{!! $errors->first('image', '<p class="help-block">:message</p>')  !!}

	@if (isset($model) && $model->image !== '' )

	<div class= "row">

		<div class= "col-md-6">

			<br>

			<p>Current iamge:</p>

			<div class= "thumbnail">

				<img src = "{{ url('/img/data/news/'. $model->image) }}" class="img-rounded" width="200" height="200">

			</div>

		</div>

	</div>

	@endif

	@if (isset($model) && $model->image == '')

	<div class= "row">

		<div class= "col-md-6">

			<br>

			<p>Current image:</p>

			<div class= "thumbnail">

				<img src = "{{ url('/img/data/news/default.png') }}" class="img-rounded" width="200" height="200">

			</div>

		</div>

	</div>

	@endif

</div>

<div class= " form-group { !! $errors->has('keywords') ? 'has-error' : '' !!}">

	{!! Form::label('image_source', 'Image Source') !!}

	{!! Form::text('image_source', null, ['class' =>'form-control', 'placeholder' => 'Ketik url sumber gambar berita']) !!}

	{!! $errors->first('keywords', '<p class="help-block">:message</p>') !!}

</div>

@if(isset($news))
	@if(Auth::user()->id == $news->created_by)
		<div class= " form-group ">

			{!! Form::label('save_as' , 'Save as') !!}

			{!! Form::select('save_as', [ 'draft' => 'Draft', 'news' => 'News' ], ($news->is_draft? 'draft' : 'news') , ['class' =>'form-control js-selectize']) !!}

		</div>
	@endif

@else
	<div class= " form-group ">

		{!! Form::label('save_as' , 'Save as') !!}

		{!! Form::select('save_as', [ 'draft' => 'Draft', 'news' => 'News' ], null, ['class' =>'form-control js-selectize']) !!}

	</div>
@endif

<center>

<a href="{{ route('news.index') }}" class="btn btn-warning">Cancel</a>

{!! Form::submit( isset( $model) ? 'Simpan' : 'Kirim Berita', [ 'class'=> 'btn btn-primary']) !!}

</center>