@extends('layouts.app2')

@section('style')

<script type="text/javascript" src="{{ asset('nicEdit/nicEdit.js')}}"></script> 
<script type="text/javascript">
//<![CDATA[
        
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });

  //]]>
</script>

@endsection

@section('title', 'News')

@section('menu', 'news')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading">

            <h3 class="panel-title">News

            </h3>                                  

        </div>

        <div class="panel-body">

            {!! Form::open(['route' => 'news.store', 'files'=>true]) !!}

                @include('news.form')

            {!! Form::close() !!}   

        </div>

    </div>

@endsection



@section('script')

<script type='text/javascript' src="{{ asset('js/plugins/icheck/icheck.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/actions.js') }}"></script> 

@endsection 