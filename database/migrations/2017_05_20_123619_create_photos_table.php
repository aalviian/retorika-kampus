<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rubric_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('slug');
            $table->string('name');
            $table->string('image');
            $table->string('content');
            $table->string('status');
            $table->timestamps();

            $table->foreign('rubric_id')->references('id')->on('rubrics');

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
