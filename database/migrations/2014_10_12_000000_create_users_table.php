<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('name');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('avatar');
            $table->string('phone')->nullable();
            $table->text('bio');
            $table->integer('level_id')->unsigned();
            $table->integer('is_confirmed')->default(0);
            $table->integer('is_login')->default(0);
            $table->rememberToken();
            $table->timestamps();
            $table->string('login_date')->nullable();
            $table->string('logout_date')->nullable()->default("2017-09-03 17:27:43");

            $table->foreign('level_id')->references('id')->on('levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
