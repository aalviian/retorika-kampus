<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rubric_id')->unsigned(); //rubrik_berita
            $table->string('slug');
            $table->string('title');
            $table->text('content');
            $table->string('image');
            $table->string('image_source');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('edited_by')->unsigned()->nullable();
            $table->integer('approved_by')->unsigned()->nullable();
            $table->string('status');
            $table->integer('views');
            $table->string('keywords');
            $table->string('is_spreaded')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('rubric_id')->references('id')->on('rubrics');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('edited_by')->references('id')->on('users');
            $table->foreign('approved_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
