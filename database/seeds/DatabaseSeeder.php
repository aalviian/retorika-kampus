<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(Levels::class);
        $this->call(Users::class);
        $this->call(Types::class);
        $this->call(Rubrics::class);
        $this->call(Highlights::class);
        $this->call(News::class);
        $this->call(Subscribes::class);
        $this->call(Advertisements::class);
        $this->call(Comments::class);
        $this->call(Configurations::class);

    }
}
