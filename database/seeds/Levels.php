<?php

use Illuminate\Database\Seeder;

class Levels extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		App\Level::create([
			'name' => 'Admin'
		]);

		App\Level::create([
			'name' => 'Redaktur'
		]);

		App\Level::create([
			'name' => 'Reporter'
		]);

		App\Level::create([
			'name' => 'User'
		]);

		$this->command->info("Successfully created 3 level : admin, redaktur, reporter");
    }
}
