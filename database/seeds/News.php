<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Rubric;

class News extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker=Faker::create("en_En");
        $status = ['pending', 'published'];
        $keywords = ['internasional','politik', 'ekonomi', 'kampus', 'opini'];
        $user = ['aalviian', 'aneksa', 'user'];
        $counter=0;
        for($i=0; $i<10; $i++){
        	$name=$faker->realText(20);
            $news = new App\News;
            $user_index = rand(1,3);
            if($user_index == 3)
            	$news->rubric_id = 1;
            else
                $news->rubric_id = rand(2, count(Rubric::all()));
        	$news->slug = str_slug($name, '-');
        	$news->title = $name;
        	$news->content = $faker->realText(5000);
        	$news->image = '';
        	$news->image_source = 'http://google.com';
            $news->created_by = $user_index;
            $news->edited_by = rand(1,2);
            $j=rand(0,1); 
            if($j){
               $news->approved_by = 1;
        	   $news->status = $status[$j];
            }else{
               $news->approved_by = NULL;
               $news->status = $status[$j];
            }
            $news->views = rand(1,100);
        	$news->keywords = $keywords[array_rand($keywords)];
        	$news->save();

        	$counter+=1;

        	$news->highlight()->sync([rand(1,4)]);

     		$news->type()->sync([rand(1,2)]); 
        }
        $this->command->info("Successfully created ".$counter." news");
    }
}
