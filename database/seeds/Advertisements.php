<?php

use Illuminate\Database\Seeder;
use App\Advertisement;
use Faker\Factory as Faker;

class Advertisements extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker=Faker::create("en_En");
        $counter = 0;
        for ($i=1; $i <= 5; $i++) { 
        	//Ads untuk halaman depan
        	$highlight = new Advertisement;
        	$highlight->title = $faker->sentence($nbWords = 3, $variableNbWords = true);
        	$highlight->description = $faker->sentence($nbWords = 6, $variableNbWords = true);
        	$highlight->image = "default.png";
        	$highlight->url = $faker->url;
        	$highlight->type = "depan";
        	$highlight->order = $i;
        	$highlight->save();
        	$counter++;

        	//Ads untuk halaman rubrik
        	$highlight = new Advertisement;
        	$highlight->title = $faker->sentence($nbWords = 3, $variableNbWords = true);
        	$highlight->description = $faker->sentence($nbWords = 6, $variableNbWords = true);
        	$highlight->image = "default.png";
        	$highlight->url = $faker->url;
        	$highlight->type = "rubrik";
        	$highlight->order = $i;
        	$highlight->save();
        	$counter++;

            //Ads untuk halaman rubrik
            $highlight = new Advertisement;
            $highlight->title = $faker->sentence($nbWords = 3, $variableNbWords = true);
            $highlight->description = $faker->sentence($nbWords = 6, $variableNbWords = true);
            $highlight->image = "default.png";
            $highlight->url = $faker->url;
            $highlight->type = "fokus";
            $highlight->order = $i;
            $highlight->save();
            $counter++;

        	//Ads untuk halaman berita
        	$highlight = new Advertisement;
        	$highlight->title = $faker->sentence($nbWords = 3, $variableNbWords = true);
        	$highlight->description = $faker->sentence($nbWords = 6, $variableNbWords = true);
        	$highlight->image = "default.png";
        	$highlight->url = $faker->url;
        	$highlight->type = "berita";
        	$highlight->order = $i;
        	$highlight->save();
        	$counter++;

        }
        $this->command->info("Successfully created ".$counter." advertisements");
    }
}
