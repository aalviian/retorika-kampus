<?php

use Illuminate\Database\Seeder;
use App\Rubric;

class Rubrics extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$slug=['internasional','politik', 'ekonomi', 'kampus'];
    	$name=['Internasional','Politik', 'Ekonomi', 'Kampus'];
        $counter=0;
        $rubric = new Rubric;
        $rubric->slug = 'opini';
        $rubric->name = 'Opini';
        $rubric->order = 5;
        $rubric->save();

        for($i=0; $i<=3; $i++){
        	$rubric = new Rubric;
        	$rubric->slug = $slug[$i];
        	$rubric->name = $name[$i];
            $rubric->order = $i+1;
        	$rubric->save();
            $rubric->childs()->saveMany([
                new Rubric(['slug' => 'umum-'.$rubric->slug, 'name' => 'Umum '.$rubric->name]),
                new Rubric(['slug' => 'khusus-'.$rubric->slug, 'name'=> 'Khusus '.$rubric->name])
            ]);
        	$counter+=1;
        }

        $this->command->info("Successfully created 6 rubrics and its sub-rubrics");  
    } 
}
