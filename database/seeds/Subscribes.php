<?php

use Illuminate\Database\Seeder;
use App\Subscribe;

class Subscribes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subscribe = new Subscribe;
        $subscribe->email = 'andyeka07@gmail.com';
        $subscribe->save();
        $subscribe->news()->sync([1,2,3,4]);
    }
}
