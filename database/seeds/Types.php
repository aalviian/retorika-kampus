<?php

use Illuminate\Database\Seeder;

class Types extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$slug=['not-headline','front-headline'];
    	$name=['Not Headline', 'Front Headline'];
        $counter=0;
        for($i=0; $i<=1; $i++){
        	$type = new App\Type;
        	$type->slug = $slug[$i];
        	$type->name = $name[$i];
        	$type->save();

        	$counter+=1;
        }
        $this->command->info("Successfully created ".$counter." types");
    }
}
 