<?php

use Illuminate\Database\Seeder;
use App\Configuration;

class Configurations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $config = new Configuration;
        $config->user_id = 1;
        $config->web_name = "Retorika Kampus";
        $config->tagline = "Membuka Cakrawala";
        $config->url = "http://www.retorikakampus.com";
        $config->email = "retorikakampus@gmail.com";
        $config->address = "Jl. Veteran Blok Barja No.2, Kb. Klp, Bogor Tengah. Kota Bogor, Jawa Barat, 16125";
        $config->phone_number = "+622165353";
        $config->mobile_number = "+6289624447446";
        $config->fax_number = 0;
        $config->logo = "logo.png";
        $config->icon = "logo.png";
        $keywords = "";
        $meta_text = "";
        $facebook = "";
        $twitter = "";
        $lat = "";
        $long = "";
        $config->save();

        $this->command->info("Successfully created some configurations");
    }
}
