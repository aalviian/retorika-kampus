<?php

use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		App\User::create([
 			'slug' => 'muhammad-alvian-supriadi',
			'name' => 'Muhammad Alvian Supriadi',
			'username' => 'aalviian',
			'email' => 'aalviian@gmail.com',
			'avatar' => '',
			'phone' => '+6287808125863',
			'bio' => 'I am an admin.',
			'level_id' => 1,
			'password' => bcrypt('secret'),
			'is_confirmed' => 1
		]);

		App\User::create([
 			'slug' => 'andy-eka-saputra',
			'name' => 'Andy Eka Saputra',
			'username' => 'aneksa',
			'email' => 'andyeka07@gmail.com',
			'avatar' => '',
			'bio' => 'I am an user.',
			'level_id' => 2,
			'password' => bcrypt('secret'),
			'is_confirmed' => 1
		]);
		
		App\User::create([
 			'slug' => 'user',
			'name' => 'User',
			'username' => 'user',
			'email' => 'user@gmail.com',
			'avatar' => '',
			'bio' => 'I am an user.',
			'level_id' => 4,
			'password' => bcrypt('secret'),
			'is_confirmed' => 1
		]);
		
		$this->command->info("Successfully created 2 users : admin and non admin");
    } 
}
