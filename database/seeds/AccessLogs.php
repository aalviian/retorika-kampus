<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\News;
use App\Rubric;

class AccessLogs extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $rubric = App\Rubric::all();
        $slug=[];
        foreach ($rubric as $f_rubric) {
            # code...
            $slug[]=$f_rubric->slug;
        }
        for($i=1;$i<=1000;$i++){
            $now=Carbon::now()->subDays(rand(0,20));
            $news=News::where('id', rand(1,50))->first();
        	$AccessLog = new App\AccessLog;
        	$AccessLog->path = "berita/rubrik/".$news->rubric->slug.'/'.$news->slug;
            $AccessLog->ip = "127.0.0.".rand(10,100);
            $AccessLog->status = "succes";
            $AccessLog->created_at = $now;
            $AccessLog->updated_at = $now;
            $AccessLog->save();
        }
        for($i=1;$i<=100;$i++){
            $now=Carbon::now();
            $hour=Carbon::create($now->year, $now->month, $now->day, rand(0,23), rand(0,59), rand(0,59));
            $news=News::where('id', rand(1,50))->first();
            $AccessLog = new App\AccessLog;
            $AccessLog->path = "berita/rubrik/".$news->rubric->slug.'/'.$news->slug;
            $AccessLog->ip = "127.0.0.".rand(10,100);
            $AccessLog->status = "succes";
            $AccessLog->created_at = $hour->toDateTimeString();
            $AccessLog->updated_at = $hour->toDateTimeString();
            $AccessLog->save();
        }

        $this->command->info("Successfully created 2000 access logs");
    } 
}
