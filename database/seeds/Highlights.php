<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class Highlights extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
    	$faker=Faker::create("en_En");
    	$slug=['kampus-ipb','kampus-itb', 'makanan-indonesia', 'opini-politik'];
    	$name=['Kampus IPB', 'Kampus ITB', 'Makanan Indonesia', 'Opini Politik'];
        $counter=0;
        for($i=0; $i<=3; $i++){
        	$highlight = new App\Highlight;
        	$highlight->user_id = 1;
        	$highlight->slug = $slug[$i];
        	$highlight->name = $name[$i];
        	$highlight->image = '';
        	$highlight->description = $faker->text;
        	$highlight->save();

        	$counter+=1;
        }
        $this->command->info("Successfully created ".$counter." highlights");
    }
}
