$(document).ready(function () {
  $(document.body).on('click', '.js-submit-confirm', function (event) {
    event.preventDefault() 
    var $form = $(this).closest('form')
    var $el = $(this)
    var text = $el.data('confirm-message') ? $el.data('confirm-message') : 'You want to delete this one'

    swal({
      title: 'Are you sure?',
      text: text,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Agree',
      cancelButtonText: 'Cancel',
      closeOnConfirm: true
    },
      function () {
        $form.submit() 
      })
  }) 
  $('.js-selectize').selectize({
    // sortField:'text',
    persist: false,
    create: false
  })

  //tipe-news form
  // if ($('input[name=headline]').length > 0 && $('input[name=is_news]').length > 0 && $('input[name=is_news]:checked').val() > 0) {
  //   $('input[name=headline]').prop('disabled', true)
  // }

  // $('input[name=is_news]').click(function () {
  //   var val = $('input[name=is_news]:checked').val()
  //   if (val > 0) {
  //     $('input[name=headline]').prop('disabled', true)
  //   } else {
  //     $('input[name=headline]').prop('disabled', false)
  //   }
  // })

  // if ($('input[name="headline"]').length > 0) {
  //   var selected = $('input[name="headline"]:checked').val()
  //   if (selected === 'undefined' || selected == "Headline Depan" ) {
  //     $('#rubric').hide()
  //   }

  //   $('input[name="headline"]').change(function () {
  //     var selected = $('input[name="headline"]:checked').val()
  //     if (selected == "Headline") {
  //       $('#rubric').show()
  //     } else {
  //       $('#rubric').hide() 
  //     } 
  //   })
  // }
}) 