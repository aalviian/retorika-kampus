<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['isAdmin']], function () {
	Route::get('home', 'StatisticController@index');
	Route::post('home', 'StatisticController@index');
	Route::get('/delete-rubric/{id}', 'TrashController@destroyRubric');
	Route::get('/delete-type/{id}', 'TrashController@destroyType');
	Route::get('/delete-highlight/{id}', 'TrashController@destroyHighlight');
	Route::get('/delete-news/{id}', 'TrashController@destroyNews');
	Route::get('/restore-rubric/{id}', 'TrashController@restoreRubric');
	Route::get('/restore-type/{id}', 'TrashController@restoreType');
	Route::get('/restore-highlight/{id}', 'TrashController@restoreHighlight');
	Route::get('/restore-news/{id}', 'TrashController@restoreNews'); 

	//Rubrik Controller
	Route::resource('/rubric', 'RubricController');
	Route::get('/rubric-order', 'RubricController@rubricOrder');
	Route::post('/rubric-order-save', 'RubricController@orderSave');
	//End Rubrik Controller

	//Tipe Controller
	Route::resource('/type', 'TypeController');
	//End Tipe Controller

	//Highlight Controller
	Route::resource('/highlight', 'HighlightController');
	Route::get('/highlight-order', 'HighlightController@highlightOrder');
	Route::post('/highlight-order-save', 'HighlightController@orderSave');
	//End Highlight Controller

	//News Controller
	Route::resource('/news', 'NewsController');
	Route::post('/news/updateStatus/{id}', 'NewsController@updateStatus');
	//End News Controller

	//Ads Controller
	Route::resource('/advertisement', 'AdvertisementController');
	//End Ads Controller

	//Member controller
	Route::resource('/member', 'UserController');
	Route::get('/user-confirm/{id}', 'UserController@userConfirm');
	Route::post('/spread-news/{news}', 'SubscribeController@spreadNews');

	//AccessLog
	Route::resource('/accesslog', 'AccessLogController');
	Route::post('/accesslog/export', 'AccessLogController@export');
	Route::post('/accesslog/delete', 'AccessLogController@delete');

	//Config
	Route::get('/config', 'ConfigController@index');
	Route::post('/config/update/{id}', 'ConfigController@update');
	Route::get('/log', 'LogController@index');
});

Route::group(['middleware' => ['isRedaktur']], function () {
	Route::get('home', 'StatisticController@index');
	Route::post('home', 'StatisticController@index');
	Route::get('/trash', 'TrashController@index');
	Route::get('/delete-highlight/{id}', 'TrashController@destroyHighlight');
	Route::get('/restore-highlight/{id}', 'TrashController@restoreHighlight');
	Route::resource('/highlight', 'HighlightController');
	Route::get('/highlight-order', 'HighlightController@highlightOrder');
	Route::post('/highlight-order-save', 'HighlightController@orderSave');
	
	//News Controller
	Route::resource('/news', 'NewsController');
	Route::post('/news/updateStatus/{id}', 'NewsController@updateStatus');
	
	Route::resource('/profile', 'ProfileController'); 

	//Member controller
	Route::resource('/member', 'UserController');
	Route::get('/user-confirm/{id}', 'UserController@userConfirm');
	Route::get('/spread-news/{news}', 'SubscribeController@spreadNews');
	Route::get('/log', 'LogController@index');
});  
 
Route::group(['middleware' => ['isReporter']], function () {
	Route::get('home', 'StatisticController@index');
	Route::post('home', 'StatisticController@index');
	Route::get('/trash', 'TrashController@index');
	Route::resource('/news', 'NewsController');
	Route::resource('/profile', 'ProfileController');
	Route::get('/log', 'LogController@index');
}); 

Route::group(['middleware' => ['isUser']], function () {
	Route::get('home', 'StatisticController@index');
	Route::post('home', 'StatisticController@index');
	Route::resource('/news', 'NewsController');
	Route::resource('/profile', 'ProfileController');
	Route::get('/log', 'LogController@index');
}); 


//FRONT END 
Route::get('/', 'FrontEnd\IndexController@index');
Route::post('/user-store', 'UserController@userStore');
Route::post('/subscribe', 'SubscribeController@postSubscribe');
Route::get('berita/rubrik/{rubric}', 'FrontEnd\RubrikController@index');
Route::get('berita/rubrik/{rubric}/{news}', 'FrontEnd\NewsController@rubric_news');
Route::get('berita/fokus/{highlight}', 'FrontEnd\FokusController@index');
Route::get('berita/fokus/{highlight}/{news}', 'FrontEnd\NewsController@highlight_news');
Route::resource('/subscribe', 'SubscribeController');
Route::resource('/comment', 'CommentController');
Route::get('/request-ads', function(){
	Alert::message('Silahkan hubungi kami melalui email untuk pemasangan iklan di portal kami. Email : retorikakampus@gmail.com', 'Permohonan Izin Iklan')->persistent('Close');
	return redirect('/');
});
Route::get('/pencarian', 'SearchController@searchResult');
Route::get('/pencarian/berita', 'SearchController@searchNews');
Route::get('/pencarian/komentar', 'SearchController@searchComments');