<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable; 
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'name', 
        'username',
        'password',
        'email', 
        'avatar',
        'phone',
        'bio',
        'level_id',
        'is_confirmed',
        'is_login',
        'login_date',
        'logout_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function highlight(){
        return $this->hasMany('App\Highlight');
    }

    public function level(){
        return $this->hasOne('App\Level');
    } 

    public function news(){
        return $this->hasMany('App\News');
    } 

    public function getPhotoPathAttribute(){
        if($this->avatar !== ''){
            return url('/img/data/user/'.$this->avatar);
        }
        else{
            return url('/img/data/user/default.png');
        }
    } 

    public function sendPasswordResetNotification($token) {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function routeNotificationForNexmo() {
        return $this->phone;
    } 

}
