<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
	protected $fillable = [
		'user_id',
		'web_name',
		'tagline',
		'url',
		'email',
		'address',
		'phone_number',
		'mobile_number',
		'fax_number',
		'logo',
		'icon',
		'keywords',
		'meta_text',
		'facebook',
		'twitter',
		'maps'
	];
}
