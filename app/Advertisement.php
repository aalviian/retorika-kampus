<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    protected $fillable = [
    	'type',
        'order',
    	'title',
    	'description',
    	'image',
    	'url'
    ];

    public function getPhotoPathAttribute(){
        if($this->image != ''){
            return url('/img/data/advertisement/'.$this->image);
        }
        else if($this->image == NULL){
            return url('/img/data/advertisement/default.png');
        }
        else{
            return url('/img/data/advertisement/default.png');
        }
    } 
}
