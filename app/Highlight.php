<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Highlight extends Model
{
    use SoftDeletes; 

    protected $fillable = [
        'user_id',
        'slug', 
        'name',
        'image',
        'order'
    ];

    public static function boot(){
        parent::boot();

        static::deleting(function($model){
            $model->news()->detach();
        }); 
    }

    protected $dates = ['deleted_at'];

    public function news(){
        return $this->belongsToMany('App\News');
    }  

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function getPhotoPathAttribute(){
        if($this->image != ''){
            return url('/img/data/highlight/'.$this->image);
        }
        else if($this->image == NULL){
            return url('/img/data/highlight/default.png');
        }
        else{
            return url('/img/data/highlight/default.png');
        }
    } 

}
