<?php

namespace App; 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
	use SoftDeletes; 

	protected $fillable = [
		'slug',
		'name'	
	];

	public static function boot(){
		parent::boot();

		static::deleting(function($model){
			$model->news()->detach();
		});
	}

	protected $dates = ['deleted_at'];

	public function news(){
    	return $this->belongsToMany('App\News');
    }
}
