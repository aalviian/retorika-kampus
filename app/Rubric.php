<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rubric extends Model
{
	use SoftDeletes; 

    protected $fillable = [
    	'slug',
    	'name',
    	'order',
    	'parent_id'
    ]; 

    protected $dates = ['deleted_at'];

	public static function boot(){
		parent::boot();

		static::deleting(function($model){
			$model->news()->delete();

			// foreach($model->childs() as $child){
			// 	$child->update(['parent_id'=>0]);
			// }
		});   
	}

    // mencari sub rubric, relasinya one-to-many.
    public function childs()
	{
		return $this->hasMany('App\Rubric', 'parent_id'); 
	}  

	// mencari parent rubric dr sub rubric, relasinya one-to-many.
	public function parent()
	{
		return $this->belongsTo('App\Rubric', 'parent_id');
	}

	public function scopeNoParent($query){
		return $query->where('parent_id', 0);
	}

	public function scopeIsChild($query){
		return $query->where('parent_id','>', 0);
	}

	public function hasParent(){ 
		return $this->parent_id > 0;
	}

	public function hasChild(){
		return $this->childs()->count() > 0;
	}

    public function news(){
    	return $this->hasMany('App\News');
    } 
}
