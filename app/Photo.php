<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
	protected $fillable = [
		'rubric_id',
		'user_id',
		'slug',
		'name',
		'image',
		'content',
		'status'
	];
}
