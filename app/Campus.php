<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campus extends Model
{
	protected $fillable = [
		'slug',
		'name',
		'image',
		'address'
	];
}
