<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
	protected $fillable = [
		'rubric_id',
		'user_id',
		'slug',
		'title',
		'status',
		'content'
	];
}
