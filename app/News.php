<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
	use SoftDeletes; 

	protected $fillable = [
		'rubric_id',
		'slug',
		'title',
		'content',
		'image',
		'image_source',
		'status',
		'keywords',
		'is_spreaded' 
	];

	public static function boot(){
		parent::boot();

		static::deleting(function($model){
			$model->highlight()->detach();
		});  
	}

	protected $dates = ['deleted_at'];

	public function getRubric($id){

	}

    public function rubric(){
    	return $this->belongsTo('App\Rubric');
    }

    public function highlight(){
    	return $this->belongsToMany('App\Highlight');
    }

    public function type(){
    	return $this->belongsToMany('App\Type');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function comment(){
    	return $this->hasMany('App\Comment');
    }

    public function getPhotoPathAttribute(){
        if($this->image != ''){
            return url('/img/data/news/'.$this->image);
        }
        else{
            return url('/img/data/news/default.png');
        }
    } 
}
