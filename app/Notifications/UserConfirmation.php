<?php



namespace App\Notifications;



use Illuminate\Bus\Queueable;

use Illuminate\Notifications\Notification;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Notifications\Messages\MailMessage;



class UserConfirmation extends Notification

{

    use Queueable;



    /**

     * Create a new notification instance.

     *

     * @return void

     */

    public function __construct($datas)

    { 

        $this->datas=$datas;

    }



    /**

     * Get the notification's delivery channels.

     *

     * @param  mixed  $notifiable

     * @return array

     */

    public function via($notifiable)

    {

        return ['mail'];

    }



    /**

     * Get the mail representation of the notification.

     *

     * @param  mixed  $notifiable

     * @return \Illuminate\Notifications\Messages\MailMessage

     */

    public function toMail($notifiable)

    {

        return (new MailMessage)

                    ->subject('Membership of Retorika Kampus')

                    ->greeting("Hello Admin")

                    ->line('User baru telah dibuat dengan identitas sebagai berikut.')

                    ->line('Nama Lengkap : '.$this->datas['name'])

                    ->line('Email : '.$this->datas['email'])

                    ->line('Username : '.$this->datas['username'])

                    ->line('Password : '.$this->datas['password'])

                    ->line('No handphone : '.$this->datas['phone'])

                    ->line('Peran : '.$this->datas['level'])

                    ->action('Click to confirm', url('/user-confirm/'.$this->datas['id']));

    }



    /**

     * Get the array representation of the notification.

     *

     * @param  mixed  $notifiable

     * @return array

     */

    public function toArray($notifiable)

    {

        return [

            //

        ];

    }

}

