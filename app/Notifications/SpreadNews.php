<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SpreadNews extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($datas)
    {
        $this->datas=$datas;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail']; 
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Latest News from Retorika Kampus')
                    ->line('Hai pengguna portal berita Retorika Kampus, kini ada berita terbaru dari kami.')
                    ->line('Dengan judul beritanya : '.$this->datas['news'])
                    ->line('Silahkan mengunjungi laman website dibawah ini untuk membacanya.')
                    ->action('Portal Berita', url('/berita/rubrik/'.$this->datas['rubric'].'/'.$this->datas['news_slug']))
                    ->line('Terima kasih sudah menjadi subscriber di portal berita kami!')
                    ->replyTo($this->datas['email']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
