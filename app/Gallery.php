<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
	protected $fillable = [
		'slug',
		'name',
		'image',
		'status',
		'content'
	];
}
