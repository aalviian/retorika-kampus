<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => 'required',
            'name' => 'required',
            'username' => 'required|unique:users',
        ];    
    }

    public function messages(){
        return [
            'email.required' => 'Email wajib diisi',
            'checkout_password.required_if' => 'Password wajib diisi jika kamu pelanggan tetap',
        ];
    }
}
