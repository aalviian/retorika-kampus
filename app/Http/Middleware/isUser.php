<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Alert;
use Auth;


class isUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user() AND Auth::user()->level_id == 4 or Auth::user()->level_id == 1 or Auth::user()->level_id == 2 or Auth::user()->level_id == 3){
                if(Auth::user()->is_confirmed == 1){
                    return $next($request);
                }
                else{
                    Session::flush();
                    Auth::logout();
                    alert()->error('Kamu belum di konfirmasi oleh admin', 'Login gagal')->persistent('Close');
                    return redirect('/');
                }
            }
        }        
        Session::flush();
        Auth::logout();
        alert()->error('Sistem akan redirect ke halaman home...', 'Anda tidak dapat mengakses halaman tersebut')->persistent('Close');
        return redirect('/');
    }
}
