<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use DateTime;
use App\News;
use Carbon\Carbon;


class AccessLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $response = $next($request);
            $today = Carbon::now()->toDateString();
            $path = $request->path();
            $split_path = explode('/', $path);
            if(count($split_path)){
                $check_news = News::where('slug', $split_path[count($split_path)-1])->count();
            }
            if($path == '/' or $check_news>0){
                $check_acclog = DB::table('access_logs')->where('path', $path)->where('ip', $request->getClientIp())->whereDate('created_at', $today)->count();
            }
            else{
                $check_acclog = 1;
            }
            if($check_acclog == 0){
                DB::table('access_logs')->insert([
                    'path' => $request->path(),
                    'ip'=> $request->getClientIp(),
                    'status' => 'succes',
                    'created_at'=> new DateTime,
                    'updated_at' => new DateTime
                ]);
            }
            //tambah custom field isLogged buat ngecek apakah terminate atau ngga
            $request->merge(['isLogged' => 1]);
            return $response;
        }
        catch (\Illuminate\Database\QueryException $e) {
            return view('errors.query');
        }
        catch (\Exception $e) {
            return view('errors.500');
        }
    } 

    public function terminate($request, $response)
    {
        try{
            if (!$request->has('isLogged')){
                DB::table('access_logs')->insert([
                    'path' => $request->path(),
                    'ip'=> $request->getClientIp(),
                    'status' => 'failed',
                    'created_at'=> new DateTime,
                    'updated_at' => new DateTime
                ]);
            }
        }
        catch (\Illuminate\Database\QueryException $e) {
           return Response::make(view('errors.404'), 404);
        }
    }
}
