<?php

namespace App\Http\Middleware;
use Closure;
use Session;
use Alert;
use Auth;

class isRedaktur
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user() AND Auth::user()->level_id == 1 OR Auth::user()->level_id == 2){
                return $next($request);
            }
            alert()->error('Sistem akan redirect ke halaman home...', 'Anda tidak dapat mengakses halaman tersebut')->persistent('Close');
            return redirect('/');
        }
        else{
            alert()->error('Sistem akan redirect ke halaman home...', 'Anda tidak dapat mengakses halaman tersebut')->persistent('Close');
            return redirect('/');         
        }
    }
}
