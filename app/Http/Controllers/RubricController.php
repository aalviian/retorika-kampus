<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Http\Requests;

use App\Rubric;

use Session;

use Validator;



class RubricController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request)

    { 

        try{

            $query = $request->get('query');

            if($request->has('jumlah-data')) {

                $rubrics = Rubric::orderBy('parent_id','asc')->where('name','LIKE','%'.$query.'%')->paginate($request->get('jumlah-data'));

            }

            else {

                $rubrics = Rubric::where('name','LIKE','%'.$query.'%')->paginate(5);

            } 

            $count = Rubric::count();

            return view("rubric.index", compact('rubrics', 'count'));

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return \Response::view('errors.query',array(),500);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        return view('rubric.create');

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        if(empty($request->parent_id)) $request->parent_id = 0;



        $rules = [

            'name' => 'required|string|max:255|unique:rubrics', 

            // 'parent_id' => 'required|exists:rubrics,id',

        ];



        $messages = [

            'required' => 'Field harus di isi alias tidak boleh kosong',

            'unique' => 'Rubrik sudah ada dalam database atau masih ada di dalam Trash'

            // 'exists' => 'Parent tidak ditemukan'

        ];



        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menambahkan data rubrik"]);

            return redirect() -> route('rubric.index')->withErrors($validator)->withInput();

        }

 

        $rubric = new Rubric;

        $rubric -> slug = str_slug($request->name, '-');

        $rubric -> name = $request->name;

        $rubric -> parent_id = $request->parent_id;



        if($rubric->parent_id == 0){

            $rubric->order = Rubric::max('order') + 1;

        }



        $rubric -> save();

        if($rubric){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menambahkan rubric '".$request->name."' kedalam database"]);
            return redirect()->route('rubric.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menambahkan rubric '".$request->name."' kedalam database"]);
            return redirect()->route('rubric.index')->withInput();
        }



    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    { 

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $rules = [

            'name' => 'required|string|max:255|unique:rubrics,name,'.$id,

            // 'parent_id' => 'required|exists:rubrics,id'

        ];



        $messages = [

            'required' => 'Field harus di isi alias tidak boleh kosong',

            'unique' => 'Rubrik sudah ada dalam database atau masih ada didalam Trash',

            // 'exists' => 'Parent tidak ditemukan',

        ];



        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data rubrik"]);

            return redirect() -> route('rubric.index')->withErrors($validator)->withInput();

        }


        $rubric = Rubric::find($id);

        if($rubric->hasChild()){
            Session::flash('flash_notification', ["level"=>"danger", "message"=>'Maaf, rubric '+$rubric->name+'adalah parent bagi rubric lain']);
                return redirect()->route('rubric.index');
        }
        else{

            if($request->parent_id != $rubric->id){
                
                $rubric -> slug = str_slug($request->name, '-');

                $rubric -> name = $request->name;

                if(empty($request->parent_id))

                    $rubric -> parent_id = 0;

                else

                    $rubric -> parent_id = $request->parent_id;

                $rubric->save();

                if($rubric){
                    Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah data rubric kedalam database"]);
                    return redirect()->route('rubric.index');
                }
                else{
                    Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data rubric"]);
                    return redirect()->route('rubric.index');
                }

            }
            else{

                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Maaf, parent dan child tidak boleh sama"]);
                    return redirect()->route('rubric.index');
            
            }
            
        }

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        $data = Rubric::find($id);

        $rubric = Rubric::find($id)->delete();

        $childs = Rubric::where('parent_id', $id)->get();



        foreach ($childs as $child) {

            $child->update([

                'parent_id' => 0,

                'order' => Rubric::max('order')+1

            ]);

        }

        if($rubric and $childs){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menghapus rubric '".$data->name."'"]);
            return redirect()->route('rubric.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menghapus rubric '".$data->name."'"]);
            return redirect()->route('rubric.index');   
        }

    }



    public function rubricOrder(){

        $rubrics = Rubric::noParent()->get();

        return view('rubric.rubric-order', compact('rubrics'));

    }



    public function orderSave(Request $request){

        $rules = [

            'order' => 'required|exists:rubrics,order',

        ];



        $messages = [

            'required' => 'Field harus di isi alias tidak boleh kosong',

            'exists' => 'No urut tidak valid atau melebihi batas jumlah rubrik'

        ];



        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah urutan rubrik"]);

            return redirect('rubric-order')->withErrors($validator)->withInput();

        }

        for($i=0; $i<count($request->name); $i++){

            $rubric = Rubric::where('name', $request->name[$i])->update(['order'=>$request->order[$i]]);

        }

        if($rubric){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah urutan rubrik"]);
            return redirect('rubric-order');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah urutan rubrik"]);
            return redirect('rubric-order');
        }


    }

}

