<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Http\Requests;

use App\Highlight;

use Session;

use Auth;

use Validator;

use File;



class HighlightController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */



    // public function __construct() {

    //     $this->middleware('isAdmin');

    //     $this->middleware('isRedaktur');

    // }



    public function index(Request $request) 

    {

        try{

            $query = $request->get('query'); 

            if($request->has('jumlah-data')) {

                $highlights = Highlight::where('name','LIKE','%'.$query.'%')->orWhere('description','LIKE','%'.$query.'%')->orderBy('created_at', 'desc')->paginate($request->get('jumlah-data'));

            }

            else {

                $highlights = Highlight::where('name','LIKE','%'.$query.'%')->orWhere('description','LIKE','%'.$query.'%')->orderBy('created_at', 'desc')->paginate(5);

            }

            $count = Highlight::count();

            return view("highlight.index", compact('highlights', 'count'));

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return \Response::view('errors.query',array(),500);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }

    } 



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        return view('highlight.create');

    }



    /**

     * Store a newly created resource in storage. 

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $rules = [

            'name' => 'required|string|max:255|unique:highlights',

            'image' => 'required|image|mimes:jpeg,jpg,png|max:2000|',

        ];



        $messages = [

            'required' => 'Field harus di isi alias tidak boleh kosong',

            'image' => 'Data harus berbenuk gambar',

            'unique' => 'Fokus sudah ada dalam database atau masih ada didalam Trash', 

            'mimes' => 'Image harus berekstensi JPEG, JPG, dan PNG'

        ];



        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menambahkan data highlight"]);

            return redirect() -> route('highlight.index')->withErrors($validator)->withInput();

        }



        $highlight = new Highlight;

        $highlight -> user_id = Auth::user()->id;

        $highlight -> slug = str_slug($request->name, '-');

        $highlight -> name = $request->name;

        if($request->hasFile('image')){

            $highlight -> image = $this->savePhoto($request->file('image'));

        }

        $highlight -> order = Highlight::max('order') + 1;

        $highlight -> save();

        if($highlight){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menambahkan highlight '".$request->name."' kedalam database"]);
            return redirect()->route('highlight.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menambahkan highlight '".$request->name."' kedalam database"]);
            return redirect()->route('highlight.index')->withInput();
        }


    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $highlight = Highlight::find($id);

        return view('highlight.edit', compact('highlight'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $rules = [

            'name' => 'required|string|max:255|unique:highlights,name,'.$id,

            'image' => 'required|image|mimes:jpeg,jpg,png|max:2000|',

        ];



        $messages = [

            'required' => 'Field harus di isi alias tidak boleh kosong',

            'image' => 'Data harus berbenuk gambar',

            'unique' => 'Fokus sudah ada dalam database atau masih ada didalam Trash',

            'mimes' => 'Image harus berekstensi JPEG, JPG, dan PNG'

        ];



        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data highlight"]);

            return redirect() -> route('highlight.index')->withErrors($validator)->withInput();

        }



        $data = Highlight::find($id);

        $highlight = Highlight::find($id);

        $highlight -> user_id = Auth::user()->id;

        $highlight -> slug = str_slug($request->name, '-');

        $highlight -> name = $request->name;

        if ($request->hasFile('image')){

            $highlight -> image = $this->savePhoto($request->file('image'));

            if($data->image !== '') $this->deletePhoto($data->image);

        } 

        $highlight -> save();

        if($highlight){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah highlight '".$data->name."' menjadi '".$highlight->name."' kedalam database"]);
            return redirect()->route('highlight.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data highlight"]);
            return redirect() -> route('highlight.index')->withInput();
        }


    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        $data = Highlight::find($id);

        $highlight = Highlight::find($id)->delete();

        if($highlight){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menghapus highlight '".$data->name."'"]);
            if($data->image !== '') $this->deletePhoto($data->image);
            return redirect()->route('highlight.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menghapus highlight '".$data->name."'"]);
            return redirect()->route('highlight.index');
        }


    }



    public function savePhoto(UploadedFile $photo) {

        $fileName = str_random(40) . '.' . $photo->guessClientExtension();

        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/data/highlight';

        $photo -> move($destinationPath, $fileName);

        return $fileName;

    }



    public function deletePhoto($filename){

        $path = public_path() . DIRECTORY_SEPARATOR . 'img/data/highlight/'.$filename;

        return File::delete($path);

    }



    public function highlightOrder(){

        $highlights = Highlight::all();

        return view('highlight.highlight-order', compact('highlights'));

    }



    public function orderSave(Request $request){

        $count = Highlight::count();

        if($request->has('order')){

            $rules = [

                'order[]' => "max:{$count}|unique:highlights,order",

            ];



            $messages = [

                'max' => 'Order tidak boleh melebih batas maksimum data',

                'unique' => 'Order tidak boleh sama'

            ];



            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah urutan highlight"]);

                return redirect('highlight-order')->withErrors($validator)->withInput();

            }

        }

        for($i=0; $i<count($request->name); $i++){

            $highlight = Highlight::where('name', $request->name[$i])->update(['order'=>$request->order[$i]]);

        }

        if($highlight){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah urutan highlight"]);
            return redirect('highlight-order'); 
        }
        else{
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Oops, gagal mengubah urutan highlight"]);
            return redirect('highlight-order'); 
        }


    }

}

