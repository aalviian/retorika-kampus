<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Comment;

use App\News;

use App\Rubric;

use Validator;

use Session;

use Alert;

use DB;



class CommentController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        //

        try{

            $count = Comment::count();

            $comments = Comment::orderBy('created_at', 'desc')->paginate(10);

            return view('comment.index', compact('comments','count'));

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return \Response::view('errors.query',array(),500);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $rules = [

            'name' => 'required|string|max:255',

            'email' => 'required|string|email|max:255|',

            'message' => 'required'

        ];



        $messages = [

            'required' => 'Field harus di isi alias tidak boleh kosong',

            'email' => 'Email tidak valid',

        ];



        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menambahkan komentar"]);

            return back()->withErrors($validator)->withInput();

        }

        $comment = Comment::create([

            'news_id' => $request->news_id,

            'name' => $request->name,

            'email' => $request->email,

            'message' => $request->message

        ]);

        $rubric_slug = $request->slug;

        $news_slug = DB::table('news')->where('id',$comment->news_id)->value('slug');

        if($comment){

            Alert::success('Comment Successfully', 'Terima kasih sudah berkomentar')->persistent('Close');

            return redirect('/berita/rubrik/'.$rubric_slug.'/'.$news_slug);

       }

        else{

            Alert::error('Comment Failed', 'Oops, silahkan coba lagi!')->persistent('Close');

            return redirect('/berita/rubrik/'.$rubric_slug.'/'.$news_slug);

        }

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        //

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        $comment = Comment::find($id)->delete();
        if($comment){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menghapus comment dari database"]);
            return redirect()->route('comment.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menghapus comment dari database"]);
            return redirect()->route('comment.index');
        }

    }

}