<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advertisement;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Session;
use Auth;
use Validator;
use File;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $advertisements = Advertisement::orderBy('created_at', 'desc')->paginate(5);
            return view('advertisement.index', compact('advertisements'));
        }
        // catch (\Illuminate\Database\QueryException $e) {
        //     return \Response::view('errors.query',array(),500);
        // }
        catch (\Exception $e) {
            return \Response::view('errors.500',array(),500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|string|max:255|unique:advertisements',
            'type' => 'required|in:depan,rubrik,berita,fokus',
            'image' => 'image|mimes:jpeg,jpg,png,gif|max:5000|',
            'url' => 'url|unique:advertisements',
            'description' => 'string|max:255',
            'order' => 'numeric|in:1,2,3,4,5'
        ];

        $messages = [
            'required' => 'Field harus di isi alias tidak boleh kosong',
            'image' => 'Data harus berbenuk gambar',
            'unique' => 'Data sudah ada dalam database', 
            'mimes' => 'Image harus berekstensi JPEG, JPG, dan PNG',
            'url' => 'URL tidak valid',
            'in' => 'Order melebihi batas'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menambahkan data advertisement"]);
            return redirect() -> route('advertisement.index')->withErrors($validator)->withInput();
        }

        $advertisements = Advertisement::all();
        $check_advertisement = Advertisement::where('type', $request->type)->where('order', $request->order)->count();
        if($check_advertisement) {
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Order dengan tipe yang sama sudah ada"]);
            return redirect()->route('advertisement.index')->withInput();        
        }

        $data = $request->except(['image']);
        if ($request->hasFile('image')){
            $data['image'] = $this->savePhoto($request->file('image'));
        } 

        $advertisement = Advertisement::create($data);
        if($advertisement){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menambahkan advertisement '".$request->title."' kedalam database"]);
            return redirect()->route('advertisement.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menambahkan advertisement"]);
            return redirect()->route('advertisement.index')->withInput();  
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required|string|max:255|unique:advertisements,title,'.$id,
            'type' => 'required|in:depan,rubrik,berita,fokus',
            'image' => 'image|mimes:jpeg,jpg,png,gif|max:5000|',
            'url' => 'url|unique:advertisements,url,'.$id,
            'description' => 'string|max:255',
            'order' => 'numeric|in:1,2,3,4,5'
        ];

        $messages = [ 
            'required' => 'Field harus di isi alias tidak boleh kosong',
            'image' => 'Data harus berbenuk gambar',
            'unique' => 'Data sudah ada dalam database', 
            'mimes' => 'Image harus berekstensi JPEG, JPG, dan PNG',
            'url' => 'URL tidak valid'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah advertisement"]);
            return redirect() -> route('advertisement.index')->withErrors($validator)->withInput();
        }

        $advertisement = Advertisement::find($id);
        if($advertisement->type != $request->type or $advertisement->order != $request->order){
            $check_advertisement = Advertisement::where('type', $request->type)->where('order', $request->order)->count();
        }
        else{
            $check_advertisement = 0;
        }

        if($check_advertisement) {
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Order dengan tipe yang sama sudah ada"]);
            return redirect()->route('advertisement.index')->withInput();        
        }

        $olddata = Advertisement::find($id);
        $data = $request->except(['image']);
        if ($request->hasFile('image')){
            $data['image'] = $this->savePhoto($request->file('image'));
            if($olddata->image !== '') $this->deletePhoto($olddata->image);
        } 

        $advertisement = $advertisement->update($data);
        if($advertisement){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah data advertisement kedalam database"]);
            return redirect()->route('advertisement.index');    
        }
        else {
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah advertisement"]);
            return redirect() -> route('advertisement.index')->withErrors($validator)->withInput();   
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        $data = Advertisement::find($id);
        $advertisement = Advertisement::find($id)->delete();
        if($advertisement){
            if($data->image !== '') $this->deletePhoto($data->image);
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menghapus advertisement '".$data->title."'"]);
            return redirect()->route('advertisement.index');
        }
        else {
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menghapus advertisement"]);
            return redirect() -> route('advertisement.index')->withErrors($validator)->withInput();   
        }
    }

    public function savePhoto(UploadedFile $photo) {
        $fileName = str_random(40) . '.' . $photo->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/data/advertisement';
        $photo -> move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename){
        $path = public_path() . DIRECTORY_SEPARATOR . 'img/data/advertisement/'.$filename;
        return File::delete($path);
    }

}
