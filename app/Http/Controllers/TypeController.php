<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Type;

use Session;

use Validator;

use DB;



class TypeController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        try{

            $types = Type::orderBy('created_at', 'desc')->paginate(5);

            $count = Type::count();

            return view("type.index", compact('types', 'count'));

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return \Response::view('errors.query',array(),500);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $rules = [

            'name' => 'required|string|max:255|unique:types',

        ];



        $messages = [

            'required' => 'Field harus di isi alias tidak boleh kosong',

            'unique' => 'Tipe sudah ada dalam database atau masih ada di dalam Trash'

        ];



        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menambahkan data tipe news"]);

            return redirect() -> route('type.index')->withErrors($validator)->withInput();

        }



        $type = new Type;

        $type -> slug = str_slug($request->name, '-');

        $type -> name = $request->name;

        $type -> save();

        if($type){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menambahkan type '".$request->name."' kedalam database"]);

            return redirect()->route('type.index');   
        }


    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $rules = [

            'name' => 'required|string|max:255|unique:types,name,'.$id,

        ];



        $messages = [

            'required' => 'Field harus di isi alias tidak boleh kosong',

            'unique' => 'Tipe sudah ada dalam database atau masih ada didalam Trash'

        ];



        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data tipe news"]);

            return redirect() -> route('type.index')->withErrors($validator)->withInput();

        }



        $data = Type::find($id);

        $type = Type::find($id);

        $type -> slug = str_slug($request->name, '-');

        $type -> name = $request->name;

        $type->save();

        if($type){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah type '".$data->name."' menjadi '".$type->name."' kedalam database"]);
            return redirect()->route('type.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah type '".$data->name."' menjadi '".$type->name."' kedalam database"]);
            return redirect()->route('type.index');   
        }


    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        $data = Type::find($id);

        $type = Type::find($id)->delete();

        $news = DB::table('news')->get();

        // if($data->name == "Front Headline"){

        //     foreach($news as $f_news){

        //         $f_news->type()->sync()

        //     }

        // }

        if($type){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menghapus type '".$data->name."'"]);
            return redirect()->route('type.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menghapus type '".$data->name."'"]);
            return redirect()->route('type.index');
        }


    }

}

