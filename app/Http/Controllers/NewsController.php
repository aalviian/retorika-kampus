<?php



namespace App\Http\Controllers;
 


use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Http\Requests;

use App\News;
use App\Rubric;

use Session;

use Auth;

use DB;

use Validator;

use App\Highlight;

use File;
use Alert;





class NewsController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request) 

    {

        try{

            $news = News::orderBy('created_at', 'desc')->paginate(10);
            $pendings = News::orderBy('created_at', 'desc')->where('status', 'pending')->paginate(10);
            $published = News::orderBy('created_at', 'desc')->where('status', 'published')->paginate(10);

            $news_user = News::where('created_by', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(10);
            $news_pending_user = News::where('created_by', Auth::user()->id)->orderBy('created_at', 'desc')->where('status', 'pending')->paginate(10);
            $news_published_user = News::where('created_by', Auth::user()->id)->orderBy('created_at', 'desc')->where('status', 'published')->paginate(10);

            $count = News::count();

            return view("news.index", compact('news', 'pendings', 'published', 'news_user', 'news_pending_user', 'news_published_user', 'count'));

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return \Response::view('errors.query',array(),500);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }



    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

        $pluck=[];
        $rubrics = Rubric::where('parent_id', 0)->get();
        foreach($rubrics as $rubric){
            $pluck += [ $rubric->id => $rubric->name ]; 
            $rubric_childs = Rubric::where('parent_id', $rubric->id)->get();
            foreach($rubric_childs as $rubric_child){
                $pluck += [ $rubric_child->id => $rubric->name.' -> '.$rubric_child->name ];
            }
        }

        return view('news.create', compact('pluck'));

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $rules = [

            'title' => 'required|string|max:255|unique:news',

            'content' => 'required',

            'image_source' => 'required|url',

            'image' => 'required|image|mimes:jpeg,jpg,png|max:2000|dimensions:width=1200,height=673',
            
            'rubric' => 'required|exists:rubrics,id',

            'highlight' => 'exists:highlights,id',

            'type' => 'exists:types,id'

        ];



        $messages = [

            'required' => 'Field harus di isi alias tidak boleh kosong',

            'unique' => 'Judul berita sudah ada dalam database atau masih ada didalam Trash',

            'image' => 'Gambar berita harus berbentuk image dan ukuran harus berukuran 1200x673', 

            'mimes' => 'Image harus berekstensi JPEG, JPG, dan PNG',

            'url' => 'URL tidak valid, misal: http://www.facebook.com',

            'min' => 'Rubrik tidak tersedia',

            'max' => 'Rubrik tidak tersedia',

            'dimensions'=> 'Gambar harus berukuran 1200 x 673'

        ];



        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menambahkan data news"]);

            return redirect() -> route('news.create')->withErrors($validator)->withInput();

        }



        $news = new News;

        $news -> rubric_id = $request->rubric;

        $news -> slug = str_slug($request->title, '-');

        $news -> title = $request->title;

        $news -> content = $request->content;

        $news -> image = $this->savePhoto($request->file('image'));

        $news -> image_source = $request->image_source;

        $news -> created_by = Auth::user()->id;

        $news -> edited_by = Auth::user()->id;

        $news -> approved_by = NULL;

        $news -> status = 'pending';

        if($request->save_as == 'draft')
            $news -> is_draft = 1;
        else
            $news -> is_draft = 0;

        $news -> views = 0;

        $news -> keywords = $request->keywords;

        $news -> save();


        if(isset($request->highlight))
            $news->highlight()->sync($request->highlight);

        if(isset($request->type))
            $news->type()->sync($request->type); 

        if($news){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menambahkan news '".$request->name."' kedalam database"]);
            return redirect()->route('news.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menambahkan news '".$request->name."' kedalam database"]);
            return redirect() -> route('news.create')->withInput();
        }


        

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

        $news = News::find($id);
        $pluck=[];
        $rubrics = Rubric::where('parent_id', 0)->get();
        foreach($rubrics as $rubric){
            $pluck += [ $rubric->id => $rubric->name ]; 
            $rubric_childs = Rubric::where('parent_id', $rubric->id)->get();
            foreach($rubric_childs as $rubric_child){
                $pluck += [ $rubric_child->id => $rubric->name.' -> '.$rubric_child->name ];
            }
        }


        return view('news.edit', compact('news', 'pluck'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $rules = [

            'title' => 'required|string|max:255|unique:news,title,'.$id,

            'content' => 'required',

            'image_source' => 'required|url',

            'image' => 'image|mimes:jpeg,jpg,png|max:2000|dimensions:width=1200,height=673',
            
            'rubric' => 'required|exists:rubrics,id',

            'highlight' => 'exists:highlights,id',

            'type' => 'exists:types,id'

        ];



        $messages = [

            'required' => 'Field harus di isi alias tidak boleh kosong',

            'unique' => 'Judul berita sudah ada dalam database atau masih ada didalam Trash',

            'mimes' => 'Image harus berekstensi JPEG, JPG, dan PNG',

            'url' => 'URL tidak valid, misal: http://www.facebook.com',

            'min' => 'Rubrik tidak tersedia',

            'max' => 'Rubrik tidak tersedia',

            'dimensions'=> 'Gambar harus berukuran 1200 x 673'

        ];



        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data news"]);

            return redirect() -> route('news.edit', $id)->withErrors($validator)->withInput();

        }



        $data = News::find($id);

        $news = News::find($id);

        $news -> rubric_id = $request->rubric;

        $news -> slug = str_slug($request->title, '-');

        $news -> title = $request->title;

        $news -> content = $request->content;

        $news -> keywords = $request->keywords;

        

        if ($request->hasFile('image')){

            $news -> image = $this->savePhoto($request->file('image'));

            if($data->image !== '') $this->deletePhoto($data->image);

        }

        $news -> edited_by = Auth::user()->id;

        $news -> image_source = $request->image_source;

        if($request->save_as == 'draft')
            $news -> is_draft = 1;
        else
            $news -> is_draft = 0;

        $news -> save();

        if(isset($request->highlight))
            $news->highlight()->sync($request->highlight);

        if(isset($request->type))
            $news->type()->sync($request->type); 
        
        if($news){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah news '".$data->title."' menjadi '".$news->title."' kedalam database"]);
            return redirect()->route('news.index');            
        }
        else{
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Oops, gagal mengubah news '".$data->title."' menjadi '".$news->title."' kedalam database"]);
            return redirect() -> route('news.edit', $id)->withInput(); 
        }



    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    

    public function destroy($id)

    {

        $data = News::find($id);

        $news = News::find($id)->delete();

        if($news){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menghapus news '".$data->title."'"]);
            return redirect()->route('news.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menghapus news '".$data->title."'"]);
            return redirect()->route('news.index');
        }



    }



    public function savePhoto(UploadedFile $photo) {

        $fileName = str_random(40) . '.' . $photo->guessClientExtension();

        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/data/news';

        $photo -> move($destinationPath, $fileName);

        return $fileName;

    }



    public function deletePhoto($filename){

        $path = public_path() . DIRECTORY_SEPARATOR . 'img/data/news/'.$filename;

        return File::delete($path);

    }

    public function updateStatus(Request $request, $id)

    {

        if(isset($request->status_change)){

            $news = News::find($id);

            $news -> status = $request->status_change;

            if($request->status_change=='published'){

                $news -> approved_by = Auth::user()->id;

            } else {

                $news -> approved_by = NULL;

            }

            $news -> save();

            if($news){
                Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah news"]);
                return redirect()->route('news.index');            
            }
            else{
                Session::flash('flash_notification', ["level"=>"success", "message"=>"Oops, gagal mengubah news"]);
                return redirect()->route('news.index');
            }
        }
        else{
            Alert::message('Status tidak boleh kosong!', 'Oopss..')->persistent('Close');
                return redirect('/news');
        }
    }

}

