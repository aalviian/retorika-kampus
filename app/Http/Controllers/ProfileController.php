<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\User;

use Auth;

use Session;

use Validator;

use File;

use Hash;



class ProfileController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        try{

            $user = Auth::user();

            return view('profile.index', compact('user'));

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return response()->make(view('errors.404'), 404);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        //

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */ 

    public function update(Request $request, $id)

    { 

        $data = User::find($id);

        $user = User::find($id);

        if ($request->hasFile('avatar')){

            $rules = [



                'avatar' => 'required|image|mimes:jpeg,jpg,png',

            ];

            

            $messages = [

                'required' => 'Field harus di isi alias tidak boleh kosong',

                'image' => 'Data harus berbenuk gambar',

                'mimes' => 'Image harus berekstensi JPEG, JPG, dan PNG'

            ];



            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data profile"]);

                return redirect() -> route('profile.index')->withErrors($validator)->withInput();

            }



            $user -> avatar = $this->savePhoto($request->file('avatar'));

            $user->save();

            if($data->avatar !== '') $this->deletePhoto($data->avatar);

            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah avatar"]);

            return redirect() -> route('profile.index');

        }



        if ($request->has('name')){

            $rules = [

                'name' => 'required',

                'username' => 'required|unique:users,username,'.$id,

                'email' => 'required|email|unique:users,email,'.$id,

            ];



            $messages = [

                'required' => 'Field harus di isi alias tidak boleh kosong',

                'unique' => 'Data sudah ada dalam database',

                'email' => 'Email tidak valid'

            ];



            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data profile"]);

                return redirect() -> route('profile.index')->withErrors($validator)->withInput();

            }



            $user->update($request->all());

            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah data profile"]);

            return redirect() -> route('profile.index');

        }



       if ($request->has('old_password')){

            Validator::extend('old_password', function($attribute, $value) use ($data) {

                return Hash::check($value, $data->password );

            });



            $rules = [

                'old_password' => 'required|old_password:' . Auth::user()->password,

                'password' => 'required|min:6|different:old_password|confirmed',

            ];



            $messages = [

                'required' => 'Field harus di isi alias tidak boleh kosong',

                'old_password' => 'Password lama tidak sesuai',

                'different' => 'Password baru sama dengan password lama',

                'confirmed' => 'Password tidak match'

            ];



            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()) {

                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data password"]);

                return redirect() -> route('profile.index')->withErrors($validator)->withInput();

            }



            $user->password = bcrypt($request->password);

            $user->save();

            if($user){
                Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah data user"]);
                return redirect() -> route('profile.index');
            }
            else{
                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data"]);
                return redirect() -> route('profile.index');
            }


        }



    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        //

    }



    public function savePhoto(UploadedFile $photo) {

        $fileName = str_random(40) . '.' . $photo->guessClientExtension();

        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/data/user';

        $photo -> move($destinationPath, $fileName);

        return $fileName;

    }



    public function deletePhoto($filename){

        $path = public_path() . DIRECTORY_SEPARATOR . 'img/data/user/'.$filename;

        return File::delete($path);

    }



}

