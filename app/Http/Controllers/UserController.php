<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Notifications\UserCreating as UserCreatingNotification;

use App\Notifications\UserConfirmation as UserConfirmationNotification;

use App\User;

use App\News;

use Mail;

use Faker\Factory as Faker;

use Session;

use Validator;

use Auth;

use Alert;



class UserController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        try{

            $count = User::count();

            $users = User::orderBy('created_at', 'desc')->paginate(10);

            return view('member.index', compact('users','count'));

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return \Response::view('errors.query',array(),500);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        return view('member.create');

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $data)

    {

        $faker=Faker::create("en_En");

        $data['username'] = $faker->unique()->username;

        $data['password'] = $faker->unique()->username;



        $rules = [

            'name' => 'required|string|max:255',

            'username' => 'required|unique:users',

            'password' => 'required',

            'email' => 'required|string|email|max:255|unique:users',

            'phone' => 'required|numeric|unique:users',

            'level_id' => 'required|in:1,2,3,4'

        ];



        $messages = [

            'required' => 'Field harus di isi alias tidak boleh kosong',

            'email.unique' => 'Email '.$data['email'].' sudah ada dalam database.',

            'phone.unique' => 'Mobile phone '.$data['phone'].' sudah ada dalam database.',

            'numeric' => 'Mobile phone tidak valid',

            'email' => 'Email tidak valid',

            'username.unique' => 'Usernama random '.$data['username'].' sudah ada dalam database',

        ];



        $validator = Validator::make($data->all(), $rules, $messages);

        if($validator->fails()) {

            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menambahkan data member"]);

            return redirect() -> route('member.index')->withErrors($validator)->withInput();

        }



        $phone = '+62'.$data['phone'];



        $user = User::create([

            'slug' => str_slug($data['name'], '-'),

            'name' => $data['name'],

            'username' => $data['username'],

            'email' => $data['email'],

            'password' => bcrypt($data['password']),

            'avatar' => '',

            'phone' => $phone,

            'bio' => 'Hello, I am a new membership of retorika kampus',

            'level_id' => $data['level_id'],

            'is_confirmed' => 1

        ]);



        $iduser = $user->id;



        if($data['level'] == 1) $role = 'Admin';

        else if($data['level'] == 2) $role = 'Redaktur';

        else if($data['level'] == 3) $role = 'Reporter';

        else $role = 'User';



        $datas = [

            'name' => $data['name'],

            'username' => $data['username'],

            'email' => $data['email'],

            'password' => $data['password'],

            'phone' => $phone,

            'level' => $role 

        ];



        // Mail::send('vendor.notifications.member', $datas, function($message) use($user) {

        //     $message->to($user->email);

        //     $message->subject('Membership of Retorika Kampus');

        // });



        $user->notify(new UserCreatingNotification($datas));

        if($user){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menambahkan member '".$user->name."' kedalam database"]);
            return redirect()->route('member.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menambahkan member '".$user->name."' kedalam database"]);
            return redirect()->route('member.index')->withInput();
        }



    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $member = User::find($id);

        return view('member.edit',compact('member'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $user = User::find($id);

        $rules = [

            'name' => 'required|string|max:255',

            'username' => 'required|unique:users,username,'.$user->id,

            'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,

            'level_id' => 'required|in:1,2,3,4'

        ];



        $messages = [

            'required' => 'Field harus di isi alias tidak boleh kosong',

            'username.unique' => 'Username '.$request->username.' sudah ada dalam database.',

            'email.unique' => 'Email '.$request->email.' sudah ada dalam database.',

            'email' => 'Email tidak valid',

        ];



        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) {

            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data member"]);

            return redirect() -> route('member.index')->withErrors($validator)->withInput();

        }



        $user->update([

            'name' => $request->name,

            'username' => $request->username,

            'email' => $request->email,

            'level_id' => $request->level_id,

            'is_confirmed' => $request->is_confirmed

        ]);



        if($user){

            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah data member"]);

            return redirect()->route('member.index');

        }

        else{

            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data member"]);

            return redirect()->route('member.index');

        }

        

    } 



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        try {

            $data = User::find($id);

            $news_edited = News::where('edited_by', $id)->update(['edited_by'=>NULL]);

            $news_approved = News::where('approved_by', $id)->update(['approved_by'=>NULL, 'status'=>'pending']);

            $member = User::find($id)->delete();

            if($member){

                Session::flash('flash_notification', ["level"=>"success", "message"=>'Berhasil menghapus member'.$data->name]);

                return redirect()->route('member.index');

            }

            else{

                Session::flash('flash_notification', ["level"=>"danger", "message"=>'Oops, gagal menghapus member'.$data->name]);

                return redirect()->route('member.index');

            }

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return \Response::view('errors.query',array(),500);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }

        

    }



    public function userStore(Request $data){

        $rules = [

            'name' => 'required|string|max:255',

            'username' => 'required|unique:users',

            'password' => 'required|confirmed',

            'email' => 'required|string|email|max:255|unique:users',

            'phone' => 'required|numeric|unique:users'

        ];



        $messages = [

            'required' => 'Field harus di isi alias tidak boleh kosong',

            'confirmed' => 'Password tidak match',

            'email.unique' => 'Email '.$data['email'].' sudah ada dalam database.',

            'phone.unique' => 'Mobile phone '.$data['phone'].' sudah ada dalam database.',

            'numeric' => 'Mobile phone tidak valid',

            'email' => 'Email tidak valid',

            'username.unique' => 'Usernama '.$data['username'].' sudah ada dalam database',

        ];



        $validator = Validator::make($data->all(), $rules, $messages);

        if($validator->fails()) {

            Alert::error('Created Failed', 'Oops, silahkan coba lagi!')->persistent('Close');

            return redirect('/')->withErrors($validator)->withInput();

        }



        $phone = '+62'.$data['phone'];



        $user = User::create([

            'slug' => str_slug($data['name'], '-'),

            'name' => $data['name'],

            'username' => $data['username'],

            'email' => $data['email'],

            'password' => bcrypt($data['password']),

            'avatar' => '',

            'phone' => $phone,

            'bio' => 'Hello, I am a new membership of retorika kampus',

            'level_id' => 4,

            'is_confirmed' => 0

        ]);



        $datas = [

            'id' => $user->id,

            'name' => $data['name'],

            'username' => $data['username'],

            'email' => $data['email'],

            'password' => $data['password'],

            'phone' => $phone,

            'level' => "User" 

        ];



        $admin = User::find(1)->first();

        $admin->notify(new UserConfirmationNotification($datas));

        $user->notify(new UserCreatingNotification($datas));



        if($user){

            Alert::success('Created Successfully', 'Terima kasih!')->persistent('Close');

            return redirect('/');

        }

        else{

            Alert::error('Created Failed', 'Oops, silahkan coba lagi!')->persistent('Close');

            return redirect('/')->withErrors($validator)->withInput();;

        }

    }



    public function userConfirm($id){

        $user = User::find($id);

        if(Auth::check()){
            if(Auth::user()->id == 1 or Auth::user()->id == 1){
                $user->update(['is_confirmed' => 1]);

                Alert::success('Confirmed Successfully', 'User '.$user->name.' telah di konfirmasi')->persistent('Close');

                return redirect('/member');
            }
            else{
                Alert::error('Confirmation is failed', 'Oops, kamu tidak punya wewenang!')->persistent('Close');
                return redirect('/');
            }
        }
        else{
            Alert::error('Confirmation is failed', 'Oops, silahkan login terlebih dahulu!')->persistent('Close');
            return redirect('/');
        }
       

    }

}

