<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use DB;
use Session;
use Alert;
use Validator;
use File;
use Hash;
use Auth;
use App\Configuration;

class ConfigController extends Controller
{
    public function index(){
    	$config = Configuration::find(1)->first();
    	if($config != NULL){
    		return view('config.index', compact('config'));
    	}
    	else{
    		abort(401);
    	}
    }

    public function update(Request $request, $id){
    	$data = Configuration::find($id); 
    	$config = Configuration::find($id);
    	if ($request->hasFile('logo')){
            $rules = [
                'logo' => 'required|image|mimes:jpeg,jpg,png',
            ];
            
            $messages = [
                'required' => 'Field harus di isi alias tidak boleh kosong',
                'image' => 'Logo harus berbenuk gambar',
                'mimes' => 'Image harus berekstensi JPEG, JPG, dan PNG'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()) {
                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data logo config"]);
                return redirect('config')->withErrors($validator)->withInput();
            }

            $config -> logo = $this->savePhoto($request->file('logo'));
            $config->save();
            if($data->avatar !== '') $this->deletePhoto($data->avatar);
            if($config){
	            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah data logo"]);
	            return redirect('config');
	        }
	        else{
                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data logo config"]);
                return redirect('config');
	        }
        }

        if($request->has('facebook')){
            $rules = [
                'facebook' => 'required|url',
                'twitter' => 'required|url',
                'email' => 'required|email',
            ];
            
            $messages = [
                'required' => 'Field harus di isi alias tidak boleh kosong',
                'image' => 'Logo harus berbenuk gambar',
                'mimes' => 'Image harus berekstensi JPEG, JPG, dan PNG',
                'email' => 'Email tidak valid',
                'url' => 'URL tidak valid'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()) {
                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data social network config"]);
                return redirect('config')->withErrors($validator)->withInput();
            }
            $config->update($request->all());
            if($config){
				Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah data social network"]);
	            return redirect('config');
	        }
	       	else{
                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data social network"]);
                return redirect('config');
	        }
        }

        if($request->has('phone_number')){
            $rules = [
                'phone_number' => 'required|numeric',
                'mobile_number' => 'required|numeric',
                'fax_number' => 'required|numeric',
            ];
            
            $messages = [
                'required' => 'Field harus di isi alias tidak boleh kosong',
                'numeric.phone_number' => 'Nomor telepon tidak valid',
                'numeric.mobile_number' => 'Nomor handphone tidak valid',
                'numeric.fax_number' => 'Nomor fax tidak valid'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()) {
                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data config"]);
                return redirect('config')->withErrors($validator)->withInput();
            }
            $config->update($request->all());
            if($config){
				Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah data social network"]);
	            return redirect('config');
	        }
	       	else{
                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data social network"]);
                return redirect('config');
	        }
        }

        if($request->has('web_name')){
            $rules = [
                'web_name' => 'required|string',
                'url' => 'required|url',
                'tagline' => '',
                'keywords' => 'string',
                'lat' => 'numeric',
                'long' => 'numeric',
                'address' => 'required',
            ];
            
            $messages = [
                'required' => 'Field harus di isi alias tidak boleh kosong',
                'url' => 'Web tidak valid',
                'numeric.lat' => 'Latitude tidak valid',
                'numeric.long' => 'Longitude tidak valid'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()) {
                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data general config"]);
                return redirect('config')->withErrors($validator)->withInput();
            }
            $config->update($request->all());
            if($config){
				Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah data general config"]);
	            return redirect('config');
	        }
	       	else{
                Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal mengubah data general config"]);
                return redirect('config');
	        }
        }
    }

    public function savePhoto(UploadedFile $photo) {
        $fileName = str_random(40) . '.' . $photo->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'images/general';
        $photo -> move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename){
        $path = public_path() . DIRECTORY_SEPARATOR . 'images/general'.$filename;
        return File::delete($path);
    }
}
 