<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use JWTAuth;
use JWTException;
use Auth;
use Session;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function username()
    {
        return 'username';
    }
 
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout','authenticate');
    }

    public function index(){
        return view('getToken');
    }

    public function getToken(Request $request)
    {
        // mengambli credential dari client
        $credentials = $request->only('email', 'password');
        try {
            // memvalidasi crendential yang dikirim client
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // ada error
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        // mengirim token ke client
        return response()->json(compact('token'));
    }
    public function logout()
    {
        $id = Auth::id();
        $user = User::find($id);
        $user->update([
            'is_login' => 0,
            'logout_date' => date('Y-m-d H:i:s')
        ]);
        Auth::logout();
        Session::flush();
        return redirect('/');
    }
}
