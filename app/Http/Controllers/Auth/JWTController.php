<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use JWTAuth;
use JWTException;
use Alert;
use Cookie;

class JWTController extends Controller
{
    public function index(){
        return view('getToken');
    }

    public function getToken(Request $request)
    {
        // mengambli credential dari client
        $credentials = $request->only('email', 'password');
        try {
            // memvalidasi crendential yang dikirim client
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // ada error
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        // mengirim token ke client

       	Cookie::queue('jwt_token', $token, 15);
        alert()->success('Token disimpan didalam browser anda..', 'Anda telah men-generate token')->persistent('Close');
        return view('home');
    }
}