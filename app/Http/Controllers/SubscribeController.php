<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\SpreadNews as NewsSpreadingNotification;
use App\Subscribe;
use App\News;
use Carbon\Carbon;
use Session;
use Validator;
use Alert;
use DB;

class SubscribeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('isAdmin');
    }

    public function index()
    {
        try{
            $month = Carbon::now()->subMonth();
            $count = Subscribe::count();
            $users = Subscribe::paginate(5);
            $news = News::where('created_at', '>=', $month)->where('status','published')->orderBy('created_at', 'desc')->paginate(5);
            return view('subscribe.index', compact('users','count','news'));
        }
        // catch (\Illuminate\Database\QueryException $e) {
        //     return \Response::view('errors.query',array(),500);
        // }
        catch (\Exception $e) {
            return \Response::view('errors.500',array(),500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'email' => 'required|email|max:255|unique:subscribes', 
        ];

        $messages = [
            'required' => 'Field harus di isi alias tidak boleh kosong',
            'email' => 'Email kamu tidak valid',
            'unique' => 'Email sudah melakukan subscribe'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            Alert::error('Subscribe Failed', 'Oops, subscribe gagal. Silahkan coba lagi!')->persistent('Close');
            return redirect('/')->withErrors($validator)->withInput();
        }

        $subscribe = Subscribe::create(['email'=>$request->email]);
        if($subscribe){
            Alert::success('Subscribe Success', 'Terima kasih sudah subscribe portal berita kita!')->persistent('Close');
            return redirect('/');
        }
        else{
            Alert::error('Subscribe Failed', 'Oops, subscribe gagal. Silahkan coba lagi!')->persistent('Close');
            return redirect('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = subscribe::find($id);
            $subscriber = Subscribe::find($id)->delete();
            if($subscriber){
                Session::flash('flash_notification', ["level"=>"success", "message"=>'Berhasil menghapus subscriber '.$user->email]);
                return redirect()->route('subscribe.index');
            }
            else{
                Session::flash('flash_notification', ["level"=>"danger", "message"=>'Oops, gagal menghapus subscriber '.$user->email]);
                return redirect()->route('subscribe.index');
            }
        }
        catch (\Exception $e) {
            return \Response::view('errors.500',array(),500);
        }
        
    }

    public function spreadNews($slug_news, Request $request){
        $subscriber_count = Subscribe::count();
        $spreads = $request->spreads;
        $news = News::where('slug', $slug_news)->first();
        $rubric = $news->rubric->slug;
        foreach ($spreads as $spread) {
            $subscriber = Subscribe::where('id', $spread)->first();
            if(DB::table('news_subscribe')->where('subscribe_id',$subscriber->id)->where('news_id',$news->id)->count()==0)
                DB::table('news_subscribe')->insert(['subscribe_id' => $subscriber->id, 'news_id' => $news->id]);
            $datas = [
                'email' => $subscriber->email,
                'news' => $news->title,
                'rubric' => $rubric,
                'news_slug' => $news->slug,
            ];
            $subscriber->notify(new NewsSpreadingNotification($datas));
        }
        $subscribe_count = DB::table('news_subscribe')->where('news_id', $news->id)->count();
        if($subscriber_count >= $subscribe_count){
            $news->update([
                'is_spreaded' => 1
            ]);
        } else{
            $news->update([
                'is_spreaded' => 0
            ]);
        }

        Alert::success('Subscribe Success', 'Berhasil menyebarkan berita ke '.count($spreads).' email subscriber')->persistent('Close');
        return redirect()->route('subscribe.index');

        // echo $rubric.'/'.$news->slug;

    }
}
