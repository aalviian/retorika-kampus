<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Rubric;

use App\Type;

use App\Highlight;

use App\News;

use Session;

use DB;



class TrashController extends Controller

{

    public function index(){

        try{

            $rubrics = Rubric::withTrashed()->whereNotNull('deleted_at')->orderBy('deleted_at', 'desc')->paginate(5);

            $types = Type::withTrashed()->whereNotNull('deleted_at')->orderBy('deleted_at', 'desc')->paginate(5);

            $highlights = Highlight::withTrashed()->whereNotNull('deleted_at')->orderBy('deleted_at', 'desc')->paginate(5);

            $news = News::withTrashed()->whereNotNull('deleted_at')->orderBy('deleted_at', 'desc')->paginate(5);

            return view('trash', compact('rubrics', 'types','highlights','news'));

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return \Response::view('errors.query',array(),500);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }

    }



    public function destroyRubric($id)

    {

        $data = Rubric::withTrashed()->find($id);



        $news = DB::table('news')->where('rubric_id', $data->id)->get();


        foreach ($news as $f_news) {

            DB::table('news_type')->where('news_id', $f_news->id)->delete();

            DB::table('highlight_news')->where('news_id', $f_news->id)->delete();

            News::withTrashed()->find($f_news->id)->forceDelete();

        }



        $rubric = Rubric::withTrashed()->find($id);

        $rubric->forceDelete();

        if($rubric){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menghapus rubrik '".$data->name."' dari database"]);
            return redirect()->route('rubric.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal menghapus rubrik '".$data->name."' dari database"]);
            return redirect()->route('rubric.index');   
        }





    } 



    public function restoreRubric($id)

    {

        $data = Rubric::withTrashed()->find($id);



        $child = Rubric::where('order',Rubric::max('order'))->first();

        $child->update([

            'parent_id' => $id,

            'order' => NULL

        ]);



        $rubric = Rubric::withTrashed()->find($id);



        $news = DB::table('news')->where('rubric_id', $data->id)->get();

        

        foreach ($news as $f_news) {

            News::withTrashed()->find($f_news->id)->restore();

        } 



        $rubric->restore();

        if($rubric){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil memulihkan rubrik '".$data->name."' kedalam database"]);
            return redirect()->route('rubric.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Opps, gagal memulihkan rubrik '".$data->name."' kedalam database"]);
            return redirect()->route('rubric.index');
        }



    }



    public function destroyType($id)

    {

        $data = Type::withTrashed()->find($id);

        $type = Type::withTrashed()->find($id);

        $type->forceDelete();

        if($type){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil meenghapus Type '".$data->name."' dari database"]);
            return redirect()->route('type.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal meenghapus Type '".$data->name."' dari database"]);
            return redirect()->route('type.index');   
        }



    }



    public function restoreType($id)

    {

        $data = Type::withTrashed()->find($id);

        $type = Type::withTrashed()->find($id);

        $type->restore();

        if($type){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil memulihkan Type '".$data->name."' kedalam database"]);
            return redirect()->route('type.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal memulihkan Type '".$data->name."' kedalam database"]);
            return redirect()->route('type.index');   
        }



    }



    public function destroyHighlight($id)

    {

        $data = Highlight::withTrashed()->find($id);

        $highlight = Highlight::withTrashed()->find($id);

        $highlight->forceDelete();

        if($highlight){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil meenghapus Highlight '".$data->title."' dari database"]);
            return redirect()->route('highlight.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal meenghapus Highlight '".$data->title."' dari database"]);
            return redirect()->route('highlight.index');
        }
    }



    public function restoreHighlight($id)

    {

        $data = Highlight::withTrashed()->find($id);

        $highlight = Highlight::withTrashed()->find($id);

        $highlight->restore();

        if($highlight){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil memulihkan Highlight '".$data->title."' kedalam database"]);
            return redirect()->route('highlight.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal memulihkan Highlight '".$data->title."' kedalam database"]);
            return redirect()->route('highlight.index');   
        }
    }



    public function destroyNews($id)

    {

        $data = News::withTrashed()->find($id);

        $type = News::withTrashed()->find($id);

        $type->forceDelete();

        if($type){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil meenghapus news '".$data->title."' dari database"]);
            return redirect()->route('news.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal meenghapus news '".$data->title."' dari database"]);
            return redirect()->route('news.index');   
        }

    }


    public function restoreNews($id)

    {

        $data = News::withTrashed()->find($id);

        $type = News::withTrashed()->find($id);

        $type->restore();

        if($type){
            Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil memulihkan news '".$data->title."' kedalam database"]);
            return redirect()->route('news.index');
        }
        else{
            Session::flash('flash_notification', ["level"=>"danger", "message"=>"Oops, gagal memulihkan news '".$data->title."' kedalam database"]);
            return redirect()->route('news.index');   
        }

    }

}

