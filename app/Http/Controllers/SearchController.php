<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\News;
use App\Rubric;
use App\Highlight;
use App\Type;
use App\AccessLog;
use App\Advertisement;
use App\Configuration;
use App\Comment;


class SearchController extends Controller
{
    public function searchResult(Request $request){
            $rubrics = Rubric::noParent()->orderBy('order')->get();
            $highlights = Highlight::whereNotNull('order')->orderBy('order')->paginate(10);
            $recent_news = News::orderBy('created_at','desc')->take(10)->where('status', 'published')->get();
            $popular_news = News::orderBy('views', 'desc')->take(10)->where('status', 'published')->get();
            $headlines = Type::find(2)->news()->where('status', 'published')->orderBy('created_at', 'desc')->get();
            $config = Configuration::find(1);
            $ads1 = Advertisement::where('type','depan')->where('order',1)->first();
            $ads2 = Advertisement::where('type','depan')->where('order',2)->first();
            $ads3 = Advertisement::where('type','depan')->where('order',3)->first();
            $ads4 = Advertisement::where('type','depan')->where('order',4)->first();
            $ads5 = Advertisement::where('type','depan')->where('order',5)->first();
            $query = $request->get('query');
            if($query) {
                $total_news = News::where('title','LIKE','%'.$query.'%')->orWhere('content','LIKE','%'.$query.'%')->orWhere('created_by','LIKE','%'.$query.'%')->orWhere('edited_by','LIKE','%'.$query.'%')->orWhere('approved_by','LIKE','%'.$query.'%');
                $news = News::where('title','LIKE','%'.$query.'%')->orWhere('content','LIKE','%'.$query.'%')->orWhere('created_by','LIKE','%'.$query.'%')->orWhere('edited_by','LIKE','%'.$query.'%')->orWhere('approved_by','LIKE','%'.$query.'%')->paginate(10);
                $count = $total_news->count(); 
            }
            else {
                $news = News::all();
                $count = $news->count();
            }
            return view('frontend.searchResult', compact('rubrics','highlights','recent_news','popular_news','headlines','config','ads1','ads2','ads3','ads4','ads5','news', 'count', 'query'));
    } 

    public function searchComments(Request $request){
        try{

                $query = $request->get('query');

                $count = Comment::count();

                $comments = Comment::where('name','LIKE','%'.$query.'%')->orWhere('message','LIKE','%'.$query.'%')->orderBy('created_at', 'desc')->paginate(10);

                return view('comment.search', compact('comments','count','query'));

            }

            // catch (\Illuminate\Database\QueryException $e) {

            //     return \Response::view('errors.query',array(),500);

            // }

            catch (\Exception $e) {

                return \Response::view('errors.500',array(),500);

            }
    }

    public function searchNews(Request $request){
        try{

            $query = $request->get('query');
            if($query) {
                $news = News::where('title','LIKE','%'.$query.'%')->orWhere('content','LIKE','%'.$query.'%')->orWhere('created_by','LIKE','%'.$query.'%')->orWhere('edited_by','LIKE','%'.$query.'%')->orWhere('approved_by','LIKE','%'.$query.'%')->orderBy('created_at', 'desc')->paginate(10);
                    $count = $news->count(); 
            }
            else {
                $news = News::all();
            }

            return view("news.search", compact('news', 'count', 'query'));

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return \Response::view('errors.query',array(),500);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }
    }
}
