<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Http\Requests;

use App\News;

use App\User;

use App\AccessLog;

use App\Rubric;

use Session;

use Auth;

use Validator;

use File;

use Carbon\Carbon;



class StatisticController extends Controller 

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request)

    {

        try{

            $users = User::all();

            $news = News::count();

            $news_user = News::where('created_by', Auth::user()->id)->count();

            $rubrics = Rubric::all();



            File::put(public_path('data_rubric.json'),$rubrics);

            $visitors = AccessLog::whereDate('created_at', Carbon::now()->format('Y-m-d'))->distinct()->count(["ip"]);

            // foreach ($users as $user) {

            //     if($user->level_id != 1){

            //         $statistic[$user->username]=News::where('created_by', $user->username)->count();

            //     }

            // }



            $usersNews=[];

            foreach ($users as $user){

                $count = count(News::where('created_by', $user->id)->get());

                $usersNews[]=['level_id'=>$user->level_id, 'name'=>$user->name, 'username'=>$user->username, 'count'=>$count];

            }

            $cek=collect($usersNews)->sortByDesc('count')->values()->all();



            //chart

            if(!$request->has('logVisitType'))

                $logVisitType='daily';

            else {

                $logVisitType = $request->logVisitType;

            }

            $AccessLogs = [];

            

            if($logVisitType == 'hourly'){

                for($i=0; $i<24; $i++){

                    $today = Carbon::now();

                    $start_hour = Carbon::create($today->year, $today->month, $today->day, $i, 0, 0);

                    if($i<23){

                        $end_hour = Carbon::create($today->year, $today->month, $today->day, $i+1, 0, 0);

                    }

                    else {

                        $end_hour = Carbon::create($today->year, $today->month, $today->day+1, 0, 0, 0);

                    }

                    $log = AccessLog::where('created_at', '>=', $start_hour->toDateTimeString())

                                    ->where('created_at', '<', $end_hour->toDateTimeString())

                                    ->select('ip')->distinct()->count(["ip"]);

                    $AccessLogs[]= ['y'=>$i, 'a'=>$log];

                }

            }

            else {

                for($i=6;$i>=0;$i--){

                    if($logVisitType=='daily'){

                        $now = Carbon::now()->subDays($i)->format('Y-m-d');

                        $log = AccessLog::whereDate('created_at', $now)->select('ip')->distinct()->count(["ip"]);

                    } else if($logVisitType=='weekly'){

                        $now = Carbon::now()->subWeeks($i)->format('Y-m-d');

                        $lastWeek = Carbon::now()->subWeeks($i+1)->format('Y-m-d');

                        $log = AccessLog::whereDate('created_at', '>', $lastWeek)

                                        ->whereDate('created_at', '<=', $now)

                                        ->select('ip')->distinct()->count(["ip"]);

                    } else {

                        $now = Carbon::now()->subMonths($i)->format('Y-m-d');

                        $lastWeek = Carbon::now()->subMonths($i+1)->format('Y-m-d');

                        $log = AccessLog::whereDate('created_at', '>', $lastWeek)

                                        ->whereDate('created_at', '<=', $now)

                                        ->select('ip')->distinct()->count(["ip"]);

                    }

                    $AccessLogs[]= ['y'=>$now, 'a'=>$log];

                }

            } 



            if(!$request->has('logRubricsType'))

                $logRubricsType='today';

            else {

                $logRubricsType = $request->logRubricsType;

            }

            $rubricsLogs = [];

            $rubrics2 = Rubric::all();

            foreach ($rubrics2 as $f_rubrics2) {

                if($logRubricsType=='today'){

                    $now = Carbon::now()->format('Y-m-d');

                    $log = AccessLog::whereDate('created_at', $now)

                                ->where('path', 'like', '%'.$f_rubrics2->slug.'%')

                                ->distinct()

                                ->count(['ip']);

                } else if($logRubricsType=='thisweek'){

                    $now = Carbon::now()->format('Y-m-d');

                    $lastWeek = Carbon::now()->subWeeks(1)->format('Y-m-d');

                    $log = AccessLog::whereDate('created_at', '<=', $now)

                                ->whereDate('created_at', '>', $lastWeek)

                                ->where('path', 'like', '%'.$f_rubrics2->slug.'%')

                                ->distinct()

                                ->count(['ip']);

                } else {

                    $now = Carbon::now()->format('Y-m-d');

                    $lastMonth = Carbon::now()->subMonths(1)->format('Y-m-d');

                    $log = AccessLog::whereDate('created_at', '<=', $now)

                                ->whereDate('created_at', '>', $lastMonth)

                                ->where('path', 'like', '%'.$f_rubrics2->slug.'%')

                                ->distinct()

                                ->count(['ip']);

                }

                $rubricsLogs[] = ['y'=>$f_rubrics2->name, 'a'=>$log];

            }

            $user_news = News::where('created_by', Auth::user()->id)->orderBy('created_at', 'desc')->take(20)->get();
            $userNewsLogs = [];
            foreach ($user_news as $f_user_news) {
                $userNewsLogs[] = ['y'=>$f_user_news->title, 'a'=>$f_user_news->views];
            }

            return view("home", compact('users', 'news','news_user', 'rubrics', 'statistic', 'visitors', 'cek', 'AccessLogs', 'rubricsLogs', 'logVisitType', 'logRubricsType', 'userNewsLogs'));

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return \Response::view('errors.query',array(),500);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }

    }  



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        //

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        //

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        //

    }

}

