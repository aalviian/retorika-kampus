<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\AccessLog;

use Excel;



class AccessLogController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        try {

            $count = AccessLog::count();

            $AccessLog = AccessLog::orderBy('created_at', 'desc')->paginate(10);

            return view('accesslog.index', compact('AccessLog', 'count'));

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return \Response::view('errors.query',array(),500);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        //

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        //

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        //

    }





    public function export(Request $request){

        try {

            $start_date = date('Y-m-d', strtotime($request->start_date));

            $end_date = date('Y-m-d', strtotime($request->end_date));

            $AccessLog = AccessLog::whereDate('created_at', '>=', $start_date)

                                  ->whereDate('created_at', '<', $end_date)

                                  ->get();

            if($request->file == 'xls'){

                Excel::create('AccessLog'.$start_date.'-'.$end_date, function($excel) use($AccessLog) {

                    $excel->sheet('Excel sheet', function($sheet) use($AccessLog){

                        $sheet->fromArray($AccessLog);

                    });

                })->export('xls');

            }

            else {

                Excel::create('AccessLog'.$start_date.'-'.$end_date, function($excel) use($AccessLog) {

                    $excel->sheet('Excel sheet', function($sheet) use($AccessLog){

                        $sheet->fromArray($AccessLog);

                    });

                })->export('csv');

            }

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return \Response::view('errors.query',array(),500);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }

    }

    public function delete(Request $request){

        try{

            $start_date = date('Y-m-d', strtotime($request->start_date));

            $end_date = date('Y-m-d', strtotime($request->end_date));

            $AccessLog = AccessLog::whereDate('created_at', '>=', $start_date)

                                  ->whereDate('created_at', '<', $end_date)

                                  ->delete();

            return redirect()->route('accesslog.index');

        }

        // catch (\Illuminate\Database\QueryException $e) {

        //     return \Response::view('errors.query',array(),500);

        // }

        catch (\Exception $e) {

            return \Response::view('errors.500',array(),500);

        }

    }

}

