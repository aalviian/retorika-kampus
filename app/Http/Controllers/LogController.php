<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class LogController extends Controller
{
    public function index(){
    	$onlines = User::where('is_login', 1)->orderBy('login_date', 'desc')->get();
    	$logs = User::where('is_login',1)->orWhere('is_login',0)->orderBy('login_date', 'desc')->get();
    	return view('online.index', compact('onlines','logs'));
    }
}
