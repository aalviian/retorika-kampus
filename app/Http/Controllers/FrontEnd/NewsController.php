<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rubric;
use App\Highlight;
use App\Type;
use App\News;
use App\Advertisement;
use App\User;
use App\AccessLog;
use App\Configuration;
use Carbon\Carbon;
use DB;


class NewsController extends Controller
{


    public function rubric_news($rubric_slug, $news_slug, Request $request)
    {
        $last7Days = Carbon::now()->subDays(7);
        $rubrics = Rubric::noParent()->orderBy('order')->get();
        $highlights = Highlight::whereNotNull('order')->orderBy('order')->get();
        $rubric = Rubric::where('slug', $rubric_slug)->first();
        $headlines = News::all();
        $rubric_news = Rubric::find($rubric->id)->news()->orderBy('created_at', 'desc')->paginate(3);
        $news = News::where('rubric_id', $rubric->id)->where('slug', $news_slug)->first();
        $Acclog_check = AccessLog::where('path', 'like', '%'.$news_slug.'%')->where('ip', $request->getClientIp())->count();
        if($Acclog_check==0){
            DB::table('news')->where('rubric_id', $rubric->id)->where('slug', $news_slug)->update(['views'=>$news->views + 1]);
        }
        $news = News::where('rubric_id', $rubric->id)->where('slug', $news_slug)->first();
        $user = User::where('id', $news->created_by)->first();
        $recent_news = News::orderBy('created_at','desc')->take(5)->where('status', 'published')->get();
        $popular_news = News::query()->where('created_at', '>=', $last7Days)->orderBy('views', 'desc')->where('status', 'published')->take(5)->get();
        $ads1 = Advertisement::where('type','berita')->where('order',1)->first();
        $ads2 = Advertisement::where('type','berita')->where('order',2)->first();
        $ads3 = Advertisement::where('type','berita')->where('order',3)->first();
        $ads4 = Advertisement::where('type','berita')->where('order',4)->first();
        $ads5 = Advertisement::where('type','berita')->where('order',5)->first();
        $config = Configuration::find(1);
        $comments = $news->comment()->orderBy('id','asc')->paginate(6);
        return view('frontend.beritaRubrik', compact('rubric', 'highlights', 'recent_news', 'popular_news', 'rubrics','headlines', 'news', 'rubric_slug', 'rubric_news', 'ads1', 'ads2', 'ads3', 'ads4', 'ads5','comments','user','config'));
    }

    public function highlight_news($highlight_slug, $news_slug, Request $request)
    {
        $last7Days = Carbon::now()->subDays(7);
        $rubrics = Rubric::noParent()->orderBy('order')->get();
        $highlights = Highlight::whereNotNull('order')->orderBy('order')->get();
        $highlight = Highlight::where('slug', $highlight_slug)->first();
        $headlines = News::all();
        $highlight_news = Highlight::find($highlight->id)->news()->orderBy('created_at', 'desc')->paginate(3);
        $news = Highlight::find($highlight->id)->news()->where('slug', $news_slug)->first();
        $Acclog_check = AccessLog::where('path', 'like', '%'.$news_slug.'%')->where('ip', $request->getClientIp())->count();
        if($Acclog_check==0){
            DB::table('news')->where('rubric_id', $news->rubric_id)->where('slug', $news_slug)->update(['views'=>$news->views + 1]);
        }
        $news = Highlight::find($highlight->id)->news()->where('slug', $news_slug)->first();
        $user = User::where('id', $news->created_by)->first();
        $recent_news = News::orderBy('created_at','desc')->take(5)->where('status', 'published')->get();
        $popular_news = News::query()->where('created_at', '>=', $last7Days)->orderBy('views', 'desc')->where('status', 'published')->take(5)->get();
        $ads1 = Advertisement::where('type','fokus')->where('order',1)->first();
        $ads2 = Advertisement::where('type','fokus')->where('order',2)->first();
        $ads3 = Advertisement::where('type','fokus')->where('order',3)->first();
        $ads4 = Advertisement::where('type','fokus')->where('order',4)->first();
        $ads5 = Advertisement::where('type','fokus')->where('order',5)->first();
        $config = Configuration::find(1);
        $comments = $news->comment()->orderBy('id','asc')->paginate(6);
        return view('frontend.beritaFokus', compact('highlight', 'highlights', 'recent_news', 'popular_news', 'rubrics', 'headlines', 'news', 'highlight_slug', 'highlight_news', 'ads1', 'ads2', 'ads3','ads4', 'ads5', 'user', 'comments', 'config'));
    }
}
