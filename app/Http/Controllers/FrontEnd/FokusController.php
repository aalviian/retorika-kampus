<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rubric;
use App\Highlight;
use App\Advertisement;
use App\Type;
use App\News;
use App\Configuration;
use Carbon\Carbon;
use DB;

class FokusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($highlight_slug)
    {
        //
        $last7Days = Carbon::now()->subDays(7);
        $rubrics = Rubric::noParent()->orderBy('order')->get();
        $highlights = Highlight::whereNotNull('order')->orderBy('order')->get();
        $highlight = Highlight::where('slug', $highlight_slug)->first();
        $recent_news = News::orderBy('created_at','desc')->where('status', 'published')->take(5)->get();
        $popular_news = News::query()->where('created_at', '>=', $last7Days)->orderBy('views', 'desc')->where('status', 'published')->take(5)->get();
        $headlines = News::join('news_type', 'news.id', '=', 'news_type.news_id')
                        ->join('highlight_news', 'news.id', '=', 'highlight_news.news_id')
                        ->select('news.*', 'news_type.news_id', 'news_type.type_id', 'highlight_news.news_id', 'highlight_news.highlight_id')
                        ->where('highlight_news.highlight_id', $highlight->id)
                        ->where('news_type.type_id', 2)
                        ->orderBy('created_at', 'desc')
                        ->get();
        $news = Highlight::find($highlight->id)->news()->where('status', 'published')->orderBy('created_at', 'desc')->paginate(6);
        $ads1 = Advertisement::where('type','fokus')->where('order',1)->first();
        $ads2 = Advertisement::where('type','fokus')->where('order',2)->first();
        $ads3 = Advertisement::where('type','fokus')->where('order',3)->first();
        $ads4 = Advertisement::where('type','fokus')->where('order',4)->first();
        $ads5 = Advertisement::where('type','fokus')->where('order',5)->first();
        $config = Configuration::find(1);
        return view('frontend.fokus', compact('rubrics','highlights','recent_news','popular_news' ,'news', 'headlines', 'highlight_slug', 'config', 'ads1', 'ads2', 'ads3', 'ads4', 'ads5'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
