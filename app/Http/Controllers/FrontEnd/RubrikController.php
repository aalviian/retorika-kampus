<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Rubric;
use App\Highlight;
use App\Type;
use App\News;
use App\Advertisement;
use Carbon\Carbon;
use App\User; 
use App\Configuration;

class RubrikController extends \App\Http\Controllers\Controller
{
    public function index($slug){
        $last7Days = Carbon::now()->subDays(7);
    	$rubrics = Rubric::noParent()->orderBy('order')->get();
    	$highlights = Highlight::whereNotNull('order')->orderBy('order')->paginate(10);
    	$rubric = Rubric::where('slug', $slug)->first();
        $recent_news = News::orderBy('created_at','desc')->where('status', 'published')->take(5)->get();
        $popular_news = News::query()->where('created_at', '>=', $last7Days)->orderBy('views', 'desc')->where('status', 'published')->take(5)->get();
        $headlines = Type::find(2)->news()->where('rubric_id', $rubric->id)->where('status', 'published')->orderBy('created_at', 'desc')->get();
        $news = Rubric::find($rubric->id)->news()->where('status', 'published')->orderBy('created_at', 'desc')->paginate(5);
        $ads1 = Advertisement::where('type','rubrik')->where('order',1)->first();
        $ads2 = Advertisement::where('type','rubrik')->where('order',2)->first();
        $ads3 = Advertisement::where('type','rubrik')->where('order',3)->first();
        $ads4 = Advertisement::where('type','rubrik')->where('order',4)->first();
        $ads5 = Advertisement::where('type','rubrik')->where('order',5)->first();
    	$rubric_slug = $slug;
        $config = Configuration::find(1);
    	return view('frontend.rubrik', compact('rubrics','highlights','recent_news','popular_news','news', 'headlines', 'rubric_slug', 'ads1', 'ads2', 'ads3', 'ads4', 'ads5', 'config'));
    }
}
