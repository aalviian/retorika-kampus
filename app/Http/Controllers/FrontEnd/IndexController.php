<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Rubric;
use App\Highlight;
use App\Type;
use App\News;
use App\Advertisement;
use App\Configuration;
use Carbon\Carbon;
use Cache;
use View;


class IndexController extends \App\Http\Controllers\Controller
{
    public function index(){
        // return Cache::rememberForever('views', function(){
        //  $rubrics = Rubric::noParent()->orderBy('order')->get();
        //  $highlights = Highlight::all();
        //  $news = News::all();
        //  $recent_news = News::orderBy('created_at','desc')->paginate(6);
        //  return view('frontend.index2', compact('rubrics','highlights','news','recent_news'))->render();
        // });
        $now = Carbon::now()->subDays(7);
        $rubrics = Rubric::noParent()->orderBy('order')->get();
        $highlights = Highlight::whereNotNull('order')->orderBy('order')->paginate(10);
        $news = Type::find(2)->news()->orderBy('created_at','desc')->where('status', 'published')->get();
        $recent_news = News::orderBy('created_at','desc')->take(10)->where('status', 'published')->get();
        $popular_news = News::where('created_at', '>=', $now)->where('status', 'published')->orderBy('views', 'desc')->take(10)->get();
        $headlines = Type::find(2)->news()->where('status', 'published')->orderBy('created_at', 'desc')->get();
        $ads1 = Advertisement::where('type','depan')->where('order',1)->first();
        $ads2 = Advertisement::where('type','depan')->where('order',2)->first();
        $ads3 = Advertisement::where('type','depan')->where('order',3)->first();
        $ads4 = Advertisement::where('type','depan')->where('order',4)->first();
        $ads5 = Advertisement::where('type','depan')->where('order',5)->first();
        $config = Configuration::find(1);
        return view('frontend.index', compact('rubrics','highlights','news','recent_news','headlines','popular_news','ads1','ads2','ads3','ads4','ads5','config'));
    }
} 
