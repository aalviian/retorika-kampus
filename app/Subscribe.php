<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\News;
use DB;

class Subscribe extends Model
{
	use Notifiable;

    protected $fillable = [
        'email'
    ];

    public function news(){
        return $this->belongsToMany('App\News');
    }

    public static function getSubscriber($id){
        $subscribes = DB::table('subscribes')->get();
        $subscriber = [];
        foreach ($subscribes as $subscribe) {
        	$news_subscribe = DB::table('news_subscribe')->where('news_id', $id)->where('subscribe_id', $subscribe->id)->count();
            if($news_subscribe == 0)
    		  $subscriber += [$subscribe->id => $subscribe->email];
    	}
    	return $subscriber;
    }
}
