<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'news_id',
    	'name',
    	'email',
    	'message'
    ];

    public function news(){
    	return $this->belongsTo('App\News');
    }

    public function getPhotoPathAttribute(){
    	return url('/img/data/comment/user.png');
    } 
}
