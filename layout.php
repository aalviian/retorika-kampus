<?php global $app;?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="msapplication-TileImage" content="assets/img/favicon/mstile-144x144.png">
    <meta name="msapplication-config" content="assets/img/favicon/browserconfig.xml">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=yes" >
    <meta name="theme-color" content="#ffffff">

    <link rel="icon" type="image/png" href="<?php echo href('/assets/img/favicon_jojo/favicon-32x32.png');?>" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo href('/assets/img/favicon_jojo/favicon-96x96.png');?>" sizes="96x96">
    <link rel="icon" type="image/png" href="<?php echo href('/assets/img/favicon_jojo/favicon-16x16.png');?>" sizes="16x16">
    <link rel="shortcut icon" href="<?php echo href('/assets/img/favicon_jojo/favicon-16x16.png');?>">

    <!-- <script src="<?php /*echo href('/assets/js/jquery.min.js'); */?>" type="text/javascript"></script>
    <script src="<?php /*echo href('/assets/js/jquery.timeago.js');*/?>" type="text/javascript"></script>-->

    <title>Jojonomic - Pro</title>


    <link href="<?php echo href('/assets/css/zebra_pagination.css'); ?>" rel="stylesheet">
    <link href="<?php echo href('/assets/css/vendors.min.css');?>" rel="stylesheet" />
    <link href="<?php echo href('/assets/css/styles.min.css');?>" rel="stylesheet" />
    <link href="<?php echo href('/assets/css/style.css');?>" rel="stylesheet" />
    <link href="<?php echo href('/assets/css/style_extension.css');?>" rel="stylesheet" />
    <link href="<?php echo href('/assets/toastr/build/toastr.css');?>" rel="stylesheet" />
    <!-- Load c3.css -->
    <link  href="<?php echo href('/assets/c3/c3.css'); ?>" rel="stylesheet" type="text/css">

    <link  href="<?php echo href('/assets/loumulti/css/multi-select.css'); ?>" rel="stylesheet" type="text/css">

    <!-- import Dhtmlx -->
    <link  href="<?php echo href('/assets/dhtmlx/css/dhtmlx.css'); ?>" rel="stylesheet" type="text/css">
    <link  href="<?php echo href('/assets/dhtmlx/css/dhtmlxcombo.css'); ?>" rel="stylesheet" type="text/css">

    <!--import nestable -->
    <link  href="<?php echo href('/assets/nestable/css/style.nestable.css'); ?>" rel="stylesheet" type="text/css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>  <![endif]-->

    <script src="<?php echo href('/');?>assets/js/jquery.min.js"></script>
    <script src="<?php echo href('/');?>assets/js/jquery.ddslick.min.js"></script>


    <!-- Load d3.js and c3.js -->
    <script src="<?php echo href('/');?>assets/c3/d3.v3.min.js" charset="utf-8"></script>
    <script src="<?php echo href('/');?>assets/c3/c3.min.js"></script>

    <!-- Load Iou Multi Select -->
    <script src="<?php echo href('/');?>assets/loumulti/js/jquery.multi-select.js"></script>

    <script src="<?php echo href('/');?>assets/toastr/toastr.js"></script>

    <script src="<?php echo href('/');?>assets/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo href('/assets/sweetalert/dist/sweetalert.css'); ?>" >

    <script src="<?php echo href('/');?>assets/select/jquery.multiple.select.js"></script>
    <link  href="<?php echo href('/assets/select/multiple-select.css'); ?>" rel="stylesheet" type="text/css">

    <script src="<?php echo href('/');?>assets/jqueryUI/jquery-ui.js"></script>
    <link  href="<?php echo href('/assets/jqueryUI/jquery-ui.css'); ?>" rel="stylesheet" type="text/css">

    <script src="<?php echo href('/');?>assets/js/moment.js"></script>

    <script src="<?php echo href('/');?>assets/js/loading.js"></script>
    <link  href="<?php echo href('/assets/css/loading.css'); ?>" rel="stylesheet" type="text/css">

    <script src="<?php echo href('/');?>assets/pikaday/pikaday.js"></script>
    <link  href="<?php echo href('/assets/pikaday/css/pikaday.css'); ?>" rel="stylesheet" type="text/css">

    <link  href="<?php echo href('/assets/css/file_manager.css'); ?>" rel="stylesheet" type="text/css">

    <link  href="<?php echo href('/assets/MagnificPopup/dist/magnific-popup.css'); ?>" rel="stylesheet" type="text/css">
    <script src="<?php echo href('/');?>assets/MagnificPopup/dist/jquery.magnific-popup.min.js"></script>

    <script src="<?php echo href('/');?>assets/js/FileSaver.js"></script>
    <script src="<?php echo href('/');?>assets/js/jspdf.min.js"></script>
    <script src="<?php echo href('/');?>assets/js/jspdf.plugin.autotable.js"></script>
    <script src="<?php echo href('/');?>assets/js/xepOnline.jqPlugin.js"></script>

    <link href="<?php echo href('/assets/css/inspinia.css');?>" rel="stylesheet" />

    <script src="<?php echo href('/');?>assets/js/numeral.js"></script>

    <script src="<?php echo href('/');?>assets/elevatezoom/jquery.elevatezoom.js"></script>

    <link  href="<?php echo href('/assets/tokenize/jquery.tokenize.css'); ?>" rel="stylesheet" type="text/css">
    <script src="<?php echo href('/assets/tokenize/jquery.tokenize.js'); ?>"></script>

    <!--<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>-->

    <!--Datatables-->
    <link  href="<?php echo href('/assets/datatables/media/css/jquery.dataTables.css'); ?>" rel="stylesheet" type="text/css">
    <script src="<?php echo href('/');?>assets/datatables/media/js/jquery.dataTables.js"></script>
    <link  href="<?php echo href('/assets/datatables/media/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
    <link  href="<?php echo href('/assets/datatables/media/css/jquery.dataTables.min.css'); ?>" rel="stylesheet" type="text/css">
    <script src="<?php echo href('/assets/datatables/media/js/dataTables.bootstrap.min.js'); ?>"></script>

    <link  href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

    <!--<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>-->
    <link  href="<?php echo href('/assets/nprogress/nprogress.css'); ?>" rel="stylesheet" type="text/css">
    <script src="<?php echo href('/assets/nprogress/nprogress.js'); ?>"></script>

    <link  href="<?php echo href('/bootstrap/css/bootstrap-tour.min.css'); ?>" rel="stylesheet" type="text/css">

    <link  href="<?php echo href('/assets/photoswipe/dist/photoswipe.css'); ?>" rel="stylesheet" type="text/css">
    <link  href="<?php echo href('/assets/photoswipe/dist/default-skin/default-skin.css'); ?>" rel="stylesheet" type="text/css">
    <script src="<?php echo href('/assets/photoswipe/dist/photoswipe.min.js'); ?>"></script>
    <script src="<?php echo href('/assets/photoswipe/dist/photoswipe-ui-default.min.js'); ?>"></script>

    <link  href="<?php echo href('/assets/growl/jquery.growl.css'); ?>" rel="stylesheet" type="text/css">
    <script src="<?php echo href('/assets/growl/jquery.growl.js'); ?>"></script>
    <script src="<?php echo href('/assets/spin/spin.min.js'); ?>"></script>

    <link  href="<?php echo href('/assets/css/animation.css'); ?>" rel="stylesheet" type="text/css">

    <!-- import Dhtmlx -->
    <script src="<?php echo href('/assets/dhtmlx/js/dhtmlx.js'); ?>"></script>
    <script src="<?php echo href('/assets/dhtmlx/js/dhtmlxcombo.js'); ?>"></script>

    <!-- import nestable -->
    <!--<script src="<?php echo href('/assets/nestable/js/jquery.nestable.js'); ?>"></script>-->
    <!--<script src="<?php echo href('/assets/nestable/js/jquery-nestable.js'); ?>"></script>-->
    <script src="<?php echo href('/');?>assets/nestable/js/jquery-nestable.js"></script>

    <!--<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jquery-editable/css/jquery-editable.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jquery-editable/js/jquery-editable-poshytip.min.js"></script>-->



    <script>
        var PAGE_NAME = "dashboard";
        var BASE_URL = "<?php echo BASE_URL_VIEW; ?>";

        jQuery(document).ready(function($) {
            $.ajax({
                url: BASE_URL + "/wizard/check-new-ui",
                success: function (result) {
                    if (result.is_show_new_ui) {
                        if (!result.is_staff) {
                            $('#div_cms').removeClass('col-md-6');
                            $('#div_cms').addClass('col-md-6 col-md-offset-3');
                            $('#div_user').hide();
                        }
                        $("#notification-screen").fadeIn();
                    }
                },
                error: function (error) {

                }
            });
        });
    </script>

    <?php if(BASE_IS_ANALYTICS) { ?>
        <!-- Hotjar Tracking Code for https://pro.jojonomic.com -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:165926,hjsv:5};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
        </script>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-64357447-2', 'auto');
            ga('send', 'pageview');

        </script>

    <?php } ?>

    <style>
        /*zendesk*/
        .zendesk-screen {
            position: fixed;
            height: 80px;
            width: 100px;
            bottom: 0;
            right: 0;
            z-index: 1050;
        }
        .zendesk-container {
            /*display: none;*/
            position: absolute;
            bottom: 37px;
            right: -12px;
            transform: translateX(-50%);
            z-index: 1050;
        }
        .zendesk-container img{
            width: 110%;
        }

        /*dhtmlx color*/
        div.dhxcombo_material.dhxcombo_actv{
            border-bottom: 2px solid #e91e63;
        }

    </style>


</head>

<body scroll-spy="" id="top" class="company theme-template-dark theme-pink alert-open alert-with-mat-grow-top-right">
<main>

    <div class="zendesk-screen">
        <div class="zendesk-container">
            <div style="position:relative;top:30px;left:102px;color:white" id="zendesk-notif">0</div>
            <div>
                <img src="<?php echo href('/assets/img/chat/help-support-ask.png'); ?>" border="0" id="zendesk-image">
            </div>
        </div>
    </div>

    <!-- notification screen -->
    <div class="notification-screen" id="notification-screen">
        <div class="floating-container">
            <img class="notification-icon" src="<?php echo href('/assets/img/gear.png');?>">
            <h1>We just update few interface for Jojonomic Pro Dashboard</h1>
            <div class="row">
                <div id="div_cms" class="col-md-6">
                    <img class="preview-icon" src="<?php echo href('/assets/img/mycompanydashboard.png');?>">
                    <h2>Company Dashboard has been moved to upper deck of Dashboard named: <a onclick="skipTutor()" href="#" class="highlight">My Company Dashboard</a></h2>
                </div>
                <div id="div_user" class="col-md-6">
                    <img class="preview-icon" src="<?php echo href('/assets/img/mypersonaldashboard.png');?>">
                    <h2>My Expense has been moved to upper deck of Dashboard named: <a href="<?php echo href('/personal/dashboard'); ?>" class="highlight">My Personal Dashboard</a></h2>
                </div>
            </div>
            <button onclick="skipTutor()" class="btn btn-border skip">Skip</button>
        </div>
    </div>

    <aside class="sidebar fixed" style="width: 260px; left: 0px; ">
        <div class="brand-logo">
            <img src="https://s3-ap-southeast-1.amazonaws.com/jojo-pro/general/images/logo_white.png" class="img-responsive" width="200px" style="margin:auto; padding-top: 23px" />
        </div>
        <div class="user-logged-in">
            <div class="content">
                <div class="profile">
                    <div class="profpic">
                        <img src="<?php echo href('/assets/img/profpic-35.png');?>" width="30px">
                    </div>
                    <div class="profname">
                        <div class="user-email"><?php echo $_SESSION['name']?></div>
                        <!--                        <div class="user-email">--><?php //echo $_SESSION['username']?><!--</div>-->
                        <div class="user-name">
                            <span class="text-muted f9">
                                <?php
                                if (in_array('mobile', $_SESSION['rules'])) {
                                    $rules = array_diff($_SESSION['rules'], ["mobile"]);
                                    echo implode(", ", $rules);
                                } else {
                                    echo implode(", ",$_SESSION['rules']);
                                }
                                ?>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="user-actions">
                    <span id="timezone-show"></span>
                </div>
                <div class="f9">
                    <a class="m-r-5" href="<?php echo href('/profile');?>">profile</a>
                    <a href="<?php echo href('logout');?>">logout</a>
                </div>
            </div>
        </div>
        <ul class="menu-links">
            <!--            <li icon="md md-desktop-mac"> <a --><?php //if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Dashboard") { $_SESSION['header_menu'] = ""; ?><!-- class="active"  --><?php //} }?>
            <!--                    href="--><?php //echo href('/dashboard');?><!--"><i class="md md-desktop-mac"></i>&nbsp;<span>Dashboard</span></a>-->
            <!--            </li>-->

            <!--            --><?php
            //            if (in_array("mobile", $_SESSION['rules'])) {?>
            <!--                <li style="background-color: #E8E8E8" icon="md md-account-box"> <a --><?php //if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Personal") { $_SESSION['header_menu'] = ""; ?><!-- class="active"  --><?php //} }?>
            <!--                        href="--><?php //echo href('/personal/dashboard');?><!--"><i  style="color: #E91E63;" class="md md-account-box"></i>&nbsp;<span style="color: #E91E63;">My Expense</span></a>-->
            <!--                </li>-->
            <!--            --><?php //} ?>

            <!--Reimbursement-->
            <!--  <?php
            if (in_array("approver", $_SESSION['rules']) || in_array("finance", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>

            <li> <a href="#" onclick="change_menu_active('menu_reimburse')" id="menu_reimburse" data-toggle="collapse" data-target="#Reimburse" aria-expanded="false" aria-controls="Reimburse"
                    class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Reimburse') { ?>collapsible-header waves-effect active<?php } else {?>collapsible-header waves-effect collapsed<?php } }?>"><i class="md md-local-atm"></i>&nbsp;Reimbursement</a>

                <ul id="Reimburse" class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Reimburse') { ?> collapse in  <?php } else { ?>collapse<?php } }?>">
                    <?php if (in_array("approver", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>
                        <li> <a <?php if(isset($_SESSION['sub_menu']))
            { if ($_SESSION['sub_menu'] == "reimburse.process" && $_SESSION['menu'] == "Reimburse") { ?> class="active"  <?php } }?>
                                href="<?php echo href('/reimburse/process');?>"><span><i class="icon-circle brown-border"></i>Sent</span></a></li>
                    <?php }?>

                    <?php if (in_array("approver", $_SESSION['rules']) || in_array("finance", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>
                        <li> <a <?php if(isset($_SESSION['sub_menu'])) { if ($_SESSION['sub_menu'] == "reimburse.approved" && $_SESSION['menu'] == "Reimburse") { ?> class="active"  <?php } }?> href="<?php echo href('/reimburse/approved');?>"><span><i class="icon-circle theme-secondary-border"></i>Approved</span></a></li>
                    <?php }?>

                    <?php if (in_array("finance", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>
                        <li> <a <?php if(isset($_SESSION['sub_menu']))
            { if ($_SESSION['sub_menu'] == "reimburse.reimbursed" && $_SESSION['menu'] == "Reimburse")
            { ?> class="active"  <?php } }?> href="<?php echo href('/reimburse/reimbursed');?>">
                                <span><i class="icon-circle green-border"></i>Reimbursed</span>
                            </a></li>
                    <?php }?>
                    <li> <a <?php if(isset($_SESSION['sub_menu'])) { if ($_SESSION['sub_menu'] == "reimburse.rejected" && $_SESSION['menu'] == "Reimburse") { ?> class="active"  <?php } }?> href="<?php echo href('/reimburse/rejected');?>"><span><i class="icon-circle theme-border"></i>Rejected</span></a></li>
                </ul>

            </li>

            <?php } ?>

            <?php
            if (in_array("approver", $_SESSION['rules']) || in_array("finance", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>

                <li> <a href="#" onclick="change_menu_active('menu_pre_approval')" id="menu_pre_approval" data-toggle="collapse" data-target="#Pre-Approval" aria-expanded="false" aria-controls="Pre-Approval"
                        class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Pre-Approval') { ?>collapsible-header waves-effect active<?php } else {?>collapsible-header waves-effect collapsed<?php } }?>"><i class="md md-wallet-travel"></i>&nbsp;Cash Advance</a>

                    <ul id="Pre-Approval" class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Pre-Approval') { ?> collapse in  <?php } else { ?>collapse<?php } }?>">
                        <?php if (in_array("approver", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>
                            <li> <a <?php if(isset($_SESSION['sub_menu']))
            { if ($_SESSION['sub_menu'] == "pre-approval.process" && $_SESSION['menu'] == "Pre-Approval") { ?> class="active"  <?php } }?>
                                    href="<?php echo href('/pre-approval/process');?>"><span><i class="icon-circle brown-border"></i>Sent</span></a></li>
                        <?php }?>

                        <?php if (in_array("approver", $_SESSION['rules']) || in_array("finance", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>
                            <li> <a <?php if(isset($_SESSION['sub_menu'])) { if ($_SESSION['sub_menu'] == "pre-approval.approved" && $_SESSION['menu'] == "Pre-Approval") { ?> class="active"  <?php } }?> href="<?php echo href('/pre-approval/approved');?>"><span><i class="icon-circle theme-secondary-border"></i>Approved</span></a></li>
                        <?php }?>

                        <?php if (in_array("finance", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>
                            <li> <a <?php if(isset($_SESSION['sub_menu']))
            { if ($_SESSION['sub_menu'] == "pre-approval.disbursed" && $_SESSION['menu'] == "Pre-Approval")
            { ?> class="active"  <?php } }?> href="<?php echo href('/pre-approval/disbursed');?>"><span><i class="icon-circle green-border"></i>Disbursed</span></a>
                            </li>
                        <?php }?>

                        <?php if (isset($_SESSION['is_cash_advance_need_verify']) && $_SESSION['is_cash_advance_need_verify']) { ?>

                            <li> <a <?php if(isset($_SESSION['sub_menu'])) { if ($_SESSION['sub_menu'] == "pre-approval.verify" && $_SESSION['menu'] == "Pre-Approval") { ?> class="active"  <?php } }?> href="<?php echo href('/pre-approval/verify');?>"><span><i class="icon-circle yellow-border"></i>Verify</span></a></li>

                            <li> <a <?php if(isset($_SESSION['sub_menu'])) { if ($_SESSION['sub_menu'] == "pre-approval.verified" && $_SESSION['menu'] == "Pre-Approval") { ?> class="active"  <?php } }?> href="<?php echo href('/pre-approval/verified');?>"><span><i class="icon-circle blue-grey-border"></i>Verified</span></a></li>

                        <?php } ?>

                        <li> <a <?php if(isset($_SESSION['sub_menu'])) { if ($_SESSION['sub_menu'] == "pre-approval.reported" && $_SESSION['menu'] == "Pre-Approval") { ?> class="active"  <?php } }?> href="<?php echo href('/pre-approval/reported');?>"><span><i class="icon-circle indigo-border"></i>Reported</span></a></li>

                        <li> <a <?php if(isset($_SESSION['sub_menu'])) { if ($_SESSION['sub_menu'] == "pre-approval.closed" && $_SESSION['menu'] == "Pre-Approval") { ?> class="active"  <?php } }?> href="<?php echo href('/pre-approval/closed');?>"><span><i class="icon-circle black-border"></i>Closed</span></a></li>

                        <li> <a <?php if(isset($_SESSION['sub_menu'])) { if ($_SESSION['sub_menu'] == "pre-approval.rejected" && $_SESSION['menu'] == "Pre-Approval") { ?> class="active"  <?php } }?> href="<?php echo href('/pre-approval/rejected');?>"><span><i class="icon-circle theme-border"></i>Rejected</span></a></li>
                    </ul>
                </li>

            <?php } ?> -->


            <!-- Reimbursement -->
            <?php if (in_array(1, $_SESSION['packages'])) {
            if (in_array("approver", $_SESSION['rules']) || in_array("finance", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>
                <li class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Reimburse') { echo 'open';}} ?>"> <a href="#" onclick="change_menu_active('menu_reimburse')" id="menu_reimburse" data-toggle="collapse" data-target="#Reimburse" aria-expanded="false" aria-controls="Reimburse"
                                                                                                                                             class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Reimburse') { ?>collapsible-header waves-effect active open<?php } else {?>collapsible-header waves-effect collapsed<?php } }?>"><i class="fa fa-money fa-fw"></i>&nbsp;Reimbursement</a>
                    <ul id="Reimburse" class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Reimburse') { ?> collapse in  <?php } else { ?>collapse<?php } }?>">

                        <?php if (in_array("approver", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "reimburse.process") { echo 'active';}}?>" href="<?php echo href('/reimburse/process');?>"><span><i class="icon-circle brown-border"></i>Sent</span></a></li>
                        <?php } ?>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "reimburse.approved") { echo 'active';}}?>"  href="<?php echo href('/reimburse/approved');?>"><span><i class="icon-circle theme-secondary-border"></i>Approved</span></a></li>

                        <?php if (in_array("finance", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "reimburse.reimbursed") { echo 'active';}}?>"  href="<?php echo href('/reimburse/reimbursed');?>"><span><i class="icon-circle green-border"></i>Reimbursed</span></a></li>
                        <?php } ?>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "reimburse.rejected") { echo 'active';}}?>"   href="<?php echo href('/reimburse/rejected');?>"><span><i class="icon-circle theme-border"></i>Rejected</span></a></li>
                    </ul>
                </li>
            <?php } } ?>


            <!--Cash Advance-->
            <?php if (in_array(2, $_SESSION['packages'])) {
            if (in_array("approver", $_SESSION['rules']) || in_array("finance", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>
                <li class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Pre-Approval') { echo 'open';}} ?>"> <a href="#" onclick="change_menu_active('menu_pre_approval')" id="menu_pre_approval" data-toggle="collapse" data-target="#Pre-Approval" aria-expanded="false" aria-controls="Pre-Approval"
                                                                                                                                                class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Pre-Approval') { ?>collapsible-header waves-effect active<?php } else {?>collapsible-header waves-effect collapsed<?php } }?>" ><i class="fa fa-pencil-square-o"></i>&nbsp;Cash Advance</a>
                    <ul id="Pre-Approval" class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Pre-Approval') { ?> collapse in  <?php } else { ?>collapse<?php } }?>">

                        <?php if (in_array("approver", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "pre-approval.process") { echo 'active';}}?>" href="<?php echo href('/pre-approval/process');?>"><span><i class="icon-circle brown-border"></i>Sent</span></a></li>
                        <?php } ?>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "pre-approval.approved") { echo 'active';}}?>"   href="<?php echo href('/pre-approval/approved');?>"><span><i class="icon-circle theme-secondary-border"></i>Approved</span></a></li>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "pre-approval.disbursed") { echo 'active';}}?>"  href="<?php echo href('/pre-approval/disbursed');?>"><span><i class="icon-circle green-border"></i>Disbursed</span></a></li>

                        <?php if (isset($_SESSION['is_cash_advance_need_verify']) && $_SESSION['is_cash_advance_need_verify']) { ?>

                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "pre-approval.verify") { echo 'active';}}?>"   href="<?php echo href('/pre-approval/verify');?>"><span><i class="icon-circle yellow-border"></i>Unverified</span></a></li>

                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "pre-approval.verified") { echo 'active';}}?>"   href="<?php echo href('/pre-approval/verified');?>"><span><i class="icon-circle blue-grey-border"></i>Verified</span></a></li>

                        <?php } ?>

                        <?php if (in_array("finance", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "pre-approval.reported") { echo 'active';}}?>"   href="<?php echo href('/pre-approval/reported');?>"><span><i class="icon-circle indigo-border"></i>Reported</span></a></li>
                        <?php } ?>
                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "pre-approval.closed") { echo 'active';}}?>"   href="<?php echo href('/pre-approval/closed');?>"><span><i class="icon-circle black-border"></i>Closed</span></a></li>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "pre-approval.rejected") { echo 'active';}}?>"   href="<?php echo href('/pre-approval/rejected');?>"><span><i class="icon-circle theme-border"></i>Rejected</span></a></li>
                    </ul>

                </li>
            <?php } } ?>

            <?php if (in_array(3, $_SESSION['packages'])) {?>
                <!--------------------- INVOICE ----->
                <a class="" style="font-weight: 600"
                   href="<?php echo href('/jp2/invoice/claimer');?>">
                    <i class="md md md-blur-on"></i>&nbsp;Invoice
                </a>
            <?php } ?>

            <?php if (in_array(4, $_SESSION['packages'])) {?>
                <!--------------------- Procurement ----->
                <a class="" style="font-weight: 600"
                   href="<?php echo href('/jp2/procurement/claimer');?>">
                    <i class="md md md-blur-on"></i>&nbsp;Procurement
                </a>
            <?php } ?>


            <!--Attendance-->
            <?php
            if (in_array(5, $_SESSION['packages'])) { ?>
                <!--Attendance-->
                <li class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Attendance') { echo 'open';}} ?>">
                    <a href="#" onclick="change_menu_active('menu_attendance')" id="menu_attendance" data-toggle="collapse" data-target="#Attendance" aria-expanded="false" aria-controls="Attendance"
                       class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Attendance') { ?>collapsible-header waves-effect active<?php } else {?>collapsible-header waves-effect collapsed<?php } }?>"><i class="md md-alarm-on"></i>&nbsp;Attendance</a>

                    <ul id="Attendance" class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Attendance') { ?> collapse in  <?php } else { ?>collapse<?php } }?>">

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Today") { echo 'active';} }?>" href="<?php echo href('/attendance/today');?>"><i class="md md-access-time"></i>&nbsp;<span>Today</span></a></li>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Report") { echo 'active';} }?>" href="<?php echo href('/attendance/report');?>"><i class="md md-insert-chart"></i>&nbsp;<span>Report Attendance</span></a></li>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "ReportLeave") { echo 'active';} }?>" href="<?php echo href('/attendance/report-leave');?>"><i class="md md-insert-chart"></i>&nbsp;<span>Report Leave</span></a></li>

                    </ul>

                </li>
            <?php } ?>


            <!--Tools-->
            <?php if (in_array(1, $_SESSION['packages']) || in_array(2, $_SESSION['packages'])) {
            if (in_array("approver", $_SESSION['rules']) || in_array("finance", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) {?>

                <li class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Tools') { echo 'open';}} ?>"> <a href="#" onclick="change_menu_active('menu_tools')" id="menu_tools" data-toggle="collapse" data-target="#Tools" aria-expanded="false" aria-controls="Tools"
                                                                                                                                         class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Tools') { ?>collapsible-header waves-effect active<?php } else {?>collapsible-header waves-effect collapsed<?php } }?>"><i class="md md-list"></i>&nbsp;Tools</a>

                    <ul id="Tools" class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Tools') { ?> collapse in  <?php } else { ?>collapse<?php } }?>">

                        <!--Report-->
                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Report") { echo 'active';} }?>" href="<?php echo href('/report');?>"><i class="md md-insert-chart"></i>&nbsp;<span>Report</span></a></li>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Receipts") { echo 'active';} }?>" href="<?php echo href('/receipts');?>"><i class="md md-photo-camera"></i>&nbsp;<span>Receipts Photos</span></a></li>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "My-Logs") { echo 'active';} }?>" href="<?php echo href('/my-logs');?>"><i class="md md-swap-vert"></i>&nbsp;<span>My Logs</span></a></li>

                        <?php if (isset($_SESSION['company_id']) && $_SESSION['company_id'] != 282) { ?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Logs") { echo 'active';} }?>" href="<?php echo href('/logs');?>"><i class="md md-swap-vert"></i>&nbsp;<span>Logs</span></a></li>
                        <?php } ?>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Track") { echo 'active';} }?>" href="<?php echo href('/track');?>"><i class="md md-swap-vert-circle"></i>&nbsp;<span>Tracking</span></a></li>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Fraud") { echo 'active';} }?>" href="<?php echo href('/fraud');?>"><i class="md md-error"></i>&nbsp;<span>Fraud Detection</span></a></li>

                        <?php if (isset($_SESSION['is_direct_bank_transfer']) && $_SESSION['is_direct_bank_transfer'] && (in_array("finance", $_SESSION['rules']) || in_array("management", $_SESSION['rules']))) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Direct Transfer") { echo 'active';} }?>" href="<?php echo href('/tools/directtransfer');?>"><i class="md md-developer-mode"></i>&nbsp;<span>Direct Transfer</span></a></li>
                        <?php } ?>

                    </ul>

                </li>

            <?php } } ?>


            <!--Setup-->
            <?php
            if (in_array("sysadmin", $_SESSION['rules']) || in_array("admin", $_SESSION['rules'])) {?>

                <li class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Setup') { echo 'open';}} ?>"> <a href="#" onclick="change_menu_active('menu_setup')" id="menu_setup" data-toggle="collapse" data-target="#Setup" aria-expanded="false" aria-controls="Setup"
                                                                                                                                         class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Setup') { ?>collapsible-header waves-effect active<?php } else {?>collapsible-header waves-effect collapsed<?php } }?>"><i class="md md-tune"></i>&nbsp;Setup</a>
                    <ul id="Setup" class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Setup') { ?> collapse in  <?php } else { ?>collapse<?php } }?>">

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "User") { echo 'active';} }?>" href="<?php echo href('/user/list');?>"><i class="md md-desktop-windows"></i>&nbsp;<span>Dashboard Access</span></a></li>

                        <?php if (in_array(1, $_SESSION['packages']) || in_array(2, $_SESSION['packages'])) { ?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Category") { echo 'active';} }?>" href="<?php echo href('/category/list');?>"><i class="md md-attach-money"></i>&nbsp;<span>Expense Category</span></a></li>
                        <?php } ?>
                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Group") { echo 'active';} }?>" href="<?php echo href('/group/list');?>"><i class="md md-group"></i>&nbsp;<span>Teams</span></a></li>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Employee") { echo 'active';} }?>" href="<?php echo href('/employee/list');?>"><i class="md md-person"></i>&nbsp;<span>Staff</span></a></li>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Tag") { echo 'active';} }?>" href="<?php echo href('/tag');?>"><i class="md md-event-note"></i>&nbsp;<span>Tag</span></a></li>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Project") { echo 'active';} }?>" href="<?php echo href('/project');?>"><i class="md md-assignment"></i>&nbsp;<span>Project</span></a></li>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Label") { echo 'active';} }?>" href="<?php echo href('/label');?>"><i class="md md-loyalty"></i>&nbsp;<span>Label</span></a></li>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Source") { echo 'active';} }?>" href="<?php echo href('/source');?>"><i class="md md-group-work"></i>&nbsp;<span>Source</span></a></li>

                    </ul>

                </li>

            <?php } ?>

            <!--Settings-->
            <?php
            if (in_array("sysadmin", $_SESSION['rules']) || in_array("admin", $_SESSION['rules']) || in_array("finance", $_SESSION['rules'])) {?>

                <li class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Settings') { echo 'open';}} ?>"> <a href="#" onclick="change_menu_active('menu_settings')" id="menu_settings" data-toggle="collapse" data-target="#Settings" aria-expanded="false" aria-controls="Settings"
                                                                                                                                            class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Settings') { ?>collapsible-header waves-effect active<?php } else {?>collapsible-header waves-effect collapsed<?php } }?>"><i class="md md-settings"></i>&nbsp;Settings</a>

                    <ul id="Settings" class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Settings') { ?> collapse in  <?php } else { ?>collapse<?php } }?>">

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Setting") { echo 'active';} }?>" href="<?php echo href('/setting');?>"><i class="md md-settings-applications"></i>&nbsp;<span>General</span></a></li>

                        <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Developer") { echo 'active';} }?>" href="<?php echo href('/setting/developer');?>"><i class="md md-developer-mode"></i>&nbsp;<span>Developer</span></a></li>

                        <?php if (isset($_SESSION['is_have_attendance']) && $_SESSION['is_have_attendance']) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Attendance") { echo 'active';} }?>" href="<?php echo href('/setting/attendance');?>"><i class="md md-access-alarm"></i>&nbsp;<span>Attendance</span></a></li>
                        <?php } ?>

                        <?php if (isset($_SESSION['is_have_attendance']) && $_SESSION['is_have_attendance']) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Leave") { echo 'active';} }?>" href="<?php echo href('/setting/leave');?>"><i class="md md-flight"></i>&nbsp;<span>Leave</span></a></li>
                        <?php } ?>

                    </ul>

                </li>

            <?php } ?>


            <!--Sync Expense-->
            <?php
            if ( (in_array("finance", $_SESSION['rules']) || in_array("management", $_SESSION['rules'])) && ((isset($_SESSION['is_xero']) && $_SESSION['is_xero'])
                    || (isset($_SESSION['is_mandiri']) && $_SESSION['is_mandiri'])
                    || (isset($_SESSION['is_zahir']) && $_SESSION['is_zahir'])
                    || (isset($_SESSION['is_sap']) && $_SESSION['is_sap'])
                    || (isset($_SESSION['is_openbravo']) && $_SESSION['is_openbravo'])
                    || (isset($_SESSION['is_jurnal']) && $_SESSION['is_jurnal']))) {?>
                <!--Sync-->
                <li> <a href="#" onclick="change_menu_active('menu_sync')" id="menu_sync" data-toggle="collapse" data-target="#Sync" aria-expanded="false" aria-controls="Sync"
                        class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Sync') { ?>collapsible-header waves-effect active<?php } else {?>collapsible-header waves-effect collapsed<?php } }?>"><i class="md md-swap-horiz"></i>&nbsp;Sync Expense</a>

                    <ul id="Sync" class="<?php if(isset($_SESSION['header_menu'])) { if ($_SESSION['header_menu'] == 'Sync') { ?> collapse in  <?php } else { ?>collapse<?php } }?>">

                        <?php
                        if (isset($_SESSION['is_xero']) && $_SESSION['is_xero']) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Xero") { echo 'active';} }?>" href="<?php echo href('/xero');?>"><i class="md md-data-usage"></i>&nbsp;<span>Xero</span></a></li>
                        <?php } ?>

                        <?php
                        if (isset($_SESSION['is_jurnal']) && $_SESSION['is_jurnal']) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Jurnal") { echo 'active';} }?>" href="<?php echo href('/jurnal');?>"><i class="md md-data-usage"></i>&nbsp;<span>Jurnal</span></a></li>
                        <?php } ?>

                        <?php
                        if (isset($_SESSION['is_zahir']) && $_SESSION['is_zahir']) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Zahir") { echo 'active';} }?>" href="<?php echo href('/zahir');?>"><i class="md md-data-usage"></i>&nbsp;<span>Zahir</span></a></li>
                        <?php } ?>

                        <?php
                        if (isset($_SESSION['is_sap']) && $_SESSION['is_sap']) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "SAP") { echo 'active';} }?>" href="<?php echo href('/sap');?>"><i class="md md-data-usage"></i>&nbsp;<span>SAP</span></a></li>
                        <?php } ?>

                        <?php
                        if (isset($_SESSION['is_openbravo']) && $_SESSION['is_openbravo']) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "OpenBravo") { echo 'active';} }?>" href="<?php echo href('/openbravo');?>"><i class="md md-data-usage"></i>&nbsp;<span>OpenBravo</span></a></li>
                        <?php } ?>

                        <?php
                        if (isset($_SESSION['is_zahir_online']) && $_SESSION['is_zahir_online']) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Zahir Online") { echo 'active';} }?>" href="<?php echo href('/zahironline');?>"><i class="md md-data-usage"></i>&nbsp;<span>Zahir Online</span></a></li>
                        <?php } ?>

                        <?php
                        if (isset($_SESSION['is_mandiri']) && $_SESSION['is_mandiri']) {?>
                            <li> <a class="submenu <?php if(isset($_SESSION['menu'])) { if ($_SESSION['menu'] == "Mandiri") { echo 'active';} }?>" href="<?php echo href('/mandiri');?>"><i class="md md-data-usage"></i>&nbsp;<span>Mandiri e-cash</span></a></li>
                        <?php } ?>

                    </ul>

                </li>

            <?php } ?>


        </ul>
    </aside>

    <div class="main-container">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">


                <div class="navbar-header pull-left">
                    <button type="button" class="navbar-toggle pull-left m-15" data-activates=".sidebar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                </div>

                <!-- Dashboard active -->
                <div class="navbar-header pull-left dashboard">
                    <ul class="breadcrumb">
                        <?php
                        if (in_array("mobile", $_SESSION['rules']) &&
                            ((in_array(1, $_SESSION['packages']) || in_array(2, $_SESSION['packages']))) ) {?>
                            <li class=""><a class="btn btn-border btn-dashboard personal-dashboard" href="<?php echo href('/personal/dashboard');?>"><i class="fa fa-desktop"></i> My Personal Dashboard</a></li>
                        <?php }?>
                        <li class="active"><a class="btn btn-border btn-dashboard company-dashboard" href="<?php echo href('/dashboard');?>"><i class="fa fa-caret-right fa-2x"></i> <i class="fa fa-cog" style="margin: 0 0 0 6px;"></i> My Company Dashboard</a></li>

                    </ul>

                </div>


                <ul class="nav navbar-nav navbar-right navbar-right-no-collapse">
                    <!--<li class="dropdown pull-right">
                        <button class="dropdown-toggle pointer btn btn-round-sm btn-link withoutripple" data-template="assets/tpl/partials/dropdown-navbar.html" data-toggle="dropdown"> <i class="md md-more-vert f20"></i> </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownListExample">
                            <li role="presentation" class="dropdown-header"><i class="md md-desktop-mac"></i> Welcome</li>
                            <li role="presentation"><a role="menuitem" href="#"><i class="md md-help"></i> Help</a></li>
                        </ul>
                    </li>-->

                    <li class="dropdown pull-right">

                        <button onclick="location.href = '<?php echo href('/help');?>';" class="dropdown-toggle pointer btn btn-round-sm btn-link withoutripple"
                                data-placement="bottom" data-title="Help" data-toggle="tooltip" data-original-title="">
                            <i class="md  md-live-help f20"></i>
                        </button>

                    </li>

                    <li class="dropdown pull-right">

                        <button onclick="location.href = '<?php echo href('/messages');?>';" class="dropdown-toggle pointer btn btn-round-sm btn-link withoutripple"
                                data-placement="bottom" data-title="Messages" data-toggle="tooltip" data-original-title=""> <i class="md md-message f20"></i>
                        </button>

                    </li>

                </ul>


            </div>
        </nav>

        <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="">
            <?php
            if(isset($view)) {
                if(!isset($data)) $data=[];
                $data['app']= $app;
                $app->render($view, $data);
            }
            ?>

        </div>

        <div style="text-align: center" id="modal_welcome" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div style="margin-left: 10px">
                        <h3 class="modal-title">Welcome to Jojonomic</h3>
                        <br />
                        The easiest way to manage reimbursement.</div>

                    <div  class="text-center">
                        <!--Rata kiri-->
                        <div style="text-align: center" class="modal-footer">
                            <button style="margin-bottom: 10px; min-width: 400px" type="button" onclick="modal_personal()" class="btn btn-success" data-dismiss="modal">Start Reimbursing expense claims</button>
                            <br />
                            <button style="min-width: 400px" type="button" onclick="modal_invite()" class="btn btn-primary" data-dismiss="modal">I'm an Approver, Start Approving claims</button>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div id="modal_invite" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <fieldset>
                        <div class="modal-header">
                            <!--                        <h1 class="modal-title">Welcome</h1>-->
                            <legend>Send Invitations</legend>
                            <span class="help-block">Invite your team</span>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="hidden-xs col-md-8 left">
                                    <label>User(s)</label>
                                </div>
                                <div class="hidden-xs col-md-4 left">
                                    <label>Task</label>
                                </div>
                            </div>

                            <div id="divInviteEmail">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <input name="register_email[]" type="email" class="form-control" placeholder="work email">
                                        </div>
                                        <div class="col-md-4">
                                            <select name="register_rule[]" class="form-control">
                                                <option value="staff">Claim Expense</option>
                                                <option value="approver">Approve Expense</option>
                                                <option value="stapro">Claim & Approve Expense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <input name="register_email[]" type="email" class="form-control" placeholder="work email">
                                        </div>
                                        <div class="col-md-4">
                                            <select name="register_rule[]" class="form-control">
                                                <option value="staff">Claim Expense</option>
                                                <option value="approver">Approve Expense</option>
                                                <option value="stapro">Claim & Approve Expense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <input name="register_email[]" type="email" class="form-control" placeholder="work email">
                                        </div>
                                        <div class="col-md-4">
                                            <select name="register_rule[]" class="form-control">
                                                <option value="staff">Claim Expense</option>
                                                <option value="approver">Approve Expense</option>
                                                <option value="stapro">Claim & Approve Expense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="right">
                                <button onclick="return addInviteMore()" id="btnAddMore" class="btn btn-link">+add another invitation</button>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!--                        <button type="button" class="btn btn-success" data-dismiss="modal">Start reimburse claiming expense</button>-->
                            <!--                        <button type="button" class="btn btn-primary" data-dismiss="modal">I'm an Approver</button>-->
                            <button onclick="return modal_confirm()" class="btn btn-primary">Next</button>
                        </div>
                    </fieldset>
                </div>

            </div>
        </div>

        <div id="modal_confirm" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <fieldset>
                        <div class="modal-header">
                            <legend>Confirm</legend>
                            <span class="help-block">Confirm your company detail</span>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                <label>Email</label>
                                <input id="register_email_confirm" name="register_email_confirm" type="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Company name</label>
                                <input id="register_company_name_confirm" name="register_company_name_confirm" type="text" class="form-control" value="payung putih">
                            </div>

                            <br/>
                            <div id="divInviteEmailConfirm">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button onclick="modal_finish()" class="btn btn-primary" ><span id="modal_finish_text">finish</span></button>
                        </div>
                    </fieldset>
                </div>

            </div>
        </div>

    </div>
</main>

<!--//still confuse-->
<script charset="utf-8" src="<?php echo href('/');?>assets/js/vendors.min.js"></script>
<script charset="utf-8" src="<?php echo href('/');?>assets/js/app.min.js"></script>
<script src="<?php echo href('/assets/js/zebra_pagination.js'); ?>"></script>
<script src="<?php echo href('/assets/js/jQuery.print.js'); ?>"></script>

<script src="<?php echo href('/assets/js/session.js'); ?>"></script>

<script src="<?php echo href('/assets/moment/moment-timezone.min.js'); ?>"></script>
<script src="<?php echo href('/assets/moment/moment-timezone-with-data.min.js'); ?>"></script>
<script src="<?php echo href('/assets/moment/jstz-1.0.4.min.js'); ?>"></script>

<script src="<?php echo href('/bootstrap/js/bootstrap-tour.min.js'); ?>"></script>
<script src="<?php echo href('/assets/js/tourPro.js'); ?>"></script>

<script>

    function skipTutor() {
        $("#notification-screen").fadeOut();
    }

    function modal_invite() {
        $('#modal_invite').modal({backdrop: 'static', keyboard: false});
        return false;
    }

    function modal_personal() {
        window.location.href = BASE_URL + "/personal/dashboard";
    }

    function modal_confirm() {

        var emails = [""];
        var values = $("input[name='register_email[]']").map(function(){return $(this).val();}).get();
        if (values != null) {
            emails = values;
        }

        var rules = [""];
        var values = $("select[name='register_rule[]']").map(function(){return $(this).val();}).get();
        if (values != null) {
            rules = values;
        }

        var params = {
            email: emails,
            rule: rules
        };

        $.ajax({
            type: "POST",
            url: BASE_URL + "/register/live/invite",
            data: params,
            success: function (result) {
                if (!result.error) {

                    $('#modal_invite').modal('hide');
                    $('#modal_confirm').modal({backdrop: 'static', keyboard: false});

                    //load data confirm company
                    $('#register_email_confirm').val(result.company_email);
                    $('#register_company_name_confirm').val(result.company_name);

                    for (i = 0; i < result.result.length; i++) {
                        $('#divInviteEmailConfirm').append("<div class=\"form-group\"><div class=\"row\"><div class=\"col-md-8\"><input class=\"form-control\" value=\"" + result.result[i].email + "\" readonly>" +
                        "</div><div class=\"col-md-4 text-right\"><div>" + result.result[i].task +
                        "</div></div></div></div>");
                    }
                }
            },
            error: function (error) {
            }
        });

        return false;
    }

    function modal_finish() {

        var company_name = "";
        if ($('#register_company_name_confirm').val() != null) {
            company_name = $('#register_company_name_confirm').val();
        } else {
            return false;
        }

        var params = {
            company_name: company_name
        };

        $('#modal_finish_text').text("please wait...");

        $.ajax({
            type: "POST",
            url: BASE_URL + "/register/live/confirm",
            data: params,
            success: function (result) {
                if (result) {
                    $('#modal_confirm').modal('hide');
                }

                location.reload();
            },
            error: function (error) {
            }
        });
    }

</script>

<script>

    var count = 0;
    function addInviteMore() {
        if (count < 2) {
            $("#divInviteEmail").append("<div class=\"form-group\"><div class=\"row\"><div class=\"col-md-8\"><input name=\"register_email[]\" type=\"email\" class=\"form-control\" placeholder=\"work email\"></div>" +
            "<div class=\"col-md-4\"><select name=\"register_rule[]\" class=\"form-control\"><option value=\"staff\">Claim Expense</option><option value=\"approver\">Approve Expense</option><option value=\"stapro\">Claim & Approve Expense</option></select></div></div></div>");

            count++;
            if (count == 2) {
                $('#btnAddMore').hide();
            }
        } else {
            $('#btnAddMore').hide();
        }
        return false;
    }

</script>

<script type="text/javascript">

    function callTour() {

        window.open(
            'https://pro.jojonomic.com/help',
            '_blank' // <- This is what makes it open in a new window.
        );
        return false;

        if (PAGE_NAME == 'dashboard_approver') {
            callTourDashboardApprover();
        } else  if (PAGE_NAME == 'dashboard_admin') {
            callTourDashboardAdmin();
        } else  if (PAGE_NAME == 'dashboard_finance') {
            callTourDashboardFinance();
        } else  if (PAGE_NAME == 'report') {
            callTourReport();
        }
    }


    NProgress.configure({ showSpinner: false });
    $(document).ready(function() {
        NProgress.start();
    });

    $(window).load(function() {
        //setTimout() for 2 seconds progress stop delay
        setTimeout(function(){NProgress.done();
        },2000);
    });

    //Page is loading start progress
    document.onreadystatechange = function () {
        if (document.readyState == "interactive") {
            NProgress.start(); //start progress bar
        }
    }

    //when page is loaded stop progress
    var everythingLoaded = setInterval(function() {
        if (/loaded|complete/.test(document.readyState)) {
            clearInterval(everythingLoaded);
            setTimeout(function(){NProgress.done();
            },2000);
        }
    }, 10);

    //save timezone to session
    var timezone = jstz.determine();
    Session.set("timezone_name", timezone.name());
    var showDate = timezone.name() + ", " + moment().format("MMM DD YYYY");
    $('#timezone-show').text(showDate);

    function change_menu_active(menu_name){
        var NAME = document.getElementById(menu_name);
        if (NAME.className == "collapsible-header waves-effect" || NAME.className == "collapsible-header waves-effect collapsed") { // Check the current class name
            NAME.className = "collapsible-header waves-effect active";   // Set other class name
        } else {
            NAME.className = "collapsible-header waves-effect";  // Otherwise, use `second_name`
        }
    }

    if (Session.get("xero_timer_js") != null && Session.get("xero_timer_js") > 0) {
        setInterval(function () {
            var timer = Session.get('xero_timer_js');

            if (timer > 1) {
                --timer;
                Session.set('xero_timer_js', timer);
            } else {
                Session.set('xero_timer_js', 0);
            }

        }, 1000);
    }

</script>

<!-- Start of jojonomic Zendesk Widget script -->
<script>
    var ask = '<?php echo href('/assets/img/chat/help-support-ask.png'); ?>';
    var notif = '<?php echo href('/assets/img/chat/help-support-notif.png'); ?>';

    /*<![CDATA[*/ window.zEmbed || function(e, t) {
        var n, o, d, i, s, a = [],
            r = document.createElement("iframe");
        window.zEmbed = function() {
            a.push(arguments)
        }, window.zE = window.zE || window.zEmbed, r.src = "javascript:false", r.title = "", r.role = "presentation", (r.frameElement || r).style.cssText = "display: none", d = document.getElementsByTagName("script"), d = d[d.length - 1], d.parentNode.insertBefore(r, d), i = r.contentWindow, s = i.document;
        try {
            o = s
        } catch (e) {
            n = document.domain, r.src = 'javascript:var d=document.open();d.domain="' + n + '";void(0);', o = s
        }
        o.open()._l = function() {
            var o = this.createElement("script");
            n && (this.domain = n), o.id = "js-iframe-async", o.src = e, this.t = +new Date, this.zendeskHost = t, this.zEQueue = a, this.body.appendChild(o)
        }, o.write('<body onload="document._l();">'), o.close()
    }("//assets.zendesk.com/embeddable_framework/main.js", "jojonomic.zendesk.com");
    /*]]>*/

    $( "#zendesk-notif" ).hide();
    zE(function() {

        $('#zendesk-image').attr(ask);
        $('#zendesk-image').click(function(){
            $zopim.livechat.window.show();
            //clearBubble();
        });

        $zopim(function(){

            $zopim.livechat.window.onHide(hidenotif);
            $zopim.livechat.window.onShow(hidenotif);

            function hidenotif(number) {

                $('#zendesk-image').attr('src', ask);
                $( "#zendesk-notif" ).html("");
            }
            $zopim.livechat.setOnUnreadMsgs(unread);

            function unread(number) {
                $('#zendesk-image').attr('src', ask);
                if (number>=1) {
                    if (!($zopim.livechat.window.getDisplay())) {
                        //Add counter to unread message
                        $( "#zendesk-notif" ).show();
                        //$( "#zendesk-notif" ).html(number);
                        //$( "#zendesk-image" ).effect( "bounce", {times:3}, 400 );
                        $('#zendesk-image').attr('src', notif);
                    }
                }
            }

            $zopim.livechat.setOnStatus(bubble);
            function clearBubble() {
                $( "#zendesk-notif" ).html("");
                $('#zendesk-image').attr('src', ask);
            }
            function bubble(status){
                if(status=='online') {
                    $('#zendesk-image').click(function(){
                        $zopim.livechat.window.show();
                        clearBubble();
                    });
                    if (!($zopim.livechat.window.getDisplay())) {
                        clearBubble();
                    }


                    $zopim.livechat.window.setPosition('br');
                    $zopim.livechat.window.setOffsetVertical(52);
                    $zopim.livechat.window.setOffsetHorizontal(26);

                    $(".zEWidget-launcher").hide();

                    $( "#zendesk-image" ).animate({opacity: 0}, 1000 , function() {

                        $('#zendesk-image').attr('src', ask);
                        $( "#zendesk-image" ).animate({opacity: 1}, 100);
                        //$('#zendesk-image').css('opacity') = 1;
                        //$( "#zendesk-image" ).effect( "bounce", {times:3}, 400 );
                    });

                    //$zopim.livechat.bubble.hide();
                } else if(status=='away') {
                    //embed the API which hides the bubble. This part of the code runs only when the chat status is away
                    //$zopim.livechat.bubble.hide();
                } else if(status=='offline') {
                    //alert("offline");
                    //embed the API which hides the bubble. This part of the code runs only when the chat status is offline
                    //$zopim.livechat.bubble.hide();
                }
            }

        });
    });


    $('#zendesk-image').hover(function() {
        $(this).css('cursor','pointer');
    });

</script>
<!-- End of jojonomic Zendesk Widget script -->


<script>
    // show overflow x scroll
    $(document).ready(function($){
        $('table#gridTable').parent().css('overflow-x', 'auto');
    });
</script>


</body>
</html>