# Retorika Kampus #

News Online for Indonesia's People

### How do I get set up? ###

1. clone this repository
2. composer install
3. create file .env from .env-examplae
4. setting your database
5. php artisan key:generate
6. php artisan migrate:refresh --seed


### Team ###

1. Muhammad Alvian Supriadi
2. Andy Eka Saputra
